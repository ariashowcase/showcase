(function(ctx) {
  // UI Constants
  var _CLASSNAME_DISABLED = "slds-hide";

  // Code Constants
  var _COMMAND_LINK_STATE = {
    enabled: "enabled",
    disabled: "disabled"
  };

  // Controller
  ctx.Rjs.define(['lib/jquery', 'lib/moment', 'utils/js/logUtil', 'utils/js/sldsUtil', 'utils/js/roUtil'], function($, moment, LogUtil, SldsUtil, RoUtil) {
    var _currentCommandLinkState = _COMMAND_LINK_STATE.disabled;
    LogUtil.debug("WINDOWS LOCATION: "+ window.location.href);
    initializeBoard();
    LogUtil.info("User ID: {0}", ctx.ForceUI.UserId);

    var _caseIdToOpen = null;

    function initializeBoard(){
      $Lightning.use("c:BBSObjectBoardOutApp", function() {
        $Lightning.createComponent("c:BBSObjectBoard",
          {
            'SObjectType': "Case",
            'StageValueField' : "Status",
            'FieldNames' : "CaseNumber, Subject, LastViewedDate",
            'Filter': "OwnerId = '" + ctx.ForceUI.UserId + "'",
            'OrderBy': "LastViewedDate ASC",
            'UseAbbreviatedFieldNames': true
          },
          "kanbanBoard",
          function(cmp) {
            $A.eventService.addHandler({
              event: "c:BBSObjectBoardRecordSelectedEvent",
              handler: function(event) {
                LogUtil.debug("Received record selected event: {0}", JSON.stringify(event));
                var recordId = event.getParam("recordId");
                _caseIdToOpen = recordId;
                if (!recordId) {
                  LogUtil.error("Record ID missing");
                  return;
                }

                sforce.console.openPrimaryTab(null, '/' + recordId, true, undefined, function (openPrimaryResult) {
                  LogUtil.debug("OpenPrimaryTab result: {0}", openPrimaryResult.success);
                  if (!openPrimaryResult.success) {
                    LogUtil.debug("OpenPrimaryTab failed, trying to find existing tab");
                    sforce.console.focusPrimaryTabByName(recordId, function (focusPrimaryResult) {
                      LogUtil.debug("FocusPrimaryTabByName result: {0}", focusPrimaryResult.success);
                    });
                  }
                }, recordId );

                sforce.console.setCustomConsoleComponentVisible(false);
                }
              });
            }
          );
        }
      );
    }

    function acceptWorkIfBacklogCase(workId, workItemId) {
          if (!_caseIdToOpen) {
            LogUtil.info('Workitem was not opened through the backlog');
            return;
          }

          if (_caseIdToOpen.substring(0, 15) === workItemId.substring(0, 15)) {
            sforce.console.presence.acceptAgentWork(workId, function(result) {
              _caseIdToOpen = null;
              if (result.success) {
                LogUtil.debug('Accepted work successfully');
              } else {
                LogUtil.debug('Failed to accept the workitem, it might be auto-accepted');
              }
            });
          }  else {
            LogUtil.info('Workitem is not the case that was opened through the backlog');
            _caseIdToOpen = null;
          }
        }

    function setCommandLinkState(state) {
      LogUtil.debug("Setting commandLink state to: {0}", state);
      _currentCommandLinkState = state;

      var $commandLinks = $(".clickableTitle");
      LogUtil.debug("# of command links testing: {0}", $commandLinks.length);

      var isEnabled = _COMMAND_LINK_STATE.enabled === (state || "").toLowerCase();
      LogUtil.debug("THIS IS ISENABLED VALUE: "+isEnabled)
      $Lightning.use("c:BBSObjectBoardOutApp", function() {
          var myExternalEvent = $A.get("e.c:BBSObjectBoardConsoleStatusChangeEvent");
          myExternalEvent.setParams({"status" : isEnabled});
          myExternalEvent.fire();
      });


      if (isEnabled) {
        $("#omni-invalid-state-msg").addClass("slds-hide");
      } else {
        $("#omni-invalid-state-msg").removeClass("slds-hide");
      }
    }

    function setCurrentCommandLinkState() {
      setCommandLinkState(_currentCommandLinkState);
    }

    function getCaseBacklogState(info) {
      var caseBacklogState = _COMMAND_LINK_STATE.disabled;
      var channels = JSON.parse(info.channels);

      var i, len = channels.length;
      for (i = 0; i < len; i++) {
        var theChannel = channels[i];
        LogUtil.debug("Channel name: {0}", theChannel.developerName);
        if (theChannel.developerName === "ASP_AgentBacklogChannel") {
          caseBacklogState = _COMMAND_LINK_STATE.enabled;
        }
      }
      return caseBacklogState;
    }

    function refreshPageList()  {
      LogUtil.debug("Refreshing board");
      $Lightning.use("c:BBSObjectBoardOutApp", function() {
        var myExternalEvent = $A.get("e.c:BBSObjectBoardRecordStageChangedEvent");
        myExternalEvent.fire();
      });
    }

    LogUtil.debug('Getting current omni status');
    sforce.console.presence.getServicePresenceStatusChannels(function(result) {
      if (result.success) {
        LogUtil.debug("Current omni status received: {0}", JSON.stringify(result));
        setCommandLinkState(getCaseBacklogState(result));
      } else {
        LogUtil.warn('Retrieving current channel status failed - might be offline');
        setCommandLinkState(_COMMAND_LINK_STATE.disabled);
      }
    });

    LogUtil.debug('Initializing Omni state listener');
    sforce.console.addEventListener(sforce.console.ConsoleEvent.PRESENCE.STATUS_CHANGED, function (ev) {
      LogUtil.debug("Omni state change received: {0}", JSON.stringify(ev));
      setCommandLinkState(getCaseBacklogState(ev));
    });

    sforce.console.addEventListener(sforce.console.ConsoleEvent.PRESENCE.WORK_ACCEPTED, function (ev) {
      LogUtil.debug("New workitem accepted: {0}", JSON.stringify(ev));
      refreshPageList();
    });

    sforce.console.addEventListener(sforce.console.ConsoleEvent.PRESENCE.WORK_ASSIGNED, function (ev) {
      LogUtil.debug("New workitem assigned: {0}", JSON.stringify(ev));
      acceptWorkIfBacklogCase(ev.workId, ev.workItemId);
    });

    sforce.console.addEventListener(sforce.console.ConsoleEvent.PRESENCE.LOGOUT, function (ev) {
      LogUtil.debug("Omni logout received: {0}", JSON.stringify(ev));
      setCommandLinkState(_COMMAND_LINK_STATE.disabled);
    });

    LogUtil.debug('Initializing Case push listener');
    sforce.console.addPushNotificationListener(['Case'], function (ev) {
      LogUtil.debug("Received push event: {0}", JSON.stringify(ev));
      refreshPageList();
    });

    if (sforce.console.isInConsole()) {
      LogUtil.debug("Running inside of console");
      sforce.console.addPushNotificationListener(['Case'], function (ev) {
        LogUtil.debug("Received push event: {0}", JSON.stringify(ev));

        $Lightning.use("c:BBSObjectBoardOutApp", function() {
          var myExternalEvent = $A.get("e.c:BBSObjectBoardRecordStageChangedEvent");
          myExternalEvent.fire();
        });
      });
    }
    refreshPageList();
  });
})(this);
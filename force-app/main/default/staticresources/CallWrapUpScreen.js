(function(ctx) {
  console.log('AAC_PostCallUpdateInteraction controller loaded');
  ctx.Rjs.require(['lib/jquery', 'utils/js/logUtil', 'lib/moment'], function($, LogUtil) {
    LogUtil.setPageId('AAC_PostCallUpdateInteraction.page');

    $('#addCase').click(addCaseRow);
    $('#addOpportunity').click(addOppRow);
    $('#saveBtn').click(saveRecord);

    function addCaseRow(){

      var last_id = $('[id*="caseId"]:last').attr('id');
      var num = last_id.substr(last_id.length - 1)+1;

      $('#addMoreCases').append(
        '<input type="hidden" data-object="Case" id="caseId'+num+'"/>'+
        '<input type="text" placeholder="Case lookup" style="width:215px;" value="" id="caseName'+num+'" class="slds-input slds-input_bare"/>'+
        '<a href="#" onclick="openLookupPopup(\'caseName'+num+'\', \'caseId'+num+'\', \'Case\'); return false">'+
        '<span class="slds-icon_container" title="Case Lookup (New Window)">'+
        '<svg class="slds-button__icon slds-button__icon_left slds-icon_x-small" aria-hidden="true">'+
        '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#search" />'+
        '</svg><span class="slds-assistive-text">Case Lookup (New Window)</span>'+
        '</span>'+
        '</a><br />'
      )

    }

    function addOppRow(){

      var last_id = $('[id*="oppId"]:last').attr('id');
      var num = last_id.substr(last_id.length - 1)+1;

      $('#addMoreOpportunities').append(
        '<input type="hidden" data-object="Opp" id="oppId'+num+'"/>'+
        '<input type="text" placeholder="Opportunity lookup" style="width:215px;" value="" id="oppName'+num+'" class="slds-input slds-input_bare"/>'+
        '<a href="#" onclick="openLookupPopup(\'oppName'+num+'\', \'oppId'+num+'\', \'Opportunity\'); return false">'+
        '<span class="slds-icon_container" title="Opportunity Lookup (New Window)">'+
        '<svg class="slds-button__icon slds-button__icon_left slds-icon_x-small" aria-hidden="true">'+
        '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#search" />'+
        '</svg><span class="slds-assistive-text">Case Lookup (New Window)</span>'+
        '</span>'+
        '</a><br />'
      )
    }

    function saveRecord(){
      LogUtil.info('AAC_PostCallUpdateInteraction.saveRecord invoked');

      var account = $('[id*="AccountSelector"]').val();
      var contact = $('[id*="ContactSelector"]').val();
      var segmentId = $('#segmentId').val();
      var cases = [];
      var opps = [];

      $('[data-object]').each(function(index){
        if($(this).data("object") === "Case"){
          cases.push($(this).val());
        }
        else {
          opps.push($(this).val());
        }
      });
      var casesString = cases.join(",");
      var oppsString = opps.join(",");

      var properties = JSON.stringify({
        account: account,
        contact: contact,
        cases: casesString,
        opps: oppsString,
        segmentId: segmentId
      });

      LogUtil.info('AAC_PostCallUpdateInteraction.VF remoting invoked');
      Visualforce.remoting.Manager.invokeAction(
        'AAC_PostCallUpdateInteractionController.quickSave',
        properties,
        function(result, event) {
          LogUtil.info('AAC_PostCallUpdateInteraction.VF remoting close tab function fired');
          if (sforce.console && sforce.console.isInConsole()) {
            sforce.console.getEnclosingPrimaryTabId(function(result) {
              sforce.console.closeTab(result.id);
            });
          } else {
            document.getElementById('onSaveCompleted').classList.remove('hidden');
            setTimeout(function () {
              document.getElementById('onSaveCompleted').classList.add('hidden');
            }, 3000);
          }
        }
      );
    }
  });
})(this);
phoneReady.then(function(p) {
    var phone = p.phone;
    var ui = p.ui;
    
    /*
    softphone.phone.Test.popOther("4074716908");
    softphone.phone.Test.popOther("3");
    */
    
    phone.loggedIn.subscribe(function() {
        ui.phoneBar.readyStates.push(
            { label: "Ready", data: "ready", image: "https://na1.salesforce.com/img/support/liveagent/onlineDot.png" },
            { label: "Lunch", data: "lunch", image: "https://na1.salesforce.com/img/support/liveagent/awayDot.png" },
            { label: "Not Ready", data: "notReady", image: "https://na1.salesforce.com/img/support/liveagent/awayDot.png" }
        );
    });
    
    phone.calls.added.subscribe(function() {
        phone.setState("notReady");
    });
    
    
    sforce.console.addEventListener("velocity", function(result) {
        message = JSON.parse(result.message);
        
        switch(message.command) {
            case "answer":
                answer();
                break;
            case "release":
                release();
                break;
            case "transfer":
            	transfer(message.destination, message.popIds);
                break;
        }
    });
    
    function getCall(callId) {
        return phone.calls.find(c => c.id == callId);
    }
    
    function transfer(destination, popIds) {
        var call = getCall(ui.screen.callId);
        
        phone.calls.answered.subscribe(function answered(c) {
            if(c.otherParties[0].number == destination) {
                phone.calls.answered.unsubscribe(answered);
                call.transfer(c);
            }
        });
        
        var context = call.sfContext;
        if(context && context.associatedId) {
            popIds.push(context.associatedId);
        }
        
        phone.calls.added.subscribe(function added(c) {
            if(c.otherParties[0].number == destination) {
                phone.calls.added.unsubscribe(added);
                c.data.set("userPopIds", JSON.stringify(popIds));
            }
        });
        phone.dial(destination);
    }
});
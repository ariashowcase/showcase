/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  const _SOFTPHONE_WINDOW_NAME = "AriaConnectSoftphoneWindow";

  ctx.Rjs.define(['lib/jquery', 'lib/promise', 'utils/js/logUtil', 'utils/js/stringUtil', 'UTILS/softphone-settings', 'connect', 'console', 'cti-wrapper', 'UTILS/lightning-omni-api'], function ($, Promise, LogUtil, StringUtil, Settings, connect, sfConsole, cti, ltngApi) {
    var _loginWindow = null;

    function openExternalSoftphone(ccpUrl, dimension) {
      var windowFeatures = StringUtil.format("menubar=no, location=no, toolbar=no, resizable=yes, status=no, width={0}, height={1}", dimension.width, dimension.height);

      // window.open('', ...) will retrieve the window reference of an existing popup window with the given name without refreshing the window itself.
      return window.open(ccpUrl, _SOFTPHONE_WINDOW_NAME, windowFeatures);
    }

    function hideSpinner() {} {
      $('#spinner').addClass('slds-hide');
    }

    function showLoginScreen() {
      $('#ccpSoftphoneLogin').addClass('slds-is-open');
      $('button[data-panelId="ccpSoftphoneLogin"]').addClass('slds-is-active');
    }

    function hideLoginScreen() {
      $('#ccpSoftphoneLogin').removeClass('slds-is-open');
      $('button[data-panelId="ccpSoftphoneLogin"]').removeClass('slds-is-active');
    }

    function onSoftphoneLoginSuccessful() {
      LogUtil.debug("onSoftphoneLoginSuccessful invoked");
      hideLoginScreen();

      if (_loginWindow) {
          _loginWindow.close();
          _loginWindow = null;
      }

      $('button[data-panelId="ccpSoftphoneLogin"]').remove();
    }

    function addExternalWindowAction(ccpUrl, dimension) {
      $("#externalSoftphoneBtn").click(function() {
        let softphoneWindow = openExternalSoftphone(ccpUrl, dimension);
        softphoneWindow.focus();
      });
    }

    function addToolbarButtonsActions() {
      $("button.slds-utility-bar__action").click(function() {
        let $button = $(this);
        let $panel = $("#" + $button.attr('data-panelId'));

        $panel.toggleClass('slds-is-open');
        $panel.siblings().removeClass('slds-is-open');

        $("button.slds-utility-bar__action").removeClass('slds-is-active');

        $button.toggleClass('slds-is-active', $panel.hasClass('slds-is-open'));
        $button.blur();
      });
    }

    function addCloseButtonActions() {
      $("button.minimize-button").click(function() {
        let $panel = $(this).closest('section');
        let $button = $('button[data-panelId="' + $panel.attr('id') + '"]');
        $panel.removeClass('slds-is-open');
        $button.removeClass('slds-is-active');
      });
    }

    function showCallToolbar() {
      $("li[data-toolbar='place']").addClass('slds-hide');
      $("li[data-toolbar='interaction']").removeClass('slds-hide');
    }

    function showPlaceToolbar() {
      $("li[data-toolbar='place']").removeClass('slds-hide');
      $("li[data-toolbar='interaction']").addClass('slds-hide');
    }

    function getSoftphoneDimension(ccSettings) {
      let width = null;
      let height = null;
      if (cti.isLightning()) {
          LogUtil.debug("Setting dimensions for reqConnectLW");
          width = ccSettings['/reqConnectSFCCPOptions/reqConnectLCW'];
          height = ccSettings['/reqConnectSFCCPOptions/reqConnectLCH'];
      } else if (cti.isClassic()) {
          if (sfConsole.isInConsole()) {
              LogUtil.debug("Setting dimensions for reqConnectCW");
              width = ccSettings['/reqConnectSFCCPOptions/reqConnectCW'];
              height = ccSettings['/reqConnectSFCCPOptions/reqConnectCH'];
          } else {
              LogUtil.debug("Setting dimensions for reqConnectCLCW");
              width = ccSettings['/reqConnectSFCCPOptions/reqConnectCLCW'];
              height = ccSettings['/reqConnectSFCCPOptions/reqConnectCLCH'];
          }
      }

      return {
        height: parseInt(height),
        width: parseInt(width)
      };
    }

    return {
      initialize: function(ccpUrl) {
        LogUtil.info("UIManager initializing");
        addToolbarButtonsActions();
        addCloseButtonActions();

        $(".slds-utility-bar_container").removeClass("slds-hide");

        $('#loginButton').click(function () {
          _loginWindow = window.open(ccpUrl, _SOFTPHONE_WINDOW_NAME, 'menubar=no, location=no, toolbar=no, resizable=no, status=no, width=400, height=520');
        });

        LogUtil.info("UIManager setting up logout handler");
        var eventBus = connect.core.getEventBus();
        eventBus.subscribe(connect.EventType.TERMINATED, function () {
          LogUtil.info("UIManager logout detected");
          window.location.reload();
        });

        Settings.getStringValues().then(function (ccSettings) {
          ltngApi.force && ltngApi.force.showToast({
            type: "info",
            message: "Softphone initializing...",
            duration: 2000
          });

          var dimension = getSoftphoneDimension(ccSettings);

          cti.setSoftphoneWidth(dimension.width);

          let targetHeight = dimension.height + 40;
          cti.setSoftphoneHeight(targetHeight); // 40px for utility bar
          if (cti.isLightning()) {
            LogUtil.info("UIManager:lightning adjustments");
            $('.connectContainer').css('height', dimension.height + 'px');
            $('.slds-scope .slds-utility-bar').css({ 'bottom': 'auto'} );
            $('footer').css({ 'height': '40px', 'bottom': '40px'} );
            $('section').css({ 'margin-bottom': '20%'} );
          }

          hideSpinner();
          showLoginScreen();

          LogUtil.debug("Setting up agent callback for login");
          connect.agent(function (agent) {
            LogUtil.debug("Agent sign-in detected");
            onSoftphoneLoginSuccessful();
            ltngApi.force && ltngApi.force.showToast({
              type: "success",
              message: "Softphone ready, current state: " + agent.getStatus().name,
              duration: 2000
            });
          });

          connect.contact(function (contact) {
            contact.onConnecting(showCallToolbar);
            contact.onConnected(showCallToolbar);
            contact.onEnded(showPlaceToolbar);
          });

          return ccSettings;
        });
      },
      initializeSalesCloud: function (ccpUrl) {
        LogUtil.info("UIManager initializing Sales Cloud");

        cti.setSoftphoneHeight(100);

        $("#classicLayout").removeClass("slds-hide");

        $("#ccpSoftphone").addClass("slds-hide");
        $("#consoleLayout").addClass("slds-hide");

        Settings.getStringValues().then(function (ccSettings) {
          hideSpinner();

          addExternalWindowAction(ccpUrl, getSoftphoneDimension(ccSettings));
        });
      }
    }
  });
})(this);
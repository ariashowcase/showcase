/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/promise', 'utils/js/logUtil', 'open-cti', 'open-cti-ltng'], function (Promise, LogUtil, interaction, opencti) {
    LogUtil.info("OpenCtiWrapper:initializing");

    let _isLightning = ctx.ForceUI.IsLightning;
    let _isClassic = !_isLightning;

    LogUtil.info("User interface detection: classic={0}, lightning={1}", _isClassic, _isLightning);

    function getCallCenterSettings() {
      LogUtil.debug("getCallCenterSettings invoked");
      return new Promise(function (resolve, reject) {
        let callbackFunc = function (response) {
          if (response.error || response.errors) {
            var errors = response.error || response.errors;

            LogUtil.error('getCallCenterSettings failed: ' + errors);
            reject(errors);
            return;
          }

          LogUtil.debug("getCallCenterSettings successful");
          let callCenterSettingsAsStringValues = response.returnValue || JSON.parse(response.result);
          resolve(callCenterSettingsAsStringValues);
        };

        _isClassic && interaction.cti.getCallCenterSettings(callbackFunc);
        _isLightning && opencti.getCallCenterSettings({callback: callbackFunc});
      });
    }

    function setSoftphoneWidth(newWidth) {
      LogUtil.debug("setSoftphoneWidth invoked");
      let callbackFunc = function (response) {
        if (response.error || response.errors) {
          var errors = response.error || response.errors;

          LogUtil.error('setSoftphoneWidth failed: ' + errors);
          return;
        }

        LogUtil.debug("setSoftphoneWidth successful");
      };

      _isClassic && interaction.cti.setSoftphoneWidth(newWidth, callbackFunc);
      _isLightning && opencti.setSoftphonePanelWidth({ widthPX: newWidth, callback: callbackFunc });
    }

    function setSoftphoneHeight(newHeight) {
      LogUtil.debug("setSoftphoneHeight invoked");
      let callbackFunc = function (response) {
        if (response.error || response.errors) {
          var errors = response.error || response.errors;

          LogUtil.error('setSoftphoneHeight failed: ' + errors);
          return;
        }

        LogUtil.debug("setSoftphoneHeight successful");
      };

      _isClassic && interaction.cti.setSoftphoneHeight(newHeight, callbackFunc);
      _isLightning && opencti.setSoftphonePanelHeight({ heightPX: newHeight, callback: callbackFunc });
    }

    function showSoftphone() {
      LogUtil.debug("showSoftphone invoked");
      _isClassic && interaction.setVisible(true);
      _isLightning && opencti.setSoftphonePanelVisibility({ visible: true });
    }

    function hideSoftphone() {
      LogUtil.debug("hideSoftphone invoked");
      _isClassic && interaction.setVisible(false);
      _isLightning && opencti.setSoftphonePanelVisibility({ visible: false });
    }

    function enableClickToDial() {
      LogUtil.debug("enableClickToDial invoked");
      _isClassic && interaction.cti.enableClickToDial();
      _isLightning && opencti.enableClickToDial();
    }

    function disableClickToDial() {
      LogUtil.debug("disableClickToDial invoked");
      _isClassic && interaction.cti.disableClickToDial();
      _isLightning && opencti.disableClickToDial();
    }

    function onClickToDial(listener) {
      LogUtil.debug("onClickToDial invoked");

      function callbackFunc(ev) {
        let evData = typeof(ev.result) === "string" ? JSON.parse(ev.result) : ev;

        let normalizedResult = typeof(ev.result) === "undefined" ? evData : {
          number: evData.number,
          recordId: evData.objectId,
          recordName: evData.objectName,
          objectType: evData.object,
          accountId: evData.accountId,
          contactId: evData.contactId,
          personAccount: evData.personAccount
        };

        listener(normalizedResult);
      }

      _isClassic && interaction.cti.onClickToDial(callbackFunc);
      _isLightning && opencti.onClickToDial({ listener: callbackFunc });
    }

    function searchAndScreenPop(searchParams, queryParams, callType) {
      LogUtil.debug("searchAndScreenPop invoked");
      function callbackFunc(response) {
        LogUtil.info("searchAndScreenPop:Response");
         try {
           if (response.success || response.result) {
             LogUtil.info("searchAndScreenPop:Result " + JSON.stringify(response.returnValue || response.result));
           } else {
             LogUtil.info("searchAndScreenPop:Error:" + (response.error || JSON.stringify(response.errors)));
           }
         } catch (ex) {
           LogUtil.error("searchAndScreenPop:Error:" + JSON.stringify(ex));
         }
      }

      _isClassic && interaction.searchAndScreenPop(searchParams, queryParams, callType, callbackFunc);
      _isLightning && opencti.searchAndScreenPop({
           searchParams: searchParams,
           queryParams: queryParams,
           callType: callType,
           deferred: false,
           callback: callbackFunc
      });
    }

    function screenPopUrl(url) {
      LogUtil.debug("screenPopUrl invoked");
      function callbackFunc(response) {
        LogUtil.info("screenPopUrl:Response");
        try {
         if (response.success || response.result) {
           LogUtil.info("screenPopUrl:Result " + JSON.stringify(response.returnValue || response.result));
         } else {
           LogUtil.warn("screenPopUrl:Error:" + (response.error || JSON.stringify(response.errors)));
         }
        } catch (ex) {
         LogUtil.error("screenPopUrl:Error:" + JSON.stringify(ex));
        }
      }

      _isClassic && interaction.screenPop(url, true, callbackFunc);
      _isLightning && opencti.screenPop({
         type: opencti.SCREENPOP_TYPE.URL,
         params: {
           url: url
         },
         callback: callbackFunc
      });
    }

    function saveLog(obj) {
      LogUtil.debug("saveLog invoked");
      return new Promise(function (resolve, reject) {
        let callbackFunc = function (response) {
          if (response.error || response.errors) {
            var errors = response.error || response.errors;

            LogUtil.error('saveLog failed: ' + errors);
            reject(errors);
            return;
          }

          resolve(response.result || response.returnValue.recordId);
        };

        if (_isClassic) {
          let objString = "";
          for (let key in obj) {
            if (key === "entityApiName") {
              continue;
            }

            objString += key + "=" + encodeURIComponent(obj[key]) + "&";
          }

          objString = objString.slice(0, -1);
          LogUtil.debug('objString=' + objString);
          sforce.interaction.saveLog(obj.entityApiName, objString, callbackFunc);
        }

        _isLightning && opencti.saveLog({
          value: obj,
          callback: callbackFunc
        });
      });

    }

    return {
      isClassic: function () { return _isClassic; },
      isLightning: function () { return _isLightning; },
      getCallCenterSettings: getCallCenterSettings,
      setSoftphoneWidth: setSoftphoneWidth,
      setSoftphoneHeight: setSoftphoneHeight,
      showSoftphone: showSoftphone,
      hideSoftphone: hideSoftphone,
      enableClickToDial: enableClickToDial,
      disableClickToDial: disableClickToDial,
      onClickToDial: onClickToDial,
      searchAndScreenPop: searchAndScreenPop,
      screenPopUrl: screenPopUrl,
      saveLog: saveLog
    };
  });
})(this);
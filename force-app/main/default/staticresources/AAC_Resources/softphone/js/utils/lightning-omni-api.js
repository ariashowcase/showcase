/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/promise', 'utils/js/logUtil', 'x-window'], function (Promise, LogUtil, XWindow) {
    LogUtil.info("LightningBridge initializing");

    if (!ctx.ForceUI.IsLightning) {
      LogUtil.info("LightningBridge not in lightning - disabled");
      return {};
    }

    var Event = function (eventName) {
      var _handlers = [];

      var fire = function (data) {
        for (var i = 0, length = _handlers.length; i < length; i++)
          _handlers[i](data);
      }

      XWindow.registerHandler(eventName, fire, "*");

      return {
        bind: function (handler) {
          _handlers.push(handler);
        },
        unbind: function (handler) {
          for (var i = 0, length = _handlers.length; i < length; i++) {
            if (_handler[i] == handler) {
              delete _handlers[i];
              return;
            }
          }
        }
      };
    };

    var sendCommand = function(command, args, successCallback, errorCallback, timeoutCallback) {
      LogUtil.info("LightningBridge.sendCommand '" + command + "' invoked");

      var apiCommand = "Aria.LightningOmniCtiApiBroker.Command." + command;

      XWindow.post({
        targetWindow: window.parent,
        command: apiCommand,
        parameters: args,
        targetDomain: "*",
        timeout: 3000,
        successCallback: successCallback,
        errorCallback: errorCallback,
        timeoutCallback: timeoutCallback
      });

      LogUtil.info("LightningBridge.sendCommand done");
    };

    XWindow.post({
      targetWindow: window.parent,
      command: "Aria.LightningOmniCtiApiBroker.InitializeApi",
      targetDomain: "*",
      successCallback: function () {
        LogUtil.info("LightningOmniCtiApiBroker.InitializeApi:Registered window with OmniCtiApiBroker");
      },
      errorCallback: function (message) {
        LogUtil.info("LightningOmniCtiApiBroker.InitializeApi:Error registering window with OmniCtiApiBroker: " + message);
      },
      timeoutCallback: function () {
        LogUtil.info("LightningOmniCtiApiBroker.InitializeApi:Timeout registering window with OmniCtiApiBroker");
      }
    });

    return {
      force: {
        showToast: function (args) {
          sendCommand('ShowToast', args);
        },
      },
      console: {
        getFocusedPrimaryTabId: function(successCallback, errorCallback, timeoutCallback) {
          sendCommand('GetFocusedPrimaryTabId', {}, successCallback, errorCallback, timeoutCallback);
        },
        openSubtab: function(args, successCallback, errorCallback, timeoutCallback) {
          sendCommand('OpenSubtab', args, successCallback, errorCallback, timeoutCallback);
        },
        openPrimaryTab: function(args, successCallback, errorCallback, timeoutCallback) {
          sendCommand('OpenPrimaryTab', args, successCallback, errorCallback, timeoutCallback);
        },
      },
      omni: {
        setPresenceStatus: function(args, successCallback, errorCallback, timeoutCallback) {
          sendCommand('SetPresenceStatus', args, successCallback, errorCallback, timeoutCallback);
        },
        logout: function(successCallback, errorCallback, timeoutCallback) {
          sendCommand('Logout', {}, successCallback, errorCallback, timeoutCallback);
        },
      },
      events: {
        tabClosed: new Event('Aria.LightningOmniCtiApiBroker.Event.TabClosed'),
        presenceStatusChanged: new Event('Aria.LightningOmniCtiApiBroker.Event.PresenceStatusChanged'),
        logout: new Event('Aria.LightningOmniCtiApiBroker.Event.Logout'),
        workAccepted: new Event('Aria.LightningOmniCtiApiBroker.Event.WorkAccepted'),
      }
    };
  });
})(this);
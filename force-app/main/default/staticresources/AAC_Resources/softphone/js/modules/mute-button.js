/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/jquery', 'utils/js/logUtil', 'console', 'connect', 'connect-rtc'], function ($, LogUtil, sfConsole, connect) {
    LogUtil.info("MuteButton:initializing");

    var _activeContact = null;

    if (sfConsole && !sfConsole.isInConsole()) {
      LogUtil.info("MuteButton:onAgentHandler not in console");
      return;
    }

    function parseCallConfig(serializedConfig) {
      // Our underscore is too old for unescape
      // https://issues.amazon.com/issues/CSWF-1467
      var decodedJSON = serializedConfig.replace(/&quot;/g, '"');
      return JSON.parse(decodedJSON);
    };

    function checkIsMuted(contact) {
      var audioTrack = contact.rtcSession._localStream.getAudioTracks()[0];
      if (audioTrack && !audioTrack.enabled) {
        return true;
      } else {
        return false;
      }
    }

    function setButtonStates(contact) {
      var isMuted = checkIsMuted(contact);
      LogUtil.info("MuteButton:Setting mute button state: " + isMuted);

      $("#isMutedBtn").toggleClass('slds-hide', !isMuted);
      $("#isUnmutedBtn").toggleClass('slds-hide', isMuted);
    }

    function mute(contact) {
      LogUtil.info("MuteButton:muting call");
      contact.rtcSession.pauseLocalAudio();
      setButtonStates(contact);
    }

    function unmute(contact) {
      LogUtil.info("MuteButton:unmuting call");
      contact.rtcSession.resumeLocalAudio();
      setButtonStates(contact);
    }

    function toggleMute() {
      let $button = $(this);
      let id = $button.attr('id');

      if (!_activeContact) {
        LogUtil.warn('No active call found, cancelling un/mute request');
        return;
      }

      if (id === 'isMutedBtn') {
        unmute(_activeContact);
      } else {
        mute(_activeContact);
      }
    }

    $("button.mute-button").click(toggleMute);

    connect.contact(function(contact) {
      contact.onConnected(function () {
        _activeContact = contact;
        setButtonStates(contact);
      });

      contact.onEnded(function () {
        _activeContact = null;
      });
    });
  });
})(this);
/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/jquery', 'lib/promise', 'utils/js/logUtil', 'lib/libphonenumber', 'UTILS/softphone-settings', 'connect', 'console', 'cti-wrapper', 'UTILS/lightning-omni-api'], function ($, Promise, LogUtil, PhoneLib, Settings, connect, sfConsole, cti, ltngApi) {
    LogUtil.info("CallCampaign:initializing");

    if (!sfConsole.isInConsole()) {
      LogUtil.info("CallCampaign:onAgentHandler not in console");
      return;
    }

    var onWorkItemAccepted = function(result) {
      LogUtil.info("CallCampaign:onAgentHandler:OmniWorkAccptedHandler invoked");

      var callCampaign = new SObjectModel.Call_Campaign();
      callCampaign.retrieve({where:{Id: {eq:result.workItemId}}}, function(err, records, event) {
        if (err) {
          LogUtil.error("CallCampgin:onAgentHandler unable to retrieve call campaign record: " + err);
          return;
        }

        records.forEach(function(record) {
          var phone = record.get("Phone_Number");
          var id = record.get("Id").substr(0, 15);
          var contactId = record.get("Contact");
          var leadId = record.get("Lead");
          var accountId = record.get("Account");
          var oppId = record.get("Opportunity");
          var caseId = record.get("Case");

          LogUtil.info("CallCampaign:onAgentHandler:RecordReceived "+phone+" "+id+" "+contactId+" "+accountId+" "+oppId);
          var onFocusedPrimaryTabHandler = function(response) {
            LogUtil.debug("CallCampaign:onFocusedPrimaryTabHandler: TabId=" + response.id);
            leadId && (ctx.ForceUI.IsLightning ? ltngApi.console.openSubtab({parentTabId: response.id, recordId: leadId, focus: true}) : sfConsole.openSubtab(response.id, '/' + leadId, true));
            contactId && (ctx.ForceUI.IsLightning ? ltngApi.console.openSubtab({parentTabId: response.id, recordId: contactId, focus: true}) : sfConsole.openSubtab(response.id, '/' + contactId, true));
            accountId && (ctx.ForceUI.IsLightning ? ltngApi.console.openSubtab({parentTabId: response.id, recordId: accountId, focus: !leadId && !contactId}) : sfConsole.openSubtab(response.id, '/' + accountId, !leadId && !contactId));
            oppId && (ctx.ForceUI.IsLightning ? ltngApi.console.openSubtab({parentTabId: response.id, recordId: oppId, focus: !leadId && !contactId}) : sfConsole.openSubtab(response.id, '/' + oppId, !leadId && !contactId));
            caseId && (ctx.ForceUI.IsLightning ? ltngApi.console.openSubtab({parentTabId: response.id, recordId: caseId, focus: !leadId && !contactId}) : sfConsole.openSubtab(response.id, '/' + caseId, !leadId && !contactId));
          };

          if (ctx.ForceUI.IsLightning) {
            ltngApi.console.getFocusedPrimaryTabId(onFocusedPrimaryTabHandler, function (error) { LogUtil.error("ERROR::" + error)});
          } else {
            sfConsole.getFocusedPrimaryTabId(onFocusedPrimaryTabHandler);
          }

          connect.agent(function(agent){
            Settings.getStringValues().then(function (ccSettings) {
              var connectPhoneFormat = JSON.parse(ccSettings["/reqConnectSFCCPOptions/reqConnectPhoneFormat"]);
              var phoneParsed = PhoneLib.parse(phone, { country: { default: connectPhoneFormat.Country }});

              LogUtil.info("CallCampaign:DialAgent:Parsed:"+phoneParsed.country+"|"+phoneParsed.phone);
              var e164PhoneNumber = PhoneLib.format(phoneParsed.phone, phoneParsed.country, connectPhoneFormat.NF);

              LogUtil.info("CallCampaign:DialAgent:h164Number:"+ e164PhoneNumber);
              agent.connect(connect.Address.byPhoneNumber(e164PhoneNumber),{});

              cti.showSoftphone();
            });
          });
        });
      });
    };

    if (ctx.ForceUI.IsLightning) {
      ltngApi.events.workAccepted.bind(onWorkItemAccepted);
    } else {
      sfConsole.addEventListener(
        sfConsole.ConsoleEvent.PRESENCE.WORK_ACCEPTED,
        onWorkItemAccepted
      );
    }
  });
})(this);
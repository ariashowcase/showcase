/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/promise', 'utils/js/logUtil', 'lib/libphonenumber', 'connect', 'console'], function (Promise, LogUtil, PhoneLib, connect, sfConsole) {
    LogUtil.info("CallInformation initializing");

    var _controllerName = ctx.ForceUI.NamespacePrefix + 'AAC_CallInformationController';

    if (sfConsole && !sfConsole.isInConsole()) {
      LogUtil.info("CallAttributesDisplay:onAgentHandler not in console");
      return;
    }

    function showCallTransferBtn() {
      document.getElementById("transferBtn").
        classList.remove("hidden");
    }

    function hideCallTransferBtn() {
      document.getElementById("transferBtn").
        classList.add("hidden");
    }

    function handleBtnClick(contact){
      function getLightningAppView() {
        LogUtil.info("CallInformation:handleBtnClick:getLightningAppView");
        opencti && opencti.getAppViewInfo({
          callback: function(response) {
            try {
              if (response.success) {
                LogUtil.info("CallInformation:getLightningAppView:Result " + JSON.stringify(response.returnValue));
                saveTabLink({
                  tabLink: response.returnValue.url
                });
              } else {
                LogUtil.info("CallInformation:getLightningAppView:Error:" + JSON.stringify(response.errors));
              }
            } catch (ex) {
              LogUtil.error("CallInformation:getLightningAppView:Error:" + JSON.stringify(ex));
            }
          }
        });
      }

      function saveTabLink(result) {
        var properties = JSON.stringify({contactId: contact.getContactId(), url: result.tabLink});
        Visualforce.remoting.Manager.invokeAction(
          _controllerName + '.storeCallInformation',
          properties,
          function(result, event){
            LogUtil.info("CallInformation:handleBtnClick:RemoteJavascript:Send invoked");
            if(event.status){
              LogUtil.info("CallInformation:handleBtnClick:RemoteJavascript cache updated");
            }
            else if(event.type === 'exception'){
              LogUtil.info("CallInformation:handleBtnClick:RemoteJavascript:Error "+ event.message);
            }
          }
        );
      };

      function getTabLink(result) {
        LogUtil.info("CallInformation:handleBtnClick:getTabLink");
        sfConsole && sfConsole.getTabLink(sfConsole.TabLink.PARENT_AND_CHILDREN, result.id, saveTabLink);
      }

      function getFocusedPrimaryTabId() {
        LogUtil.info("CallInformation:handleBtnClick:getFocusedPrimaryTabId");
        sfConsole && sfConsole.getFocusedPrimaryTabId(getTabLink);
      }

      LogUtil.info("CallInformation:handleBtnClick button clicked");
      getFocusedPrimaryTabId();
    }

    function showTransferBtnOnRefreshApplicable(agent, contact){
      if(!contact.isInbound()){
        LogUtil.info("CallInformation:showTransferBtnOnRefreshApplicable:call not inbound");
        return;
      }
      showCallTransferBtn();
    }

    connect.agent(function (agent) {
      LogUtil.info("CallInformation:onAgentHandler invoked");

      Visualforce.remoting.Manager.invokeAction(
        _controllerName + '.isPlatformCacheAvailable',
        function(result, event) {
          if (!(event.status && result)) {
            LogUtil.warn("CallInformation:onAgentHandler cache not configured; button disabled");
            $("#transferBtn").css('display', 'none');
          }
        }
      );

      $("#transferBtn").click(function(){
        LogUtil.info("CallInformation:transferBtnClicked");
        connect.agent(function(agent){
          var contact = agent.getContacts()[0];
          handleBtnClick(contact);
        });
      });

      agent.onAfterCallWork(function(){
        hideCallTransferBtn();
      });

      if (agent.getContacts().length === 1) {
        var contact = agent.getContacts()[0];
        showTransferBtnOnRefreshApplicable(agent, contact);
      }
    });

    connect.contact(function (con) {
      con.onAccepted(function(contact) {
        LogUtil.info("CallInformation:onAccepted");
        showCallTransferBtn();
      });

      con.onEnded(function() {
        LogUtil.info("CallInformation:onEnded");
        hideCallTransferBtn();
      });
    });

    return {
      getWorkspaceUrl: function (contact, onSuccess, onError) {
        if (ctx.ForceUI.IsLightning || !sfConsole.isInConsole()) {
          onError('WorkspaceUrl not supported outside of classic console.');
          return;
        }

        Visualforce.remoting.Manager.invokeAction(
          _controllerName + '.returnCallInformation',
          contact.getOriginalContactId(),
          function(result, event){
            LogUtil.info("CallInformation:getWorkspaceUrl:RemoteJavascript:Return invoked");
            if(event.status){
              onSuccess(result);
            }
            else if(event.type === 'exception'){
              LogUtil.info("CallInformation:getWorkspaceUrl:RemoteJavascript:Error " + event.message);
              onError(event.message);
            }
          }
        );
      }
    };
  });
})(this);
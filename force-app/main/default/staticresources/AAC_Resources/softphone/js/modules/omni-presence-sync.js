/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/jquery', 'lib/promise', 'utils/js/logUtil', 'connect', 'console', 'UTILS/lightning-omni-api'], function ($, Promise, LogUtil, connect, sfconsole, ltngApi) {
    LogUtil.info("OmniPresenceSync intializing");

    if (!sfconsole.isInConsole()) {
      LogUtil.info("OmniPresenceSync not in console - disabled");
      return {};
    }

    function setConnectAgentState(stateName) {
      connect.agent(function(agent) {
        var matchingStates = agent.getAgentStates().filter(function(state) {
          return state.name.replace(" ", "_") === stateName;
        });
        if (matchingStates.length === 1 && matchingStates[0].name !== agent.getState().name) {
          agent.setState(matchingStates[0], {
            success: function() {
              LogUtil.info("OmniPresenceSync:setConnectAgentState " +
                                    "AC agent state set to " + matchingStates[0].name);
            },
            failure: function() {
              LogUtil.error("OmniPresenceSync:setConnectAgentState " +
                                    "unable to set AC agent state to " + matchingStates[0].name);
            }
          });
        }
      });
    }

    var onPresenceStatusChangedHandler = function (args) {
      LogUtil.info("OmniPresenceSync:onAgentHandler:OmniStatusChangedHandler invoked; Omni status changed to " + args.statusApiName);
      setConnectAgentState(args.statusApiName);
    };

    if (ctx.ForceUI.IsLightning) {
      ltngApi.events.presenceStatusChanged.bind(onPresenceStatusChangedHandler);
    } else {
      sfconsole.addEventListener(
        sfconsole.ConsoleEvent.PRESENCE.STATUS_CHANGED,
        onPresenceStatusChangedHandler
      );
    }

    var onLogoutHandler = function() {
      LogUtil.info("OmniPresenceSync:onAgentHandler:OmniLogoutHandler invoked; Logged out of Omni");
      setConnectAgentState("Offline");
    };
    if (ctx.ForceUI.IsLightning) {
      ltngApi.events.logout.bind(onLogoutHandler);
    } else {
      sfconsole.addEventListener(
        sfconsole.ConsoleEvent.PRESENCE.LOGOUT,
        onLogoutHandler
      );
    }

    var sps = new SObjectModel.ServicePresenceStatus();
    var servicePresenceStatusMap = new Map();
    sps.retrieve({}, function(err, records, event) {
      if (err) {
        LogUtil.error("OmniPresenceSync:onAgentHandler " +
                                "unable to retrieve Omni Presence statuses.");
      }
      else {
        records.forEach(function(record) {
          var key = record.get("DeveloperName");
          var value = record.get("Id").substr(0, 15);
          servicePresenceStatusMap.set(key, value);
        });
      }
    });

    connect.agent(function(agent) {
      agent.onStateChange(function(stateChange) {
        LogUtil.info("OmniPresenceSync:onAgentHandler:AgentOnStateChangeHandler invoked; " +
                              "AC agent state changed to " + stateChange.newState);
        var newStateName = stateChange.newState.replace(" ", "_");

        if (newStateName === "Offline") {
          var logoutCallbackHandler = function(logoutResult) {
            if (logoutResult.success) {
              LogUtil.info("ACToolkitOmniPresenceSync:onAgentHandler:AgentOnStateChangeHandler logged out of Omni");
            }
            else {
              LogUtil.error("ACToolkitOmniPresenceSync:onAgentHandler:AgentOnStateChangeHandler unable to log out of Omni");
            }
          };

          if (ctx.ForceUI.IsLightning) {
            ltngApi.omni.logout(logoutCallbackHandler, function (error) { LogUtil.warn("ACToolkitOmniPresenceSync:onAgentHandler:AgentOnStateChangeHandler unable to log out of Omni" + error.message); });
          } else {
            sfconsole.presence.getServicePresenceStatusId(function(getStatusResult) {
              if (getStatusResult.success) {
                sfconsole.presence.logout(logoutCallbackHandler);
              }
            });
          }
        }
        else if (servicePresenceStatusMap.has(newStateName)) {
          var statusId = servicePresenceStatusMap.get(newStateName);

          if (ctx.ForceUI.IsLightning) {
            ltngApi.omni.setPresenceStatus({statusId: statusId}, function () {
              LogUtil.info("OmniPresenceSync:onAgentHandler:AgentOnStateChangeHandler Omni state set to " + newStateName);
            });
          } else {
            sfconsole.presence.getServicePresenceStatusId(
              function(getStatusResult) {
                var currentStatusId = getStatusResult.statusId;
                if (currentStatusId !== statusId) {
                  sfconsole.presence.setServicePresenceStatus(
                    statusId,
                    function(setStatusResult) {
                      if (setStatusResult.success) {
                        LogUtil.info("OmniPresenceSync:onAgentHandler:AgentOnStateChangeHandler " +
                                              "Omni state set to " + newStateName);
                      }
                      else {
                        LogUtil.error("OmniPresenceSync:onAgentHandler:AgentOnStateChangeHandler " +
                                                "unable to set Omni state to " + newStateName);
                      }
                    }
                  );
                } else {
                  LogUtil.info("OmniPresenceSync:onAgentHandler:AgentOnStateChangeHandler Omni state already set");
                }
              }
            );
          }
        }
      });
    });
  });
})(this);
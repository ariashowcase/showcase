/** Copyright 2017, Aria Solutions Inc.
    *
    * All Rights Reserved
    * Customers of Aria Solutions are permitted to use and modify this
    * code in all of their Salesforce Orgs (Production, Sandboxes), but
    * any form of distribution to other Salesforce Orgs not belonging to
    * the customer require a written permission from Aria Solutions.
    */

(function (ctx) {
  ctx.Rjs.define(['lib/jquery', 'lib/promise', 'utils/js/logUtil', 'lib/libphonenumber', 'connect', 'console', 'cti-wrapper', 'MODULES/screen-pops', 'UTILS/lightning-omni-api'], function ($, Promise, LogUtil, PhoneLib, connect, sfConsole, cti, ScreenPopController, ltngApi) {
    LogUtil.info("PostCallWrapUp:initializing");

    if (sfConsole && !sfConsole.isInConsole()) {
      LogUtil.info("PostCallWrapUp:onAgentHandler not in console");
      return;
    }

    var _tabLabel = ctx.ForceUI.Labels.PostCallWrapUpTabName;
    var _namespacePrefix = ctx.ForceUI.NamespacePrefix;
    var _wrapUpTabs = {};

    function startActiveCall() {
      LogUtil.info("PostCallWrapUp:onAgentHandler:startActiveCall invoked");
      setCallContextProperty("callActive", true);

      var callStartdate = new Date();

      setCallContextProperty("callStartTime", new Date().getTime());
      setCallContextProperty("callStartdate", callStartdate.toISOString());
      setCallContextProperty("callStartDateTime", callStartdate.toISOString().substr(0, 19).replace("T", " "));
    }

    function createTask(callContact, callContext) {
      LogUtil.info("PostCallWrapUp:createTask invoked");
      var callDuration = Math.floor((callContext.callEndTime - callContext.callStartTime) / 1000);

      var taskObj = {
        entityApiName: "Task",
        CallDurationInSeconds: callDuration,
        CallObject: callContact.getContactId(),
        CallType: callContext.callType,
        Type: "Call",
        IsClosed: true,
        Status: "Completed",
        ActivityDate: callContext.callStartdate,
        Subject: callContext.callType + " - " + callContext.callQueue + " - " + callContext.callPhoneNumber,
        TaskSubtype: "Task",
        Phone: callContext.callPhoneNumber
      };

      cti.saveLog(taskObj).
        then(function (taskId) {
          var taskUpdate = {
            entityApiName: "Task",
            Id: taskId
          };

          var attributes = callContact.getAttributes();
          var recordId = attributes[ScreenPopController.getScreenPopAttributeKey()] ? attributes[ScreenPopController.getScreenPopAttributeKey()].value : null;
          if (recordId) {
            var objectType = recordId.substr(0, 3);
            switch (objectType) {
              case '001':
              case '500':
                taskUpdate.WhatId = recordId;
                break;
              case '00Q':
              case '003':
                taskUpdate.WhoId = recordId;
                break;
            }
          }

          return cti.saveLog(taskUpdate).
            catch(function (ex) {
              LogUtil.warn('Failed to update task with related records. Error: {0}; Call Context: {1}', JSON.stringify(ex), JSON.stringify(taskUpdate));
            }).
            finally(function () {
              LogUtil.info("PostCallWrapUp:createTask task saved. Id=" + taskId);
              var taskURL = "/apex/" + _namespacePrefix + "AAC_PostCallUpdateTaskLookups?id=" + taskId;

              sfConsole.getFocusedPrimaryTabId(function(result){
                var primaryTabId = result.id;
                if (primaryTabId && primaryTabId !== "null"){
                  sfConsole.openSubtab(primaryTabId , taskURL, true, _tabLabel, null, openWorkingTab);
                } else {
                  sfConsole.openPrimaryTab(null, taskURL, true, _tabLabel, openWorkingTab);
                }
              });
            });
        }).
        catch(function (ex) {
          LogUtil.error('Failed to save task. Error: {0}; Call Context: {1}', JSON.stringify(ex), JSON.stringify(callContext));
        });
    }

    function openWorkingTab(result) {
      LogUtil.info("PostCallWrapUp:openWorkingTab invoked");
      if (result.success) {
        _wrapUpTabs[result.id] = true;
      }
      else {
        LogUtil.error("PostCallWrapUp:openWorkingTab unable to open tab");
      }
    }

    function onTabClose(args) {
      var tabId = args.id;

      if (!_wrapUpTabs[tabId]) {
        LogUtil.debug("PostCallWrapUp:onTabClose tab not registered - ignore");
        return;
      }

      connect.agent(function(agent) {
        LogUtil.info("PostCallWrapUp:onTabClose invoked");
        var availableState = agent.getAgentStates().filter(function(state) {
          return state.name === "Available";
        })[0];
        agent.setState(availableState, {
          success : function() {
            LogUtil.info("PostCallWrapUp:onTabClose agent state set to Available");
          },
          failure : function() {
            LogUtil.error("PostCallWrapUp:onTabClose unable to set agent state to Available");
          }
        });
      });

      delete _wrapUpTabs[tabId];
    }

    function setCallContextProperty(name, value) {
      LogUtil.info(
        "PostCallWrapUp:setCallContextProperty setting call context property " +
        name + " to " + value
      );
      sessionStorage.setItem("CCP-" + name, value);
    }

    function clearCallContext() {
      LogUtil.info("PostCallWrapUp:clearCallContext clearing all call context");
      sessionStorage.removeItem("CCP-callActive");
      sessionStorage.removeItem("CCP-callQueue");
      sessionStorage.removeItem("CCP-callType");
      sessionStorage.removeItem("CCP-whoId");
      sessionStorage.removeItem("CCP-callStartTime");
      sessionStorage.removeItem("CCP-callEndTime");
      sessionStorage.removeItem("CCP-callStartdate");
      sessionStorage.removeItem("CCP-callStartDateTime");
      sessionStorage.removeItem("CCP-callPhoneNumber");
    }

    function getCurrentCallContext() {
      var result = {
        callActive: sessionStorage.getItem("CCP-callActive"),
        callQueue: sessionStorage.getItem("CCP-callQueue"),
        callType: sessionStorage.getItem("CCP-callType"),
        whoId: sessionStorage.getItem("CCP-whoId"),
        callStartTime: sessionStorage.getItem("CCP-callStartTime"),
        callEndTime: sessionStorage.getItem("CCP-callEndTime"),
        callStartdate: sessionStorage.getItem("CCP-callStartdate"),
        callStartDateTime: sessionStorage.getItem("CCP-callStartDateTime"),
        callPhoneNumber: sessionStorage.getItem("CCP-callPhoneNumber")
      };
      LogUtil.info("PostCallWrapUp:getCurrentCallContext Current call context: {0}", JSON.stringify(result));
      return result;
    }

    if (ctx.ForceUI.IsLightning) {
      ltngApi.events.tabClosed.bind(onTabClose);
    } else {
      sfConsole.addEventListener(
        sfConsole.ConsoleEvent.CLOSE_TAB,
        onTabClose
      );
    }


    connect.contact(function(contact) {
      var conns = contact.getConnections();
      var custConn = conns.find(
        c => c.getType() === connect.ConnectionType.INBOUND ||
        c.getType() === connect.ConnectionType.OUTBOUND
        );
      if (!custConn)
        return;

      setCallContextProperty('callType', contact.isInbound() ? 'Inbound' : 'Outbound');

      var phoneNumber = custConn.getEndpoint().phoneNumber;
      var containsAtSymbol = phoneNumber.indexOf('@') > -1;
      setCallContextProperty('callPhoneNumber', phoneNumber.substring(0, containsAtSymbol ? phoneNumber.indexOf('@') : phoneNumber.length).replace('sip:', ''));

      contact.onAccepted(function(contactOnAccepted) {
        LogUtil.info("PostCallWrapUp:onAgentHandler:ContactOnAcceptedHandler invoked");
        startActiveCall();
      });

      contact.onConnected(function(contactOnConnected) {
        LogUtil.info("PostCallWrapUp:onAgentHandler:ContactOnConnectedHandler invoked");

        /*
         * Due to how callback works need to add a check so that it triggers
         * the startActiveCall() method. Callbacks are always inbound but hit
         * here when connecting to the customer so need to add a check to see
         */
        if (!contactOnConnected.isInbound() || contactOnConnected.getType() === 'queue_callback') {
          startActiveCall();
        }
      });

      contact.onEnded(function(contactOnEnded) {
        LogUtil.info("PostCallWrapUp:onAgentHandler:ContactOnEndedHandler invoked");
        setCallContextProperty("callEndTime", new Date().getTime());
        setCallContextProperty("callQueue", contact.getQueue().name);

        var callContext = getCurrentCallContext();

        clearCallContext();
        if (callContext.callActive) {
          createTask(contactOnEnded, callContext);
        }
      });
    });
  });
})(this);
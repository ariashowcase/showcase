/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/jquery', 'lib/promise', 'utils/js/logUtil', 'connect', 'console'], function ($, Promise, LogUtil, connect, sfConsole) {

    const _MAX_WIDTH_CONSOLE = 110;

    var _callAttributeConfigurationPromise;

    if (sfConsole && !sfConsole.isInConsole()) {
      LogUtil.info("CallAttributesDisplay:onAgentHandler not in console");
      return;
    }

    function clearCallAttributes() {
      $('#callAttributesList').empty();
    }

    function createTruncatedDiv(text) {
      return $('<div></div>').
        addClass('slds-truncate').
        attr('title', text).
        css('max-width', _MAX_WIDTH_CONSOLE + 'px').
        text(text);
    }

    function createTd(text) {
      return $('<td></td>').
        attr('data-label', text).
        append(createTruncatedDiv(text));
    }

    function addTableRow(key, value) {
      var $tr = $('<tr></tr>').
        append(createTd(key)).
        append(createTd(value)).
        appendTo('#callAttributesList');
    }

    function showCallAttributes() {
      $('#callAttributesPopoverContainer').removeClass('hidden');
    }

    function hideCallAttributes() {
      $('#callAttributesPopoverContainer').addClass('hidden');
    }

    function showCallAttributeBadge() {
      $('#callAttributesBadge').removeClass('hidden');
    }

    function isCallAttributeBadgeVisiable() {
        return $('#callAttributesBadge').
          hasClass('hidden') === false;
      }

    function hideCallAttributeBadge() {
      $('#callAttributesBadge').
        addClass('hidden');
    }

    function handleBadgeClicked() {
      $('#callAttributesPopoverContainer').
        toggle('hidden');
    }

    function createCallAttributesForContactIfApplicable(contact) {
      if (!contact.isInbound()) {
        LogUtil.info("CallAttributesDisplay:createCallAttributesForContactIfApplicable:call not inbound");
        return;
      }

      var attributes = contact.getAttributes();
      var numAttributes = Object.keys(attributes).length;

      LogUtil.info("CallAttributesDisplay:createCallAttributesForContactIfApplicable:numAttributes=" + numAttributes);
      if (numAttributes === 0) {
        hideCallAttributeBadge();
        return;
      }

      _callAttributeConfigurationPromise.
        then(function (config) {
          var shouldShowBadge = false;
          for (var key in attributes) {
            let attributeRecord = attributes[key];

            var attrConfig = config[attributeRecord.name];
            if (attrConfig) {
              addTableRow(attrConfig.label, attributeRecord.value);
              shouldShowBadge = true;
            }
          }

          if (shouldShowBadge) {
            showCallAttributeBadge();
          }
        }).
        catch(function () {
          hideCallAttributeBadge();
        });
    }

    connect.agent(function (agent) {
      LogUtil.info("CallAttributesDisplay:onAgentHandler");
      $('#callAttributesBadge').click(handleBadgeClicked);

      _callAttributeConfigurationPromise = new Promise(function (resolve, reject) {
        var callAttributeSO = new SObjectModel.Call_Attributes_Configuration();
        callAttributeSO.retrieve({}, function(err, records, event) {
          if (err) {
            LogUtil.error("CallAttributesDisplay:retrieveCallAttributeConfiguration unable to retrieve Call Attribute Configuration statuses.", err);

            reject(err);
          }
          else {
            var callAttributeConfiguration = [];
            records.forEach(function(record) {
              var key = record.get("Attribute_Name");
              var label = record.get("CCP_Attribute_Label");
              callAttributeConfiguration[key] = {'label': label};
            });

            LogUtil.info("CallAttributesDisplay:retrieveCallAttributeConfiguration:callAttributeConfiguration retrieved: " + JSON.stringify(callAttributeConfiguration));

            resolve(callAttributeConfiguration);
          }

          connect.agent(function (agent) {
            if (agent.getContacts().length === 1) {
              var contact = agent.getContacts()[0];
              createCallAttributesForContactIfApplicable(contact);
            }
          });
        });
      });
    });

    connect.contact(function(con) {
      LogUtil.info("CallAttributesDisplay:onContactHandler");

      con.onConnecting(function(contact) {
        LogUtil.info("CallAttributesDisplay:onConnecting");
        createCallAttributesForContactIfApplicable(contact);
      });

      con.onAccepted(function(contact) {
        LogUtil.info("CallAttributesDisplay:onAccepted");
        if (isCallAttributeBadgeVisiable()) {
          showCallAttributes();
        }
      });

      con.onEnded(function() {
        LogUtil.info("CallAttributesDisplay:onEnded");
        hideCallAttributes();
        clearCallAttributes();
        hideCallAttributeBadge();
      });
    });
  });
})(this);

/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/jquery', 'lib/promise', 'utils/js/logUtil', 'UTILS/softphone-settings', 'connect'], function ($, Promise, LogUtil, Settings, connect) {

    function initCCP(containerId, config) {
      LogUtil.debug("Initializing CCP with config: {0}", JSON.stringify(config))
      connect.core.initCCP($('#' + containerId)[0], config);
      connect.core.initSoftphoneManager({allowFramedSoftphone: true});
    }

    function fromConfig(containerId, config) {
      return new Promise(function (resolve) {
        initCCP(containerId, config);

        resolve(config);
      });
    }

    function fromCallCenterSettings(containerId) {
      LogUtil.debug("Initialing CCP from Call Center Settings");
      return getCCPConfig().
        then(function (config) {
          initCCP(containerId, config);

          return config;
        });

    }

    function getCCPConfig() {
      return Settings.getStringValues().then(function (ccSettings) {
        LogUtil.debug("Call Center Settings successfully retrieved");
        var defaultSettings =  {
          loginPopup: false,
          softphone: {
            allowFramedSoftphone: false
          }
        };

        return $.extend(true, {}, defaultSettings, {
          ccpUrl: ccSettings['/reqConnectSFCCPOptions/reqConnectURL'] // TODO: consider to change the path
        });
      });
    }

    return {
      getCCPConfig: getCCPConfig,
      fromConfig: fromConfig,
      fromCallCenterSettings: fromCallCenterSettings
    }
  });
})(this);
/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */
if (typeof (window.aria) === "undefined") {
  window.aria = {
    log: {
      error: function (message) {
        console.log("X-Win ERROR > " + message);
      },
      info: function (message) {
        console.log("X-Win INFO > " + message);
      },
      debug: function (message) {
        console.log("X-Win DEBUG > " + message);
      }
    }
  };
}

window.aria.crossWindowProtocol = function (messageProtocol) {

  // checkNotNull and checkType were copied here to allow this file to work without dependencies, in case this is deployed outside of CIMplicity
  function checkNotNull(value, argName) {
    if (value == null) {
      throw "Required argument '" + argName + "' not found";
    }
    return value;
  };

  function checkType(value, type, argName) {
    if (typeof value != type) {
      throw "Required argument '" + argName + "' is not of type '" + type + "', it is of type'" + typeof value + "'";
    }
    return value;
  };


  var _messageProtocol = checkNotNull(messageProtocol, "messageProtocol");
  var _messageId = 0;
  var _registeredCallbacks = {};
  var _registeredHandlers = {};

  var _RETURNCOMMAND = "_return";

  function getDefaultOrigin() {
    return window.location.origin || (window.location.protocol + "//" + window.location.host);
  };

  function init() {
    _messageProtocol.addEventListener(function (event) {
      var message = null;
      try {
        var message = _messageProtocol.parseEvent(event);
        if (!message.command) {
          throw "X-Window-Message was not sent by aria.crossWindowProtocol";
        }
      } catch (ex) {
        return;
      }

      aria.log.debug("Received message '" + message.command + "' with id '" + message.id + "' from origin '" + event.origin + "'");

      if (_registeredHandlers[message.command]) {
        var source = event.src || event.source;
        var origin = event.origin && event.origin.toLowerCase();

        function postbackResult(result) {
          var resultMessage = { id: message.id, command: _RETURNCOMMAND };

          if (result && result.exception) {
            resultMessage.exception = result.exception;
          }
          else {
            resultMessage.result = result;
          }
          _messageProtocol.postMessage(source, resultMessage, origin);
        }

        try {
          var handlers = _registeredHandlers[message.command];

          for (var handlerIndex = 0, totalHandlers = handlers.length; handlerIndex < totalHandlers; handlerIndex++) {
            var handlerData = handlers[handlerIndex];
            var allowedDomains = handlerData.allowedDomains;

            for (var domainIndex = 0, totalDomains = allowedDomains.length; domainIndex < totalDomains; domainIndex++) {
              var theDomain = allowedDomains[domainIndex].toLowerCase();

              // 'source' and 'origin' properties are not set when running inside the Salesforce Console. The security is handled through the domain
              // whitelist of the Console Application and the Salesforce APIs. As a result, if source and origin are undefined, continue to invoke the handlers.
              if (theDomain === origin || theDomain === "*" || (source === undefined && origin === undefined)) {
                handlerData.handler(message.parameters, postbackResult, source);
                break;
              }
            }
          }
        }
        catch (ex) {
          postbackResult({ exception: ex });
        }
      }
      else if (message.command === _RETURNCOMMAND) {
        if (_registeredCallbacks[message.id]) {
          var callbackFunctions = _registeredCallbacks[message.id];
          delete _registeredCallbacks[message.id];

          if (message.exception && !!callbackFunctions.error) {
            callbackFunctions.error(message.exception);
          }
          else if (!!callbackFunctions.success) {
            callbackFunctions.success(message.result);
          }
        }
      }
    }, false);
  }

  init();

  return {
    post: function (args) {
      var command = checkNotNull(args.command, "command");
      var targetWindow = args.targetWindow;
      var parameters = args.parameters;
      var onSuccess = args.successCallback;
      var onError = args.errorCallback;
      var onTimeout = args.timeoutCallback;
      var timeout = args.timeout ? args.timeout : 30000;
      var targetDomain = args.targetDomain || getDefaultOrigin();

      var id = _messageId++;

      var callbacks = {};
      if (typeof onSuccess === "function") {
        callbacks.success = onSuccess;
      }
      if (typeof onError === "function") {
        callbacks.error = onError;
      }
      if (typeof onTimeout === "function") {
        callbacks.timeout = onTimeout;
      }

      if (callbacks.success || callbacks.error) {
        _registeredCallbacks[id] = callbacks;

        setTimeout(function () {
          if (_registeredCallbacks[id]) {
            if (_registeredCallbacks[id].timeout) {
              _registeredCallbacks[id].timeout("A timeout occurred when posting the command " + command + " with parameters " + JSON.stringify(parameters));
            } else if (_registeredCallbacks[id].error) {
              _registeredCallbacks[id].error("A timeout occurred when posting the command " + command + " with parameters " + JSON.stringify(parameters));
            }
          }
          delete _registeredCallbacks[id];
        }, timeout);
      }

      // This setTimeout call is necessary for IE 8/9 to address the following scenario
      // Window A has a frame containing window B.
      // Window A pops window C, which is in the same domain as A.
      // If C calls a function in A that posts to window B, then the message event.source will reference window C as opposed to A
      // In Firefox and Chrome, event.source will actually be window A
      // The setTimeout ensures that the postMessage is made from window A, resulting in the correct source in IE
      var message = { id: id, command: command, parameters: parameters };
      setTimeout(function () { _messageProtocol.postMessage(targetWindow, message, targetDomain, onError); }, 0);
    },
    registerHandler: function (command, handler, allowedDomains) {
      if (command === _RETURNCOMMAND) {
        throw _RETURNCOMMAND + " is a reserved command";
      }

      if (!allowedDomains) {
        allowedDomains = [getDefaultOrigin()];
      }
      if (typeof allowedDomains === "string") {
        allowedDomains = [allowedDomains];
      }

      var handlerData = { handler: checkType(handler, "function", "handler"), allowedDomains: allowedDomains };
      if (_registeredHandlers[command]) {
        _registeredHandlers[command].push(handlerData);
      }
      else {
        _registeredHandlers[command] = [handlerData];
      }
    },
    removeHandler: function (command, toRemove) {
      var handlers = _registeredHandlers[command];
      if (!handlers) {
        return;
      }

      var newHandlers = [];
      for (var i = 0, total = handlers.length; i < total; i++) {
        var aHandler = handlers[i];
        if (aHandler.handler !== toRemove) {
          newHandlers.push(aHandler);
        }
      }

      _registeredHandlers[command] = newHandlers;
    }
  };
};

window.aria.crossWindowMessage = new aria.crossWindowProtocol({
  addEventListener: function(handler) {
    window.addEventListener("message", handler);
  },
  postMessage: function(targetWindow, message, targetDomain, errorCallback) {
    try {
      aria.log.debug("Posting command '" + message.command + "' with id '" + message.id + "' from '" + window.location.href + "' to target domain '" + targetDomain + "'");
      targetWindow.postMessage(JSON.stringify(message), targetDomain);
    } catch (ex) {
      aria.log.error("Error posting command " + message.command + " with message id: " + message.id + ": " + ex);
      if (typeof errorCallback === "function") {
        errorCallback(ex);
      }
    }
  },
  parseEvent: function(event) {
    return typeof (event.data) === "string" ? JSON.parse(event.data) : event.data;
  }
});
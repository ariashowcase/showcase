/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 10/4/2018.
 */

@isTest
private class AWS_ConnectContactTest {
  class AcCalloutMock implements HttpCalloutMock {
    Boolean isSuccess;

    AcCalloutMock(Boolean isSuccess) {
      this.isSuccess = isSuccess;
    }

    public HttpResponse respond(HttpRequest req) {
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/json');
      if (isSuccess) {
        res.setStatusCode(200);
        res.setBody('{"ContactId": "6789"}');
      }
      else {
        res.setStatusCode(400);
      }
      return res;
    }
  }

  static AWS_ConnectContact.StartContactRequestBody createStartContactRequestBody() {
    AWS_ConnectContact.StartContactRequestBody body = new AWS_ConnectContact.StartContactRequestBody();
    body.Attributes = new Map<String, String>();
    body.ClientToken = '';
    body.ContactFlowId = '1234';
    body.DestinationPhoneNumber = '+17805550163';
    body.InstanceId = '111';
    body.QueueId = '222';
    body.SourcePhoneNumber = '';
    return body;
  }
  
  @isTest
  static void testStartOutboundVoiceContactSuccess() {
    Test.setMock(HttpCalloutMock.class, new AcCalloutMock(true));

    AWS_Connector connector = new AWS_Connector('aaa', 'bbb', 'us-east-1');
    AWS_ConnectContact contactConnector = new AWS_ConnectContact(connector);
    String contactId = contactConnector.StartOutboundVoiceContact(createStartContactRequestBody());

    System.assertEquals('6789', contactId);
    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  static void testStartOutboundVoiceContactFail() {
    Test.setMock(HttpCalloutMock.class, new AcCalloutMock(false));

    try {
      AWS_Connector connector = new AWS_Connector('aaa', 'bbb', 'us-east-1');
      AWS_ConnectContact contactConnector = new AWS_ConnectContact(connector);
      String contactId = contactConnector.StartOutboundVoiceContact(createStartContactRequestBody());
      System.assert(false, 'Should have thrown AWS_ConnectRequestException.');
    }
    catch (AWS_ConnectContact.AWS_ConnectRequestException e) {
      System.assert(true);
    }
    catch (Exception e) {
      System.assert(false, 'Should have thrown AWS_ConnectRequestException.');
    }
  }

  static AWS_ConnectContact.StopContactRequestBody createStopContactRequestBody() {
    AWS_ConnectContact.StopContactRequestBody body = new AWS_ConnectContact.StopContactRequestBody();
    body.ContactId = '6789';
    body.InstanceId = '111';
    return body;
  }

  @isTest
  static void testStopContactSuccess() {
    Test.setMock(HttpCalloutMock.class, new AcCalloutMock(true));

    AWS_Connector connector = new AWS_Connector('aaa', 'bbb', 'us-east-1');
    AWS_ConnectContact contactConnector = new AWS_ConnectContact(connector);
    contactConnector.StopContact(createStopContactRequestBody());

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  static void testStopContactFail() {
    Test.setMock(HttpCalloutMock.class, new AcCalloutMock(false));

    try {
      AWS_Connector connector = new AWS_Connector('aaa', 'bbb', 'us-east-1');
      AWS_ConnectContact contactConnector = new AWS_ConnectContact(connector);
      contactConnector.StopContact(createStopContactRequestBody());
    }
    catch (AWS_ConnectContact.AWS_ConnectRequestException e) {
      System.assert(true);
    }
    catch (Exception e) {
      System.assert(false, 'Should have thrown AWS_ConnectRequestException.');
    }
  }
}
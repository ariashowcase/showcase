/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Thursday August 8th 2019
 * Author: varonov
 * File type: '.cls'
 */

@isTest
private class SC_TripsControllerTest {

  private static Account testAccount {
    get {
        if(testAccount == null) {
        testAccount = [SELECT Id FROM Account LIMIT 1];
        }
        return testAccount;
    }
    set;
  }

  @testSetup
  private static void setup() {
    testAccount = ATU_AccountFactory.createDefaultBuilder('testAccount').persist();
  }

  @isTest
  private static void getTripsTest() {
    createSeats();

    Test.startTest();
    List<Trip__c> trips = SC_TripsController.getTrips(testAccount.Id);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(1, [SELECT Id FROM Trip__c WHERE Account__c = :testAccount.Id].size());
    System.assertEquals(1, [SELECT Id FROM Booked_Seat__c].size());
    System.assertEquals(1, [SELECT Id FROM Flight__c].size());
  }

  // Helper methods

  private static Id createTrip() {
    Trip__c testTrip = new Trip__c(Account__c = testAccount.Id);
    insert testTrip;
    return testTrip.Id;
  }

  private static void createSeats()  {
    insert new Booked_Seat__c(Trip__c = createTrip(), Flight__c = createFlight());
  }

  private static Id createFlight() {
    Flight__c testFlight = new Flight__c();
    insert testFlight;
    return testFlight.Id;
  }
}

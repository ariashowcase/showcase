/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 04-Sep-18. 
 */

@isTest
public class AU_EmailUtilTest {

  @isTest static void getEmailTemplateByNameTest_Success() {
    EmailTemplate template = createEmailTemplate();

    System.assertEquals(AU_EmailUtil.getEmailTemplateByName(template.Name).Id, [SELECT Id FROM EmailTemplate WHERE Name = :template.Name LIMIT 1].Id);
  }

  @isTest static void getEmailTemplateByNameTest_Failure() {
    Test.startTest();
    Boolean isExceptionThrown = false;
    try {
      AU_EmailUtil.getEmailTemplateByName(null);
    } catch (AU_EmailUtil.AU_EmailResourceNotFoundException ex) {
      isExceptionThrown = true;
    }
    Test.stopTest();
    System.assertEquals(true, isExceptionThrown);
  }

  @isTest static void getFromEmailAddressTest_Success() {
    OrgWideEmailAddress emailAddress = [SELECT DisplayName FROM OrgWideEmailAddress LIMIT 1];

    System.assertEquals(AU_EmailUtil.getFromEmailAddress(emailAddress.DisplayName).Id, [SELECT Id FROM OrgWideEmailAddress LIMIT 1].Id);
  }

  @isTest static void getFromEmailAddressTest_Failure() {
    Test.startTest();
    Boolean isExceptionThrown = false;
    try {
      AU_EmailUtil.getFromEmailAddress(null);
    } catch (AU_EmailUtil.AU_EmailResourceNotFoundException ex) {
      isExceptionThrown = true;
    }
    Test.stopTest();
    System.assertEquals(true, isExceptionThrown);
  }

  static EmailTemplate createEmailTemplate() {
    EmailTemplate newTemplate = new EmailTemplate(
        Name = 'Test Template',
        DeveloperName = 'TestTemplate',
        TemplateType = 'text',
        FolderId = UserInfo.getUserId(),
        IsActive = true
    );
    insert newTemplate;
    return newTemplate;
  }

}
@RestResource(urlMapping='/Trips/*')
global with sharing class SC_TripRestResource {
  @HttpPost
  global static SC_TripService.OrderConfirmation doPost(CreateTripsRequest request) {
    SC_TripService.OrderConfirmation confirmation;
    if (request.seatOptions != null) {
      confirmation = SC_TripService.bookTrips(request.accountId, request.contactIds, request.flightIds, request.seatOptions);
    }
    else {
      confirmation = SC_TripService.bookTrips(request.accountId, request.contactIds, request.flightIds);
    }
    return confirmation;
  }

  @HttpPut
  global static SC_TripService.OrderConfirmation doReschedule(ChangeTripRequest request) {
    SC_TripService.OrderConfirmation confirmation;
    if (request.confirmationNumber != null) {
      confirmation = SC_TripService.rescheduleTrips(request.confirmationNumber, request.flightIds);
    }     
    return confirmation;
  }

  @HttpGet
  global static Trip__c doGet() {
    String confirmationNo = RestContext.request.params.get('confirmationNo');

    if (String.isBlank(confirmationNo)) {
      return null;
    }

    return SC_TripService.getTrip(confirmationNo);
  }

  @HttpDelete 
  global static void doDelete() {
    Id tripId = Id.valueOf(RestContext.request.params.get('tripId'));
    Boolean cancelOrder = RestContext.request.params.get('cancelOrder') != null;

    SC_TripService.cancelTrip(tripId, cancelOrder);
  }

  global class CreateTripsRequest {
    Id accountId;
    List<Id> contactIds;
    List<List<Id>> flightIds;
    List<SC_TripService.SeatOptions> seatOptions;
  }

  global class ChangeTripRequest  {
    String confirmationNumber;
    List<List<Id>> flightIds;
    List<SC_TripService.SeatOptions> seatOptions;
    List<Id> contactIds;
  }
}
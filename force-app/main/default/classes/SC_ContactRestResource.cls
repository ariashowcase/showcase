@RestResource(urlMapping='/Contacts/*')
global with sharing class SC_ContactRestResource {
  @HttpGet 
  global static List<Contact> doGet() {
    String phone = RestContext.request.params.get('phone');
    String accountId = RestContext.request.params.get('accountId');
    
    if (String.isNotBlank(accountId)) {
      return SC_ContactService.getContactsByAccountId(Id.valueOf(accountId));
    }
    if (String.isNotBlank(phone)) {
      return SC_ContactService.getContactsByPhone(phone);
    }

    return new List<Contact>();
  }
}

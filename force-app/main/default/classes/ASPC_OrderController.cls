/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 25-Jul-18. 
 */

public with sharing class ASPC_OrderController {
  @AuraEnabled
  public static List<Opportunity> getOrders(Id thisAccountId) {
    if(thisAccountId == null) {
      thisAccountId = [SELECT ContactId, AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
    }
    return [
        SELECT Name, StageName, CreatedDate
        FROM Opportunity
        WHERE AccountId = :thisAccountId];
  }

  @AuraEnabled
  public static Boolean isObjectAccessible()  {
    return Schema.sObjectType.Opportunity.isAccessible();
  }
}
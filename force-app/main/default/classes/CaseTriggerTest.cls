/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */
@isTest
private class CaseTriggerTest {
  private static CaseTriggerTest.TriggerHandler handler = new CaseTriggerTest.TriggerHandler();

  @isTest static void test() {
    ASP_TriggerBus.resetHandlersWith(new Map<SObjectType, AU_TriggerEventDispatcher>{
      Case.getSObjectType() => new AU_TriggerEventDispatcher(new List<AU_TriggerEntry.ITriggerEntry>{
        handler
      })
    });

    System.assertEquals(0, handler.MainCounter);
    System.assertEquals(0, handler.ProgressCounter);
    System.assertEquals(0, handler.CompletedCounter);

    Test.startTest();
    insert new Case(
      Subject = 'Test subject',
      Status = 'New',
      Origin = 'Web'
    );
    Test.stopTest();

    System.assert(handler.MainCounter > 1); // before and after insert
    System.assertEquals(0, handler.ProgressCounter);
    System.assert(handler.CompletedCounter > 1); // before and after insert
  }

  public class TriggerHandler implements AU_TriggerEntry.ITriggerEntry {

    public Integer MainCounter { get; private set; }
    public Integer ProgressCounter { get; private set; }
    public Integer CompletedCounter { get; private set; }

    public TriggerHandler() {
      MainCounter = 0;
      ProgressCounter = 0;
      CompletedCounter = 0;
    }

    public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount) {
      MainCounter++;
    }
    public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {
      ProgressCounter++;
    }
    public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {
      CompletedCounter++;
    }
  }
}
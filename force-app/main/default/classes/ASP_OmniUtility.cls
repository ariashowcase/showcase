public class ASP_OmniUtility {
  @AuraEnabled
  public static string getOmniStates(){
    List<SObject> states;
    string omniJson;
    try {
      states = Database.query('SELECT Id, DeveloperName from ServicePresenceStatus WHERE Id <> \'0N51U0000000FWUSA2\'');
    }
    catch(Exception ex) {
      omniJson = 'null';
      return omniJson;
    }

    Map<string, string> stateMap = new Map<string, string>();

    for(SObject s : states) {
      stateMap.put((string)s.get('DeveloperName'), (string)s.get('Id'));
    }

    if(stateMap.size() > 0)
      omniJson = JSON.serialize(stateMap);
    else
        omniJson = 'null';

    return omniJson;
  }
}
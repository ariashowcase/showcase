/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@isTest
private class ASP_UpdateSentimentInvokerTest {

  @isTest static void testExecute_Failure() {

    Test.setMock(HttpCalloutMock.class, new ATU_MultiHttpCalloutMock(createRequestMapping()));

    ASP_UpdateSentimentInvoker.tokenProvider = new TokenProviderMock();

    Case c = createCase('This is not great!');

    Test.startTest();
    ASP_UpdateSentimentInvoker.updateSentiments(createArgs(c));
    Test.stopTest();

    ATU_DebugInfoUtil.assertErrorsRecorded();
  }

  private static List<ASP_UpdateSentimentInvoker.SentimentArg> createArgs(Case c) {
    ASP_UpdateSentimentInvoker.SentimentArg arg = new ASP_UpdateSentimentInvoker.SentimentArg();
    arg.recordId = c.Id;
    arg.text = c.Description;
    arg.objectType = 'Case';
    arg.sentimentFieldName = 'Subject';

    return new List<ASP_UpdateSentimentInvoker.SentimentArg> {
      arg
    };
  }

  private static Case createCase(String description) {
    Case c = new Case(
      Subject = 'Foo bar',
      Description = description
    );
    insert c;

    return c;
  }

  private static Map<HttpRequest, HttpResponse> createRequestMapping() {
    return new Map<HttpRequest, HttpResponse> {
      createTokenProviderRequest() => createTokenProviderResponse(),
      createSentimentRequest() => createSentimentResponse()
    };
  }

  private static HttpRequest createTokenProviderRequest() {
    HttpRequest req = new HttpRequest();
    req.setMethod('POST');
    req.setEndpoint('https://api.einstein.ai/v2/oauth2/token');
    req.setHeader('Content-type', 'application/x-www-form-urlencoded');

    return req;
  }

  private static HttpResponse createTokenProviderResponse() {
    HttpResponse resp = new HttpResponse();
    resp.setStatusCode(200);
    resp.setBody('{ "access_token": "foo-bar" }');

    return resp;
  }

  private static HttpRequest createSentimentRequest() {
    HttpRequest req = new HttpRequest();
    req.setMethod('POST');
    req.setEndpoint('https://api.einstein.ai/v2/language/sentiment');
    req.setHeader('Content-type', HttpFormDataBodyPart.GetContentType());

    return req;
  }

  private static HttpResponse createSentimentResponse() {
    HttpResponse resp = new HttpResponse();
    resp.setStatusCode(200);
    resp.setBody('{"probabilities":[{"label":"positive","probability":0.8673582},{"label":"negative","probability":0.1316828},{"label":"neutral","probability":9.590242E-4}],"object":"predictresponse"}');

    return resp;
  }

  public class TokenProviderMock implements TokenProvider {

    public String getToken() {
      throw new ASP_UpdateSentimentInvokerTest.TokenProviderMockException();
    }
  }

  public class TokenProviderMockException extends Exception { }
}
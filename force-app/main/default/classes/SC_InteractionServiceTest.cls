/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Friday November 8th 2019
 * Author: varonov
 * File type: '.cls'
 */

@isTest
private class SC_InteractionServiceTest {
  @isTest
  private static void createAndUpdateTaskTest()  {
    Id taskId = SC_InteractionService.createTask('121a6067-ae43-4af2-b06f-6a50374d298f', datetime.now(), createContact(), 'inbound', 'sms', '+15873557154');
    System.assertEquals(1, [SELECT Id FROM Task].size());
    System.assertEquals('In Progress', [SELECT Id, Status FROM Task].Status);
    Id updatedTaskId = SC_InteractionService.updateTask(taskId, datetime.now(), datetime.now().addMinutes(5), 'Showcase Dev', 'Book a Flight', 5);
    System.assertEquals(1, [SELECT Id FROM Task].size());
    System.assertEquals('Completed', [SELECT Id, Status FROM Task].Status);
  }

  private static Id createContact() {
    Contact con = new Contact(LastName = 'LastName');
    insert con;
    return con.Id;
  } 
}

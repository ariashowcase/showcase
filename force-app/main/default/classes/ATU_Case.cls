/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@IsTest
public class ATU_Case {

  public static ATU_Case.Builder build(String subject) {
    return new ATU_Case.Builder(subject, null);
  }

  public static ATU_Case.Builder build(String subject, String accountId) {
    return new ATU_Case.Builder(subject, accountId);
  }
  
  public static ATU_Case.Builder build(fflib_SObjectUnitOfWork uow, String subject, Account a) {
    ATU_Case.Builder builder = new ATU_Case.Builder(subject, null);
    uow.registerNew(builder.theRecord, Case.AccountId, a);
    return builder;
  }

  public class Builder {

    private final Case theRecord;

    private Builder(String subject, String accountId) {
      theRecord = new Case(
        Subject = subject,
        AccountId = accountId
      );
    }

    public ATU_Case.Builder withRecordType(String recordType) {
      // The RecordTypeId field will not exist until a record type is defined.
      theRecord.put('RecordTypeId', AU_RecordTypes.getId('Case', recordType));
      return this;
    }

    public ATU_Case.Builder withOwnerId(String ownerId) {
      theRecord.OwnerId = ownerId;
      return this;
    }

    public ATU_Case.Builder withField(String fieldName, Object value) {
      theRecord.put(fieldName, value);
      return this;
    }

    public Case create() {
      return theRecord;
    }

    public Case persist() {
      insert theRecord;
      return theRecord;
    }

    public Case registerNew(AU_UnitOfWork uow) {
      uow.registerNew(theRecord);
      return theRecord;
    }

    public Case monitorNew(AU_UnitOfWork uow) {
      uow.monitorNew(theRecord);
      return theRecord;
    }
  }
}
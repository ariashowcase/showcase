/** Copyright 2017, Aria Solutions Inc.
*
* All Rights Reserved
* Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

/*
  handles the creation of the interaction (if required) and interaction segments.
  Properties attribute information comes from javascript class called call-wrap-up-interaction.
 */

global with sharing class AAC_PostCallSegmentCreationController {

  public AAC_PostCallSegmentCreationController(AAC_NamespaceController controller){}

  @RemoteAction
  global static ASP_Interaction_Segment__c createInteractionSegment(String properties){

    AWSInfo contactInformation = parse(properties);

    String originalContactId = getContactId(contactInformation);

    String interactionExtId = getOrCreateInteration(originalContactId);

    ASP_Interaction__c interaction = new ASP_Interaction__c(Interaction_Id__c=interactionExtId);
    ASP_Interaction_Segment__c segment = new ASP_Interaction_Segment__c(
      Interaction_Segment_Id__c = contactInformation.contactId,
      RecordTypeId = AU_RecordTypes.getId('ASP_Interaction_Segment__c', 'Phone')
    );

    segment.ASP_Interaction__r = interaction;

    upsert segment;

    return segment;
  }

  private static String getContactId(AWSInfo contactInformation){
    String contactId;
    if(contactInformation.attId != null){
      contactId = contactInformation.attId;
    }
    else if(contactInformation.originalContactId != null){
      contactId = contactInformation.originalContactId;
    }
    else {
      contactId = contactInformation.contactId;
    }

    return contactId;
  }

  private static String getOrCreateInteration(String contactId)
  {
    List<ASP_Interaction__c> interaction = new List<ASP_Interaction__c>();
    interaction = [SELECT Interaction_Id__c FROM ASP_Interaction__c WHERE Interaction_Id__c =: contactId];
    if(interaction.size() == 0){
      interaction.add(new ASP_Interaction__c(Interaction_Id__c = contactId));
      insert interaction;
    }

    return interaction[0].Interaction_Id__c;
  }

  private static AWSInfo parse(String json) {
    return (AWSInfo) System.JSON.deserialize(json, AWSInfo.class);
  }

  public class AWSInfo{
    public String contactId;
    public String originalContactId;
    public String attId;

  }
}
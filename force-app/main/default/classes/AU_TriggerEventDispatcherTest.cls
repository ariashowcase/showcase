/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@isTest
class AU_TriggerEventDispatcherTest {
  private static AU_TriggerEventDispatcher dispatcher;
  
  @isTest
  static void testDispatch_Success() {
    AU_TriggerEventDispatcherTest.ITestTriggerEntry entry1 = new AU_TriggerEventDispatcherTest.SuccessTriggerEntry();
    AU_TriggerEventDispatcherTest.ITestTriggerEntry entry2 = new AU_TriggerEventDispatcherTest.SuccessTriggerEntry();
    
    dispatcher = new AU_TriggerEventDispatcher(new List<AU_TriggerEntry.ITriggerEntry> { entry1, entry2 });
    
    Test.startTest();
    dispatcher.dispatch(createArgs());
    Test.stopTest();
    
    System.assertEquals(1, entry1.getMainInvocationCount());
    System.assertEquals(1, entry2.getMainInvocationCount());
    System.assertEquals(0, entry1.getProgressInvocationCount());
    System.assertEquals(0, entry2.getProgressInvocationCount());
  }
  
  @isTest
  static void testDispatch_OneHandlerFails() {
    AU_TriggerEventDispatcherTest.ITestTriggerEntry entry1 = new AU_TriggerEventDispatcherTest.SuccessTriggerEntry();
    AU_TriggerEventDispatcherTest.ITestTriggerEntry entry2 = new AU_TriggerEventDispatcherTest.FailingTriggerEntry();
    AU_TriggerEventDispatcherTest.ITestTriggerEntry entry3 = new AU_TriggerEventDispatcherTest.SuccessTriggerEntry();
    
    dispatcher = new AU_TriggerEventDispatcher(new List<AU_TriggerEntry.ITriggerEntry> { entry1, entry2, entry3 });
    
    System.assertEquals(0, Database.countQuery('SELECT COUNT() FROM AU_DebugInfo__c'));
    
    Test.startTest();
    dispatcher.dispatch(createArgs());
    Test.stopTest();
    
    System.assertEquals(1, entry1.getMainInvocationCount());
    System.assertEquals(1, entry2.getMainInvocationCount());
    System.assertEquals(1, entry3.getMainInvocationCount());
    System.assertEquals(0, entry1.getProgressInvocationCount());
    System.assertEquals(0, entry2.getProgressInvocationCount());
    System.assertEquals(0, entry3.getProgressInvocationCount());
    
    List<AU_DebugInfo__c> debugLogs = [SELECT OwnerId, Level__c, Component__c, Data__c FROM AU_DebugInfo__c];
    System.assertEquals(1, debugLogs.size());
    
    AU_DebugInfo__c errorEntry = debugLogs[0];
    System.assertEquals('Error', errorEntry.Level__c);
    System.assertEquals('AU_TriggerEventDispatcher', errorEntry.Component__c);
    
    System.assert(errorEntry.Data__c.contains('AU_TriggerEventDispatcher.dispatch'));
    System.assert(errorEntry.Data__c.contains('AU_TriggerEventDispatcherTest.MyException'));
    System.assert(errorEntry.Data__c.contains('INVOKE_MAIN_ERROR'));
  }
  
  @isTest
  static void testDispatchWhileInProgress_Success() {
    AU_TriggerEventDispatcherTest.ITestTriggerEntry entry1 = new AU_TriggerEventDispatcherTest.SuccessTriggerEntry();
    AU_TriggerEventDispatcherTest.ITestTriggerEntry entry2 = new AU_TriggerEventDispatcherTest.DispatchAnotherEventTriggerEntry();

    AU_TriggerEventDispatcher firstDispatcher = new AU_TriggerEventDispatcher(new List<AU_TriggerEntry.ITriggerEntry> { entry1, entry2 });
    dispatcher = new AU_TriggerEventDispatcher(new List<AU_TriggerEntry.ITriggerEntry> { entry1, entry2 });
    
    Test.startTest();
    firstDispatcher.dispatch(createArgs());
    Test.stopTest();
    
    System.assertEquals(2, entry1.getMainInvocationCount());
    System.assertEquals(2, entry2.getMainInvocationCount());
    System.assertEquals(1, entry1.getProgressInvocationCount());
    System.assertEquals(1, entry2.getProgressInvocationCount());
  }
  
  @isTest
  static void testDispatchSerialized() {
    AU_TriggerEventDispatcherTest.SuccessTriggerEntry entry1 = new AU_TriggerEventDispatcherTest.SuccessTriggerEntry();
    AU_TriggerEventDispatcherTest.SuccessTriggerEntry entry2 = new AU_TriggerEventDispatcherTest.SuccessTriggerEntry();

    AU_TriggerEventDispatcher firstDispatcher = new AU_TriggerEventDispatcher(new List<AU_TriggerEntry.ITriggerEntry> { entry1 });
    dispatcher = new AU_TriggerEventDispatcher(new List<AU_TriggerEntry.ITriggerEntry> { entry2 });
    
    Test.startTest();
    firstDispatcher.dispatch(createArgs());
    dispatcher.dispatch(createArgs());
    Test.stopTest();
    
    System.assertEquals(1, entry1.getSerializedInvocationCount());
    System.assertEquals(1, entry2.getSerializedInvocationCount());
  }

  @isTest
  static void testDispatchSerialized_Recursion() {
    AU_TriggerEventDispatcherTest.SuccessTriggerEntry entry = new AU_TriggerEventDispatcherTest.SuccessTriggerEntry();

    dispatcher = new AU_TriggerEventDispatcher(new List<AU_TriggerEntry.ITriggerEntry> {
      entry
    });

    Test.startTest();
    dispatcher.dispatch(createArgs());
    dispatcher.dispatch(createArgs());
    Test.stopTest();

    System.assertEquals(2, entry.getSerializedInvocationCount());
  }

  @isTest
  static void testDispatchSerialized_DifferentEvents() {
    AU_TriggerEventDispatcherTest.SuccessTriggerEntry entry = new AU_TriggerEventDispatcherTest.SuccessTriggerEntry();

    dispatcher = new AU_TriggerEventDispatcher(new List<AU_TriggerEntry.ITriggerEntry> {
      entry
    });

    Test.startTest();
    dispatcher.dispatch(createArgs());
    dispatcher.dispatch(new AU_TriggerEntry.Args(
      Case.getSObjectType(),
      TriggerOperation.AFTER_INSERT,
      false,
      new List<Sobject>(),
      null,
      new Map<Id, Sobject>(),
      null
    ));
    Test.stopTest();

    System.assertEquals(1, entry.getSerializedInvocationCount());
  }
  
  @isTest
  static void testDispatchWhileInProgress_OneHandlerFails() {
    AU_TriggerEventDispatcherTest.ITestTriggerEntry entry1 = new AU_TriggerEventDispatcherTest.SuccessTriggerEntry();
    AU_TriggerEventDispatcherTest.ITestTriggerEntry entry2 = new AU_TriggerEventDispatcherTest.FailingTriggerEntry();
    AU_TriggerEventDispatcherTest.ITestTriggerEntry entry3 = new AU_TriggerEventDispatcherTest.DispatchAnotherEventTriggerEntry();

    dispatcher = new AU_TriggerEventDispatcher(new List<AU_TriggerEntry.ITriggerEntry> { entry1, entry2, entry3 });
    
    System.assertEquals(0, Database.countQuery('SELECT COUNT() FROM AU_DebugInfo__c'));
    
    Test.startTest();
    dispatcher.dispatch(createArgs());
    Test.stopTest();
    
    System.assertEquals(1, entry1.getMainInvocationCount());
    System.assertEquals(1, entry2.getMainInvocationCount());
    System.assertEquals(1, entry3.getMainInvocationCount());
    System.assertEquals(1, entry1.getProgressInvocationCount());
    System.assertEquals(1, entry2.getProgressInvocationCount());
    System.assertEquals(1, entry3.getProgressInvocationCount());
    
    List<AU_DebugInfo__c> debugLogs = [SELECT OwnerId, Level__c, Component__c, Data__c FROM AU_DebugInfo__c];
    System.assertEquals(2, debugLogs.size());
    
    AU_DebugInfo__c errorEntry = debugLogs[0];
    System.assertEquals('Error', errorEntry.Level__c);
    System.assertEquals('AU_TriggerEventDispatcher', errorEntry.Component__c);
    
    System.assert(errorEntry.Data__c.contains('AU_TriggerEventDispatcher.dispatch'));
    System.assert(errorEntry.Data__c.contains('AU_TriggerEventDispatcherTest.MyException'));
    System.assert(errorEntry.Data__c.contains('INVOKE_MAIN_ERROR'));
    
    errorEntry = debugLogs[1];
    System.assertEquals('Error', errorEntry.Level__c);
    System.assertEquals('AU_TriggerEventDispatcher', errorEntry.Component__c);
    System.assert(errorEntry.Data__c.contains('INVOKE_WHILE_IN_PROGRESS_ERROR'));
  }
  
  private static AU_TriggerEntry.Args createArgs() {
    return new AU_TriggerEntry.Args(
      Case.getSObjectType(),
      TriggerOperation.BEFORE_INSERT,
      false,
      new List<Sobject>(), 
      null,
      new Map<Id, Sobject>(),
      null
    );
  }
  
  private interface ITestTriggerEntry extends AU_TriggerEntry.ITriggerEntry {
    Integer getMainInvocationCount();
    Integer getProgressInvocationCount();
  }
  
  private class DispatchAnotherEventTriggerEntry implements ITestTriggerEntry {
    
    private Integer mainInvocationCount;
    private Integer progressInvocationCount;
    
    public DispatchAnotherEventTriggerEntry() {
      mainInvocationCount = 0;
      progressInvocationCount = 0;
    }
    
    public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount) {
      mainInvocationCount++;
      dispatcher.dispatch(args);
    }
    
    public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {
      progressInvocationCount++;
    }
    
    public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount)
    {}
    
    public Integer getMainInvocationCount() {
      return mainInvocationCount;
    }
    
    public Integer getProgressInvocationCount() {
      return progressInvocationCount;
    }
  }
  
  private class SuccessTriggerEntry implements ITestTriggerEntry {
    
    private Integer mainInvocationCount;
    private Integer progressInvocationCount;
    private Integer serializedInvocationCount;
    
    public SuccessTriggerEntry() {
      mainInvocationCount = 0;
      progressInvocationCount = 0;
    }
    
    public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount) {
      mainInvocationCount++;
      serializedInvocationCount = invocationCount;
    }
    public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {
      progressInvocationCount++;
    }
    
    public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount)
    {}
    
    public Integer getMainInvocationCount() {
      return mainInvocationCount;
    }
    
    public Integer getProgressInvocationCount() {
      return progressInvocationCount;
    }
    
    public Integer getSerializedInvocationCount() {
      return serializedInvocationCount;
    }
  }
  
  private class FailingTriggerEntry implements ITestTriggerEntry {
    
    private Integer mainInvocationCount;
    private Integer progressInvocationCount;
    
    public FailingTriggerEntry() {
      mainInvocationCount = 0;
      progressInvocationCount = 0;
    }
    
    public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount) {
      mainInvocationCount++;
      throw new MyException('INVOKE_MAIN_ERROR');
    }
    
    public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {
      progressInvocationCount++;
      throw new MyException('INVOKE_WHILE_IN_PROGRESS_ERROR');
    }
    
    public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount)
    {}
    
    public Integer getMainInvocationCount() {
      return mainInvocationCount;
    }
    
    public Integer getProgressInvocationCount() {
      return progressInvocationCount;
    }
  }
  
  public class MyException extends Exception {}
}
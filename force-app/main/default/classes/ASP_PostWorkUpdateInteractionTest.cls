/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 08-Nov-18. 
 */

@isTest
public class ASP_PostWorkUpdateInteractionTest {

  private static final String ADMINUSERNAME = 'AdminUser@aria.test';

  private static Account testAccount { get; set; }
  private static Contact testContact { get; set; }
  private static Case testCase { get; set; }
  private static Opportunity testOpportunity { get; set; }
  private static User adminUser {
    get {
      if (adminUser == null) {
        adminUser = [SELECT Id FROM User WHERE UserName = :ADMINUSERNAME];
      }
      return adminUser;
    }
    set;
  }

  @testSetup static void setup() {

    Id userRoleId = [SELECT Id FROM UserRole LIMIT 1].Id;

    adminUser = ATU_UserFactory.createDefaultBuilder('Aria', 'Admin')
        .withProfile('System Administrator')
        .withField('Username', ADMINUSERNAME)
        .withField('UserRoleId', userRoleId)
        .persist();
  }

  @isTest
  static void quickSaveTest() {

    System.runAs(adminUser) {
      createAccountContactCaseAndOpportunity();
      Id transcriptId = createLiveChatTranscript();
      Id interactionId = createInteraction(transcriptId);
      ASP_Interaction_Segment__c segment = createSegment(interactionId, transcriptId);
      String objectIds = testCase.Id +',' + testOpportunity.Id + ',' + testAccount.Id + ',' + testContact.Id;

      PageReference pageRef = Page.ASP_PostWorkUpdateInteraction;
      Test.setCurrentPageReference(pageRef);
      pageRef.getParameters().put('interaction_segment_Id', segment.Interaction_Segment_Id__c);
      pageRef.getParameters().put('objectIds', objectIds);

      ASP_PostWorkUpdateInteractionController controller = new ASP_PostWorkUpdateInteractionController();
      List<SelectOption> dispositions = controller.getDispositionCodes();
      String disposition = dispositions[0].getValue();

      Map<String, Object> propertiesMap = new Map<String, String>();
      propertiesMap.put('account', testAccount.Id);
      propertiesMap.put('contact', testContact.Id);
      propertiesMap.put('cases', testCase.Id);
      propertiesMap.put('opps', testOpportunity.Id);
      propertiesMap.put('interactionSegmentId', segment.Interaction_Segment_Id__c);
      propertiesMap.put('disposition', disposition);
      propertiesMap.put('notes', 'Test Notes');

      String properties = JSON.Serialize(propertiesMap);

      Test.startTest();
      ASP_PostWorkUpdateInteractionController.quickSave(properties);
      Test.stopTest();

      System.assertEquals(4, [SELECT Id FROM ASP_Related_Objects__c].size());
      System.assertNotEquals(null, [SELECT Disposition__c FROM ASP_Interaction_Segment__c].Disposition__c);
      System.assertEquals(disposition, [SELECT Disposition__c FROM ASP_Interaction_Segment__c].Disposition__c);
      System.assertNotEquals(null, [SELECT Notes__c FROM ASP_Interaction_Segment__c].Notes__c);
      System.assertEquals('Test Notes', [SELECT Notes__c FROM ASP_Interaction_Segment__c].Notes__c);
    }
  }

  // Helper methods

  static Id createLiveChatTranscript()  {
    LiveChatTranscript transcript = new LiveChatTranscript(
        RequestTime = DateTime.newInstance(2018, 11, 18, 3, 3, 3),
        StartTime = DateTime.newInstance(2018, 11, 18, 3, 4, 3),
        EndTime = DateTime.newInstance(2018, 11, 18, 3, 5, 3),
        Body = 'Test Body',
        LiveChatVisitorId = createLiveChatVisitor());
    insert transcript;
    return transcript.Id;
  }

  static Id createLiveChatVisitor() {
    LiveChatVisitor visitor = new LiveChatVisitor();
    insert visitor;
    return visitor.Id;
  }

  static Id createInteraction(Id workItemId)  {
    ASP_Interaction__c interaction = new ASP_Interaction__c(Interaction_Id__c = workItemId);
    insert interaction;
    return interaction.Id;
  }

  static ASP_Interaction_Segment__c createSegment(Id interactionId, Id transcriptId) {
    ASP_Interaction_Segment__c newSegment = new ASP_Interaction_Segment__c(
        Interaction_Segment_Id__c = transcriptId + '' + UserInfo.getUserId(),
        RecordTypeId = AU_RecordTypes.getId('ASP_Interaction_Segment__c', 'Chat'),
        ASP_Interaction__c = interactionId);

    insert newSegment;
    return newSegment;
  }

  private static void createAccountContactCaseAndOpportunity() {
    testAccount = ATU_AccountFactory.createDefaultBuilder('testAccount').persist();
    testContact = ATU_ContactFactory.createDefaultBuilder('Doe', testAccount.Id)
        .withFirstName('John')
        .withField('Email', 'john.doe.test@test.com')
        .persist();

    testOpportunity = ATU_OpportunityFactory.createDefaultBuilder(testAccount.Id).persist();

    testCase = ATU_CaseFactory.createDefaultBuilder('testCase')
        .withField('ContactId', testContact.Id)
        .persist();
  }

}
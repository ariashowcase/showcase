/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Invocable action for creating skill requirements on omni routing items.
 * Created by jliu on 6/13/2018.
 */

global without sharing class ASP_SkillBasedRoutingAction {

  @InvocableMethod(label='Route With Skills' description='Routes a work item with skill-based routing.')
  global static void routeWithSkillsAsync(List<RoutingItem> routingItems) {
    // workaround for work items getting removed from backlog when owner is changed from User to Queue
    System.enqueueJob(new AsyncRouter(routingItems));
  }

  public class AsyncRouter implements Queueable {
    private List<RoutingItem> routingItems;

    public AsyncRouter(List<RoutingItem> routingItems) {
      this.routingItems = routingItems;
    }

    public void execute(QueueableContext context) {
      routeWithSkills(routingItems);
    }
  }

  private static void routeWithSkills(List<RoutingItem> routingItems) {
    try {
      AU_Debugger.enterFunction('ASP_SkillBasedRoutingAction.routeWithSkills');

      Set<String> serviceChannelNames = new Set<String>();
      Set<Id> existingWorkItemIds = new Set<Id>();
      for (RoutingItem item : routingItems) {
        serviceChannelNames.add(item.serviceChannelName);
        existingWorkItemIds.add(item.workItemId);
      }

      delete [SELECT Id FROM PendingServiceRouting WHERE WorkItemId IN :existingWorkItemIds];

      Map<String, Id> serviceChannelIdsByName = new Map<String, Id>();
      for (ServiceChannel channel : [
        SELECT DeveloperName
        FROM ServiceChannel
        WHERE DeveloperName IN :serviceChannelNames
      ]) {
        serviceChannelIdsByName.put(channel.DeveloperName, channel.Id);
      }

      List<PendingServiceRouting> pendingRoutingItems = new List<PendingServiceRouting>();
      for (RoutingItem item : routingItems) {
        PendingServiceRouting psr = new PendingServiceRouting(
          IsReadyForRouting = false,
          RoutingType = 'SkillsBased',
          RoutingModel = item.routingModel,
          RoutingPriority = item.routingPriority,
          ServiceChannelId = serviceChannelIdsByName.get(item.serviceChannelName),
          WorkItemId = item.workItemId
        );

        if (item.capacityPercentage == null)
          psr.CapacityWeight = item.capacityWeight;
        else
          psr.CapacityPercentage = item.capacityPercentage;

        pendingRoutingItems.add(psr);
      }

      insert pendingRoutingItems;

      List<SkillRequirement> skillRequirements = ASP_GlobalResolver.resolveSkills(pendingRoutingItems);
      insert skillRequirements;

      for (PendingServiceRouting item : pendingRoutingItems) {
        item.IsReadyForRouting = true;
      }
      update pendingRoutingItems;
    }
    catch (Exception e) {
      AU_Debugger.reportException('ASP_SkillBasedRoutingAction.routeWithSkills', e);
    }
    finally {
      AU_Debugger.leaveFunction();
    }
  }

  global class RoutingItem {
    @InvocableVariable(label='Work Item Id' description='The work item Id' required=true)
    global Id workItemId;

    @InvocableVariable(label='Capacity Weight' description='If using Capacity Percentage, enable it from optional parameters.' required=true)
    global Integer capacityWeight;

    @InvocableVariable(label='Routing Model' description='Type of routing model.  Valid values are not documented by Salesforce.' required=true)
    global String routingModel;

    @InvocableVariable(label='Routing Priority' description='The order in which work items are routed to agents.' required=true)
    global Integer routingPriority;

    @InvocableVariable(label='Service Channel Name' description='The developer name of the service channel.' required=true)
    global String serviceChannelName;

    @InvocableVariable(label='Capacity Percentage' description='If filled, will be used instead of Capacity Weight' required=false)
    global Integer capacityPercentage;
  }
}
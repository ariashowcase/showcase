/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
public class AU_TriggerEntry {

	public class Args {
		public final SObjectType objectType;
		public final TriggerOperation operation;
		public final Boolean isExecuting;
		public final List<Sobject> newObjects;
		public final List<Sobject> oldObjects;
		public final Map<Id, Sobject> newObjectMap;
		public final Map<Id, Sobject> oldObjectMap;
		
		public Args(
			SObjectType objectType,
			TriggerOperation operation,
			Boolean isExecuting,
			List<Sobject> newObjects,
			List<Sobject> oldObjects,
			Map<Id, Sobject> newObjectMap,
			Map<Id, Sobject> oldObjectMap 
		) {
			this.objectType = objectType;
      this.operation = operation;
			this.isExecuting = isExecuting;
			this.newObjects = newObjects;
			this.oldObjects = oldObjects;
			this.newObjectMap = newObjectMap;
			this.oldObjectMap = oldObjectMap;
		}

		public Args reduce(Set<Id> idsToKeep) {
			return new Args(
				this.objectType,
				this.operation,
				this.isExecuting,
				AU_CollectionsUtil.retainAll(this.newObjects, idsToKeep),
				AU_CollectionsUtil.retainAll(this.oldObjects, idsToKeep),
				AU_MapUtil.retainAll(this.newObjectMap, idsToKeep),
				AU_MapUtil.retainAll(this.oldObjectMap, idsToKeep)
			);
		}
		
		public override String toString() {
			String numObjectsStr;

			if (operation == TriggerOperation.BEFORE_DELETE || operation == TriggerOperation.AFTER_DELETE)
			  numObjectsStr = String.valueOf(oldObjects.size());
			else
			  numObjectsStr = String.valueOf(newObjects.size());

			return String.format(
				'AU_TriggerEntry.Args[{0}, operationType={1}, isExecuting={2}, numObjects={3}]',
				new String[]{ String.valueOf(objectType), String.valueOf(operation),  String.valueOf(isExecuting), numObjectsStr});
    }

		public Integer hashCode() {
			Integer prime = 37;
			Integer result = 1;

			result = prime * result + ((operation == null) ? 0 : System.hashCode(operation));
			result = prime * result + ((objectType == null) ? 0 : System.hashCode(objectType));
			result = prime * result + ((isExecuting == null) ? 0 : System.hashCode(isExecuting));

			return result;
		}

		public Boolean equals(Object obj) {
			if (!(obj instanceof Args)) {
				return false;
			}

      // Do not consider the new and old lists/maps as values could be changed, which would mean that the event
      // would not be considered the same. The equality here is meant to highlight whether we are dealing with the same
      // event condition.
			Args other = (args) obj;
			return objectType == other.objectType
					&& isExecuting == other.isExecuting
					&& operation == other.operation;
		}
	}

	public interface ITriggerEntry {
		void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount);
		void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount);
		void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount);
	}
}
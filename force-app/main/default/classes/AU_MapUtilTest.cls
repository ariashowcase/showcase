/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@IsTest
public class AU_MapUtilTest {

  @IsTest
  public static void testRetainAll() {
    List<Account> accounts = new List<Account> {
      ATU_Account.build('Account A').withField('AccountNumber', 'aaa').create(),
      ATU_Account.build('Account B').withField('AccountNumber', 'bbb').create()
    };
    insert accounts;

    System.assertEquals(2, [SELECT Id FROM Account].size());

    Id accountAId = accounts.get(0).Id;
    Set<Id> idsToKep = new Set<Id>{ accountAId };
    Map<Id, SObject> filteredAccounts = AU_MapUtil.retainAll(new Map<Id, Account> ([SELECT Id FROM Account]), idsToKep);

    System.assertEquals(1, filteredAccounts.size());
    System.assert(filteredAccounts.get(accountAId) != null);
  }

}
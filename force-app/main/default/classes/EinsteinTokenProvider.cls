/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

public class EinsteinTokenProvider implements TokenProvider {

  private static final String CLASS_NAME = 'EinsteinTokenProvider';

  public static TokenProvider forProfile(String profileName) {
    return new EinsteinTokenProvider(profileName);
  }

  public static TokenProvider forDefaultProfile() {
    return new EinsteinTokenProvider(DEFAULT_SECURITY_PROFILE);
  }

  private static final Integer EXPIRY = 3600;

  private static final String BASE_URL = 'https://api.einstein.ai/v2';
  private static final String OAUTH2 = BASE_URL + '/oauth2/token';
  private static final String SESSION_TOKEN_KEY = 'einsteinSessionToken';

  private static final String DEFAULT_SECURITY_PROFILE = 'DefaultProfileLj2mLH';

  private final String securityProfileName;

  private EinsteinTokenProvider(String securityProfileName) {
    this.securityProfileName = securityProfileName;
  }

  public String getToken() {
    AU_Debugger.enterFunction(CLASS_NAME + '/getToken');
    try {
      // if Token in Org cache, use token
      AU_Debugger.debug('Check Session Cache for token');
      String cachedToken = getCachedToken();
      if (String.isNotEmpty(cachedToken)) {
        return cachedToken;
      }

      // otherwise, request new token and save in org cache
      AU_Debugger.debug('No token in cache, requesting new token');
      String newToken = requestNewToken();

      setTokenCache(newToken);

      return newToken;
    } catch (Exception ex) {
      throw new EinsteinTokenProviderException(ex);
    } finally {
      AU_Debugger.leaveFunction();
    }
  }

  private void setTokenCache(String newToken) {
    try {
      Cache.Org.put(getTokenCacheKey(), newToken, EXPIRY - 60);
    } catch (Exception ex) {
      System.debug('Failed to access platform cache: ' + ex.getMessage());
    }
  }

  private String getCachedToken() {
    try {
      return (String) Cache.Org.get(getTokenCacheKey());
    } catch (Exception ex) {
      AU_Debugger.debug('Failed to access platform cache: ' + ex.getMessage());
      return null;
    }
  }

  private String requestNewToken() {
    Einstein_Security_Profile__mdt securityProfile = getSecurityProfile();

    String keyContents = securityProfile.RSA_Key__c;
    keyContents = keyContents.replace('-----BEGIN RSA PRIVATE KEY-----', '');
    keyContents = keyContents.replace('-----END RSA PRIVATE KEY-----', '');
    keyContents = keyContents.deleteWhitespace();

    JWT jwt = new JWT('RS256');
    jwt.pkcs8 = keyContents;
    jwt.iss = 'developer.force.com';
    jwt.sub = securityProfile.Account_Email__c;
    jwt.aud = OAUTH2;
    jwt.exp = String.valueOf(EXPIRY);

    return JWTBearerFlow.getAccessToken(OAUTH2, jwt);
  }

  private Einstein_Security_Profile__mdt getSecurityProfile() {
    if (securityProfileName == DEFAULT_SECURITY_PROFILE) {
      return getDefaultSecurityProfile();
    }

    return [SELECT Account_Email__c, RSA_Key__c FROM Einstein_Security_Profile__mdt WHERE DeveloperName = :securityProfileName];
  }

  private Einstein_Security_Profile__mdt getDefaultSecurityProfile() {
    try {
      return [SELECT Account_Email__c, RSA_Key__c FROM Einstein_Security_Profile__mdt WHERE Is_Default_Profile__c = TRUE];
    } catch (QueryException ex) {
      throw new DefaultProfileCouldNotBeLoadedException('The default Einstein Security Profile could not be retrieved. Details: ' + ex.getMessage(), ex);
    }
  }

  private String getTokenCacheKey() {
    return SESSION_TOKEN_KEY + securityProfileName;
  }

  public class DefaultProfileCouldNotBeLoadedException extends Exception { }
  public class EinsteinTokenProviderException extends Exception { }
}
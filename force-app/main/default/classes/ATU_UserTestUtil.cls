/**
 * Created by jfischer on 7/29/2016.
 */
@IsTest
public class ATU_UserTestUtil {

  private static final Profile STANDARD_USER;

  static {
    STANDARD_USER = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
  }

  public static User createStandardUser(String firstName, String lastName) {
    return new User(
      FirstName = firstName,
      LastName = lastName,
      Alias = firstName,
      Username = firstName.toLowerCase() + lastName.toLowerCase() + '@test.com',
      Email = firstName.toLowerCase() + lastName.toLowerCase() + '@testcompany.com',
      ProfileId = STANDARD_USER.Id,
      EmailEncodingKey='UTF-8',
      LanguageLocaleKey='en_US',
      LocaleSidKey='en_US',
      TimeZoneSidKey='America/Los_Angeles'
    );
  }
}
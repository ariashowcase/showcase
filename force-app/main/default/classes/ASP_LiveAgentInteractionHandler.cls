/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 24-Oct-18. 
 */

public class ASP_LiveAgentInteractionHandler implements AU_TriggerEntry.ITriggerEntry {

  private static final String CLASS_NAME = 'ASP_LiveAgentInteractionHandler';

  public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {
  }
  public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {
  }

  public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount) {
    AU_Debugger.enterFunction(CLASS_NAME + '.invokeMain:');
    AU_Debugger.debug('TriggerOperation: ' + args.operation);

    if (args.operation != TriggerOperation.AFTER_UPDATE) {
      AU_Debugger.debug('Operation not supported.');
      AU_Debugger.leaveFunction();
      return;
    }

    AU_TriggerEntry.Args chatArgs = AU_TriggerUtil.filter(args).byNewValueIsType('WorkItemId', LiveChatTranscript.SObjectType).run();

    List<AgentWork> acceptedWorks = (List<AgentWork>) AU_TriggerUtil.filter(chatArgs).byValueChanged('AcceptDateTime').run().newObjects;
    if (!acceptedWorks.isEmpty()) {
      createInteractions(acceptedWorks);
    }

    List<AgentWork> closedWorks = (List<AgentWork>) AU_TriggerUtil.filter(chatArgs).byValueChanged('CloseDateTime').run().newObjects;
    if (!closedWorks.isEmpty()) {
      updateSegments(chatArgs.newObjectMap.keySet());
    }

    AU_Debugger.leaveFunction();
  }

  private void createInteractions(List<AgentWork> agentWorks) {
    List<ASP_Interaction__c> interactions = new List<ASP_Interaction__c>();
    List<ASP_Interaction_Segment__c> segments = new List<ASP_Interaction_Segment__c>();

    for (AgentWork aw : agentWorks) {
      ASP_Interaction__c interaction = new ASP_Interaction__c(Interaction_Id__c = aw.WorkItemId);
      ASP_Interaction_Segment__c segment = new ASP_Interaction_Segment__c(
          Interaction_Segment_Id__c = aw.WorkItemId + '' + aw.UserId,
          RecordTypeId = AU_RecordTypes.getId('ASP_Interaction_Segment__c', 'Chat'),
          Live_Chat_Transcript__c = aw.WorkItemId,
          ASP_Interaction__r = interaction.clone()
      );
      interactions.add(interaction);
      segments.add(segment);
    }
    if (!interactions.isEmpty())  {
      Database.upsert(interactions, ASP_Interaction__c.fields.Interaction_Id__c, true);
      insert segments;
    }
  }

  @future
  public static void updateSegments(Set<Id> agentWorkIds) {

    String queryString = 'SELECT OriginalQueueId, AcceptDateTime, AssignedDateTime, CloseDateTime, ActiveTime, UserId, WorkItemId FROM AgentWork WHERE Id IN :agentWorkIds';
    List<AgentWork> workItems = Database.query(queryString);
    Map<Id, Group> queues = new Map<Id, Group>([SELECT Id, Name FROM Group WHERE Type = 'Queue']);

    if (workItems.size() > 0) {
      String interactionSegmentId = workItems[0].WorkItemId + '' + workItems[0].UserId;
      List<ASP_Interaction_Segment__c> segments = [
          SELECT Id, Interaction_Segment_Id__c, Active_Time__c, Queue__c, Disposition__c, Sentiment__c, Sentiment_Probability__c, Transcription__c, SubType__c, Start_Time__c, End_Time__c, ASP_Interaction__r.Interaction_Id__c
          FROM ASP_Interaction_Segment__c
          WHERE Interaction_Segment_Id__c = :interactionSegmentId];

      if (!segments.isEmpty()) {
        for (AgentWork workItem : workItems) {
          Group omniQueue = queues.get(String.valueOf(workItem.get('OriginalQueueId')));

          for (ASP_Interaction_Segment__c segment : segments) {
            segment.Start_Time__c = workItem.AcceptDateTime;
            segment.End_Time__c = workItem.CloseDateTime;
            segment.Queue__c = omniQueue.Name;
            segment.Active_Time__c = workItem.ActiveTime;
          }
        }
        update segments;
      }
    }
  }
}
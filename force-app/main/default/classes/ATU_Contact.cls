/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@IsTest
public class ATU_Contact {

  public static ATU_Contact.Builder build(String lastName, String accountId) {
    return new ATU_Contact.Builder(lastName, accountId);
  }
  // TODO what is this for?
  public static ATU_Contact.Builder build(fflib_SObjectUnitOfWork uow, String lastName, Account a) {
    ATU_Contact.Builder builder = new ATU_Contact.Builder(lastName, null);
    uow.registerNew(builder.theRecord, Contact.AccountId, a);
    return builder;
  }

  public class Builder {

    private final Contact theRecord;

    private Builder(String lastName, String accountId) {
      theRecord = new Contact(
        LastName = lastName,
        AccountId = accountId
      );
    }

    /*
    public ATU_Contact.Builder withRecordType(String recordType) {
      RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = :recordType];
      theRecord.RecordTypeId = rt.Id;
      return this;
    }
    */

    public ATU_Contact.Builder withFirstName(String firstName) {
      theRecord.FirstName = firstName;
      return this;
    }

    public ATU_Contact.Builder withField(String fieldName, Object value) {
      theRecord.put(fieldName, value);
      return this;
    }

    public Contact create() {
      return theRecord;
    }

    public Contact persist() {
      insert theRecord;
      return theRecord;
    }

    public Contact registerNew(AU_UnitOfWork uow) {
      uow.registerNew(theRecord);
      return theRecord;
    }

    public Contact monitorNew(AU_UnitOfWork uow) {
      uow.monitorNew(theRecord);
      return theRecord;
    }
  }
}
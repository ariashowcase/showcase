/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@isTest
private class AU_PicklistUtilTest {
  @isTest 
  static void testProvidedValidPicklistValue() {
    String validAccountType = Account.Type.getDescribe().getPicklistValues()[0].getValue();
    String result = AU_PicklistUtil.validPicklistValue(Account.Type, validAccountType, '3.14159265359...');
    system.assertEquals(validAccountType, result);

    List<AU_DebugInfo__c> debugInfos = [select id from AU_DebugInfo__c where Level__c = 'Warning'];
    system.assertEquals(0, debugInfos.size());
  }

  @isTest 
  static void testBlankValidPicklistValue() {
    String result = AU_PicklistUtil.validPicklistValue(Account.Type, '\t\n  ', '3.14159265359...');
    system.assertEquals('', result);

    List<AU_DebugInfo__c> debugInfos = [select id from AU_DebugInfo__c where Level__c = 'Warning'];
    system.assertEquals(0, debugInfos.size());
  }
	
  @isTest 
  static void testDefaultValidPicklistValue() {
    Test.startTest();
    String result = AU_PicklistUtil.validPicklistValue(Account.Type, '0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, ...', '3.14159265359...');
    Test.stopTest();
    
    system.assertEquals('3.14159265359...', result);

    List<AU_DebugInfo__c> debugInfos = [select id from AU_DebugInfo__c where Level__c = 'Warning'];
    system.assertEquals(1, debugInfos.size());
  }
}
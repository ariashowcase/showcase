/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 25-Jul-18. 
 */

@isTest
public class ASPC_UserInfoControllerTest {

  private static final String PORTALUSERNAME = 'PortalUser@aria.test';
  private static final String ADMINUSERNAME = 'AdminUser@aria.test';

  private static Account testAccount { get; set; }
  private static Contact testContact { get; set; }

  private static User portalUser {
    get {
      if (portalUser == null) {
        portalUser = [SELECT Id FROM User WHERE UserName = :PORTALUSERNAME];
      }
      return portalUser;
    }
    set;
  }

  private static User adminUser {
    get {
      if (adminUser == null) {
        adminUser = [SELECT Id FROM User WHERE UserName = :ADMINUSERNAME];
      }
      return adminUser;
    }
    set;
  }

  @testSetup static void setup() {

    Id userRoleId = [SELECT Id FROM UserRole LIMIT 1].Id;

    ATU_UserFactory.createDefaultBuilder('Aria', 'Admin')
        .withAdminProfile()
        .withField('Username', ADMINUSERNAME)
        .withField('UserRoleId', userRoleId)
        .persist();
  }

  @isTest static void getCurrentUserContactIdAndAccountIdTest_Positive() {
    System.runAs(adminUser) {
      createAccountContactAndOpportunity();
      createPortalUser();
    }

    System.runAs(portalUser) {
      Test.startTest();
      System.assertEquals(portalUser.Id, ASPC_UserInfoController.getCurrentUserContactIdAndAccountId().Id);
      System.assertEquals(testAccount.Id, ASPC_UserInfoController.getCurrentUserContactIdAndAccountId().AccountId);
      System.assertEquals(testContact.Id, ASPC_UserInfoController.getCurrentUserContactIdAndAccountId().ContactId);
      Test.stopTest();
    }
  }

  @isTest static void getCurrentUserContactIdAndAccountIdTest_Negative() {
    createAccountContactAndOpportunity();

    Test.startTest();
    System.assertEquals(null, ASPC_UserInfoController.getCurrentUserContactIdAndAccountId().AccountId);
    System.assertEquals(null, ASPC_UserInfoController.getCurrentUserContactIdAndAccountId().ContactId);
    Test.stopTest();
  }

  // Helper methods

  private static void createAccountContactAndOpportunity() {
    testAccount = ATU_AccountFactory.createDefaultBuilder('testAccount').persist();
    testContact = ATU_ContactFactory.createDefaultBuilder('Doe', testAccount.Id)
        .withFirstName('John')
        .withField('Email', 'john.doe.test@test.com')
        .persist();

    ATU_OpportunityFactory.createDefaultBuilder(testAccount.Id).persist();
  }

  private static void createPortalUser()  {
    String profileName = [SELECT Name FROM Profile WHERE UserType like '%portal%' LIMIT 1].Name;

    ATU_UserFactory.createDefaultBuilder('Portal', 'User')
        .withProfile(profileName)
        .withField('ContactId', testContact.Id)
        .withField('Username', PORTALUSERNAME)
        .persist();
  }
}
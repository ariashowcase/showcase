global with sharing class SC_ContactService {
  public static List<Contact> getContactsByPhone(String phone) {
    List<List<Contact>> searchResult = [
      FIND :phone
      IN PHONE FIELDS
      RETURNING Contact(FirstName, LastName, AccountId)
    ];

    return searchResult[0];
  }

  public static List<Contact> getContactsByAccountId(Id accountId) {
    return [
      SELECT  FirstName, LastName
      FROM    Contact 
      WHERE   AccountId = :accountId
    ];
  }
}

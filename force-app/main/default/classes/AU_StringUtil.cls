/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
public class AU_StringUtil {

  public static String truncate(String val, Integer truncatedLength) {
    if (String.isBlank(val)) {
      return val;
    }

    Integer actualLength = val.length();
    return val.substring(0, Math.min(actualLength, truncatedLength));
  }
}
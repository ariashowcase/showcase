/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 10/23/2018.
 */

public without sharing class ASP_CaseLatestQueueHandler implements AU_TriggerEntry.ITriggerEntry {
  public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount) {
    AU_Debugger.enterFunction('ASP_CaseLatestQueueHandler.invokeMain');

    if (args.operation != TriggerOperation.BEFORE_INSERT && args.operation != TriggerOperation.BEFORE_UPDATE) {
      AU_Debugger.debug('Operation not supported.');
      AU_Debugger.leaveFunction();
      return;
    }

    List<Case> casesPlacedInQueues = getCasesPlacedInQueues(args);
    if (!casesPlacedInQueues.isEmpty()) {
      populateQueueInfo(casesPlacedInQueues);
    }

    AU_Debugger.leaveFunction();
  }

  public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {}
  public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {}

  private List<Case> getCasesPlacedInQueues(AU_TriggerEntry.Args args) {
    AU_Debugger.enterFunction('ASP_CaseLatestQueueHandler.getCasesPlacedInQueues');
    List<Case> result = new List<Case>();

    switch on args.operation {
      when BEFORE_INSERT {
        for (Case c : (List<Case>) args.newObjects) {
          if (c.OwnerId.getSObjectType() == Group.SObjectType) {
            result.add(c);
          }
        }
      }
      when BEFORE_UPDATE {
        for (Case newCase : (List<Case>) args.newObjects) {
          Case oldCase = (Case) args.oldObjectMap.get(newCase.Id);
          if (oldCase.OwnerId != newCase.OwnerId && newCase.OwnerId.getSObjectType() == Group.SObjectType) {
            result.add(newCase);
          }
        }
      }
    }

    AU_Debugger.leaveFunction();
    return result;
  }

  private void populateQueueInfo(List<Case> cases) {
    AU_Debugger.enterFunction('ASP_CaseLatestQueueHandler.populateQueueInfo');
    AU_Debugger.debug('Populating ' + cases.size() + ' Cases.');

    Set<Id> queueIds = new Set<Id>();
    for (Case c : cases) {
      queueIds.add(c.OwnerId);
    }

    Map<Id, Group> queuesById = new Map<Id, Group>([
      SELECT  DeveloperName
      FROM    Group
      WHERE   Id in :queueIds
    ]);

    Datetime NOW = System.now();
    for (Case c : cases) {
      if (queuesById.containsKey(c.OwnerId)) {
        c.Latest_Queue__c = queuesById.get(c.OwnerId).DeveloperName;
        c.Latest_Queued_Date__c = NOW;
      }
    }

    AU_Debugger.leaveFunction();
  }
}
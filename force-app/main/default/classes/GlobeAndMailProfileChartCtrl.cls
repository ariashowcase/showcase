public class GlobeAndMailProfileChartCtrl {
    
    private final Id contactId;
    
    public GlobeAndMailProfileChartCtrl(ApexPages.StandardController stdController) {
        contactId = stdController.getId();
    }
    
    public List<ProfileData> getReaderProfile() {      
        List<ProfileData> result = new List<ProfileData>();
        for (Reader_Profile__c rp : [SELECT Name, Section_Investor__c, Section_News__c, Section_Sports__c FROM Reader_Profile__c WHERE Contact__c = :contactId ORDER BY CreatedDate DESC LIMIT 2]) {
            result.add(new ProfileData(rp.Name, rp.Section_News__c, rp.Section_Investor__c, rp.Section_Sports__c));
        }
        return result;
    }
    
    
    public class ProfileData {
        
        public String Name { get; set; }
        public Decimal Dental { get; set; }
        public Decimal Extended { get; set; }
        public Decimal Vision { get; set; }
        
        public ProfileData(String name, Decimal dental, Decimal extendedHealth, Decimal vision) {
            this.Name = name;
            this.Dental = dental;
            this.Extended = extendedHealth;
            this.Vision = vision;
        }
    }
}
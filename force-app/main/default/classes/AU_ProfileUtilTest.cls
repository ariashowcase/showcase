/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@isTest
private class AU_ProfileUtilTest {

  private static User standardUserJohn {
    get {
      if (standardUserJohn == null) {
        standardUserJohn = [SELECT Id, FirstName, LastName, Username FROM User WHERE LastName = 'StandardUser'];
      }
      return standardUserJohn;
    }
    set;
  }

  private static User adminUserJohn {
    get {
      if (adminUserJohn == null) {
        adminUserJohn = [SELECT Id, FirstName, LastName, Username FROM User WHERE LastName = 'SystemAdministrator'];
      }
      return adminUserJohn;
    }
    set;
  }

  @testSetup static void setup() {
    insert ATU_User.build('John', 'StandardUser').withProfile('Standard User').create();
    insert ATU_User.build('John', 'SystemAdministrator').withProfile('System Administrator').create();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest static void testGetCurrentUserProfileName() {

    System.runAs(standardUserJohn) {
      System.assertEquals('Standard User', AU_ProfileUtil.getCurrentUserProfileName());
    }

    System.runAs(adminUserJohn) {
      System.assertEquals('System Administrator', AU_ProfileUtil.getCurrentUserProfileName());
    }
  }

  @isTest static void testGetUserProfileName() {
    System.assertEquals('Standard User', AU_ProfileUtil.getUserProfileName(standardUserJohn.Id));
    System.assertEquals('System Administrator', AU_ProfileUtil.getUserProfileName(adminUserJohn.Id));
  }
}
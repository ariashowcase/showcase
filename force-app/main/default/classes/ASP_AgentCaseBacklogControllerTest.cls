/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@isTest
private class ASP_AgentCaseBacklogControllerTest {

  private static User AgentFlemming {
    get {
      if (AgentFlemming == null) {
        AgentFlemming = [SELECT Id, FirstName, LastName, Username FROM User WHERE LastName = 'flemming'];
      }
      return AgentFlemming;
    }
    set;
  }

  private static User AgentMiller {
    get {
      if (AgentMiller == null) {
        AgentMiller = [SELECT Id, FirstName, LastName, Username FROM User WHERE LastName = 'miller'];
      }
      return AgentMiller;
    }
    set;
  }

  @testSetup static void setup() {
    insert ATU_User.build('agent', 'flemming').create();
    insert ATU_User.build('agent', 'miller').create();
  }

  @isTest static void testCreateAgentWorkRecord_AgentInInvalidState() {
    Case c = ATU_Case.build('Case 1').withOwnerId(AgentFlemming.Id).create();

    System.assertEquals(0, [SELECT count() FROM AgentWork]);

    System.runAs(AgentFlemming) {

      Test.startTest();
      ASP_AgentCaseBacklogController.createAgentWorkRecord(c.Id);

      System.assertEquals(0, [SELECT count() FROM AgentWork]);
      Test.stopTest();
    }
  }
}
/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@IsTest
public class ATU_User {

  private static final Profile STANDARD_USER;
  private static final Profile ADMIN_USER;

  static {
    STANDARD_USER = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
    ADMIN_USER = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
  }

  public static ATU_User.Builder build(String firstName, String lastName) {
    return new ATU_User.Builder(firstName, lastName);
  }

  public class Builder {
    private User theRecord;

    public Builder(String firstName, String lastName) {
      theRecord = new User(
        FirstName = firstName,
        LastName = lastName,
        Alias = firstName,
        Username = firstName.toLowerCase() + lastName.toLowerCase() + '@aria-test.com',
        Email = firstName.toLowerCase() + lastName.toLowerCase() + '@testcompany.com',
        ProfileId = STANDARD_USER.Id,
        EmailEncodingKey='UTF-8',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Los_Angeles'
      );
    }

    public ATU_User.Builder withAdminProfile() {
      theRecord.ProfileId = ADMIN_USER.Id;
      return this;
    }

    public ATU_User.Builder withProfile(String profileName) {
      Profile p = [SELECT Id FROM Profile WHERE Name = :profileName];
      theRecord.ProfileId = p.Id;
      return this;
    }

    public ATU_User.Builder withField(String fieldName, Object value) {
      theRecord.put(fieldName, value);
      return this;
    }

    public User create() {
      return theRecord;
    }

    public User persist () {
      insert theRecord;
      return theRecord;
    }
  }

}
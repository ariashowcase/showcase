/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Wednesday July 24th 2019
 * Author: varonov
 * File type: '.cls'
 */


public with sharing class SC_TripsController {
  @AuraEnabled(cacheable=true)
  public static List<Trip__c> getTrips(Id accountId) {
    return [
      SELECT Name, Confirmation_Number__c, Origin__c, Destination__c, Date__c, 
      (
        SELECT Name, Seat_Number__c, Seat_Type__c, Flight__r.Id, Flight__r.Name, Flight__r.Flight_Status__c, Flight__r.Flight_Date_Time__c, Flight__r.Arrival_Date_Time__c, Flight__r.Flight_Product__r.Origin__c, Flight__r.Flight_Product__r.Destination__c 
        FROM Booked_Seats__r 
        ORDER BY Flight__r.Flight_Date_Time__c) 
      FROM Trip__c 
      WHERE Account__c = :accountId 
      ORDER BY Date__c desc LIMIT 1 
    ];
  }
}
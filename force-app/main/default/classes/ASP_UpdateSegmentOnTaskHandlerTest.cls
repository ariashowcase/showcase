/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 06-Jun-19. 
 */

@isTest
private class ASP_UpdateSegmentOnTaskHandlerTest {

  private static Account testAccount {
    get {
      if(testAccount == null) {
        testAccount = [SELECT Id FROM Account LIMIT 1];
      }
      return testAccount;
    }
    set;
  }
  private static Contact testContact {
    get {
      if(testContact == null) {
        testContact = [SELECT Id FROM Contact LIMIT 1];
      }
      return testContact;
    }
    set;
  }
  private static Case testCase {
    get {
      if(testCase == null) {
        testCase = [SELECT Id FROM Case LIMIT 1];
      }
      return testCase;
    }
    set;
  }
  private static Opportunity testOpportunity {
    get {
      if(testOpportunity == null) {
        testOpportunity = [SELECT Id FROM Opportunity LIMIT 1];
      }
      return testOpportunity;
    }
    set;
  }
  private static Lead testLead {
    get {
      if(testLead == null) {
        testLead = [SELECT Id FROM Lead LIMIT 1];
      }
      return testLead;
    }
    set;
  }

  @testSetup
  private static void setup() {
    testAccount = ATU_AccountFactory.createDefaultBuilder('testAccount').persist();
    testContact = ATU_ContactFactory.createDefaultBuilder('Doe', testAccount.Id)
        .withFirstName('John')
        .withField('Email', 'john.doe.test@test.com')
        .persist();

    testCase = ATU_CaseFactory.createDefaultBuilder('testCase')
        .withField('ContactId', testContact.Id)
        .withField('AccountId', testAccount.Id)
        .persist();

    testOpportunity = ATU_OpportunityFactory.createDefaultBuilder(testAccount.Id).persist();
    testLead = new Lead(FirstName = 'John', LastName = 'Doe', Company = 'Aria');
    insert testLead;
  }

  @isTest
  private static void createInteractionAndSegmentAndCaseAsRelatedObject()  {
    Test.startTest();
    Task callTask = createTask('Case');
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(1, [SELECT Id FROM ASP_Interaction__c].size());
    System.assertEquals(1, [SELECT Id FROM ASP_Interaction_Segment__c].size());
    System.assertEquals(3, [SELECT Id FROM ASP_Related_Objects__c].size());
  }

  @isTest
  private static void createInteractionAndSegmentAndOpportunityAsRelatedObject()  {
    Test.startTest();
    Task callTask = createTask('Opportunity');
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(1, [SELECT Id FROM ASP_Interaction__c].size());
    System.assertEquals(1, [SELECT Id FROM ASP_Interaction_Segment__c].size());
    System.assertEquals(2, [SELECT Id FROM ASP_Related_Objects__c].size());
  }

  @isTest
  private static void createInteractionAndSegmentAndLeadAsRelatedObject()  {
    Test.startTest();
    Task callTask = createTask('Lead');
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(1, [SELECT Id FROM ASP_Interaction__c].size());
    System.assertEquals(1, [SELECT Id FROM ASP_Interaction_Segment__c].size());
    System.assertEquals(1, [SELECT Id FROM ASP_Related_Objects__c].size());
  }

  @isTest
  private static void createInteractionAndSegmentAndAccountAsRelatedObject()  {
    Test.startTest();
    Task callTask = createTask('Account');
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(1, [SELECT Id FROM ASP_Interaction__c].size());
    System.assertEquals(1, [SELECT Id FROM ASP_Interaction_Segment__c].size());
    System.assertEquals(1, [SELECT Id FROM ASP_Related_Objects__c].size());
  }

  @isTest
  private static void createInteractionAndSegmentAndContactAsRelatedObject()  {
    Test.startTest();
    Task callTask = createTask('Contact');
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(1, [SELECT Id FROM ASP_Interaction__c].size());
    System.assertEquals(1, [SELECT Id FROM ASP_Interaction_Segment__c].size());
    System.assertEquals(2, [SELECT Id FROM ASP_Related_Objects__c].size());
  }

  @isTest
  private static void updateInteractionSegmentTest()  {
    Task callTask = createTask('Case');
    callTask.Interaction_Segment__c = [SELECT Id FROM ASP_Interaction_Segment__c WHERE Interaction_Segment_Id__c = :callTask.Id].Id;
    callTask.CallDisposition = 'Default Disposition';
    callTask.Sentiment_Score__c = 0.1;
    update callTask;
    TriggerOperation operation = TriggerOperation.AFTER_UPDATE;
    AU_TriggerEntry.Args args = createUpdateArgs(new List<Task> {callTask}, operation);

    Test.startTest();
    ASP_UpdateSegmentOnTaskCreatedHandler handler = new ASP_UpdateSegmentOnTaskCreatedHandler();
    handler.invokeMain(args, 1);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(callTask.CallDisposition, [SELECT Id, Disposition__c FROM ASP_Interaction_Segment__c WHERE Interaction_Segment_Id__c = :callTask.Id].Disposition__c);
  }

  @isTest
  private static void notSupportedTriggers() {
    Task callTask = createTask(null);
    TriggerOperation operation = TriggerOperation.BEFORE_INSERT;
    AU_TriggerEntry.Args args = createUpdateArgs(new List<Task> {callTask}, operation);

    Test.startTest();
    ASP_UpdateSegmentOnTaskCreatedHandler handler = new ASP_UpdateSegmentOnTaskCreatedHandler();
    handler.invokeMain(args, 1);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  private static void invokeWhileInProgressAndCompleted() {
    AU_TriggerEntry.Args args;
    ASP_UpdateSegmentOnTaskCreatedHandler handler = new ASP_UpdateSegmentOnTaskCreatedHandler();

    handler.invokeWhileInProgress(args, 1);
    ATU_DebugInfoUtil.assertNoErrors();
    handler.invokeCompleted(args, 1);
    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  private static void bulkTest()  {
    Test.startTest();
    List<Task> tasks = new List<Task> {
        new Task(
            WhatId = testCase.Id,
            WhoId = testContact.Id,
            Subject = 'Call',
            CallObject = '1234-5678'
        ),
        new Task(
            WhatId = testOpportunity.Id,
            WhoId = testContact.Id,
            Subject = 'Call',
            CallObject = '1234-5687'
        ),
        new Task(
          WhatId = testAccount.Id,
          WhoId = testContact.Id,
          Subject = 'Call',
          CallObject = '1234-5699'
        )
    };
    insert tasks;
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(3, [SELECT count() FROM ASP_Interaction__c]);
    System.assertEquals(3, [SELECT count() FROM ASP_Interaction_Segment__c]);
    System.assertEquals(8, [SELECT count() FROM ASP_Related_Objects__c]);
    System.assertEquals(3, [SELECT count() FROM ASP_Related_Objects__c WHERE Object_Type__c = 'Contact']);
    System.assertEquals(3, [SELECT count() FROM ASP_Related_Objects__c WHERE Object_Type__c = 'Account']);
    System.assertEquals(1, [SELECT count() FROM ASP_Related_Objects__c WHERE Object_Type__c = 'Case']);
    System.assertEquals(1, [SELECT count() FROM ASP_Related_Objects__c WHERE Object_Type__c = 'Opportunity']);
  }

  // Helper methods

  private static AU_TriggerEntry.Args createUpdateArgs(List<Task> items, TriggerOperation operation) {
    return new AU_TriggerEntry.Args(
        Task.SObjectType,
        operation,
        false,
        items,
        items,
        new Map<Id, SObject>(items),
        new Map<Id, SObject>(items)
    );
  }

  private static Task createTask(String relatedObject)  {
    Id whatId = null;
    Id whoId = null;
    String subject = 'Call';
    Decimal sentiment = null;


    switch on relatedObject {
      when 'Case' {
        whatId = testCase.Id;
        subject = 'Call';
      }
      when 'Opportunity'  {
        whatId = testOpportunity.Id;
        subject = 'SMS';
        sentiment = -0.2;
      }
      when 'Lead' {
        whoId = testLead.Id;
        subject = 'Email';
        sentiment = -0.1;
      }
      when 'Account'  {
        whatId = testAccount.Id;
        subject = 'Call';
      }
      when 'Contact'  {
        whoId = testContact.Id;
        subject = 'Call';
      }
    }

    Task callTask = new Task(
        Subject = subject,
        CallObject = '1234-5678',
        Description = 'test',
        Sentiment_Score__c = sentiment,
        WhatId = whatId,
        WhoId = whoId
    );
    insert callTask;
    return callTask;
  }
}
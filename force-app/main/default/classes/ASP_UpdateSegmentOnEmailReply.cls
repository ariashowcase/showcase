/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 10/25/2018.
 */

public without sharing class ASP_UpdateSegmentOnEmailReply implements AU_TriggerEntry.ITriggerEntry {
  public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount) {
    AU_Debugger.enterFunction('ASP_UpdateSegmentOnEmailReply.invokeMain');

    if(args.operation != TriggerOperation.AFTER_INSERT) {
      AU_Debugger.debug('Operation not supported.');
      AU_Debugger.leaveFunction();
      return;
    }

    List<EmailMessage> caseReplyEmails = getCaseReplyEmails(args);
    if (!caseReplyEmails.isEmpty()) {
      AU_Debugger.debug('Processing ' + caseReplyEmails.size() + ' Case Reply Emails...');
      updateSegments(caseReplyEmails);
    }

    AU_Debugger.leaveFunction();
  }

  public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {}
  public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {}

  private List<EmailMessage> getCaseReplyEmails(AU_TriggerEntry.Args args) {
    AU_Debugger.enterFunction('ASP_UpdateSegmentOnEmailReply.getCaseReplyEmails');

    List<EmailMessage> result = new List<EmailMessage>();

    for (EmailMessage em : (List<EmailMessage>) args.newObjects) {
      if (
        !em.Incoming && 
        em.ReplyToEmailMessageId != null && 
        em.ParentId != null && 
        em.ParentId.getSObjectType() == Case.SObjectType
      ) {
        result.add(em);
      }
    }

    AU_Debugger.leaveFunction();
    return result;
  }

  private void updateSegments(List<EmailMessage> replyEmails) {
    AU_Debugger.enterFunction('ASP_UpdateSegmentOnEmailReply.updateSegments');

    Map<Id, EmailMessage> repliesByOriginalId = new Map<Id, EmailMessage>();
    for (EmailMessage reply : replyEmails) {
      repliesByOriginalId.put(reply.ReplyToEMailMessageId, reply);
    }

    Map<Id, ASP_Interaction_Segment__c> segmentsByOriginalEmailId = new Map<Id, ASP_Interaction_Segment__c>();
    List<ASP_Interaction_Segment__c> segmentsToUpdate = new List<ASP_Interaction_Segment__c>();
    final Datetime NOW = System.now();

    for (ASP_Interaction_Segment__c segment : [
      SELECT  EmailMessage_Id__c
      FROM    ASP_Interaction_Segment__c
      WHERE   EmailMessage_Id__c in :repliesByOriginalId.keySet()
          AND CreatedById = :UserInfo.getUserId()
      ORDER BY CreatedDate
    ]) {
      EmailMessage reply = repliesByOriginalId.get(segment.EmailMessage_Id__c);
      if (reply != null) {
        segment.Reply_Date__c = NOW;
        // TODO: replace the text copy with a link to the EmailMessage (and possibly display it in a component)
        segment.Email_Reply__c = reply.TextBody;
        segmentsToUpdate.add(segment);
      }
    }
    
    if (!segmentsToUpdate.isEmpty()) {
      AU_Debugger.debug('Updating ' + segmentsToUpdate.size() + ' Interaction Segments...');
      update segmentsToUpdate;
    }

    AU_Debugger.leaveFunction();
  }
}
/**
 * Created by jfischer on 10/18/2016.
 */

@isTest
private class ASP_AgentCaseBacklogLtngControllerTest {

  private static User AgentSmith {
    get {
      if (AgentSmith == null) {
        AgentSmith = [SELECT Id, FirstName, LastName, Username FROM User WHERE LastName = 'smith-ASP_AgentCaseBacklogLtngControllerTest'];
      }
      return AgentSmith;
    }
  }

  private static User AgentMiller {
    get {
      if (AgentMiller == null) {
        AgentMiller = [SELECT Id, FirstName, LastName, Username FROM User WHERE LastName = 'miller-ASP_AgentCaseBacklogLtngControllerTest'];
      }
      return AgentMiller;
    }
  }

  @testSetup static void setup() {
    insert ATU_UserTestUtil.createStandardUser('agent', 'smith-ASP_AgentCaseBacklogLtngControllerTest');
    insert ATU_UserTestUtil.createStandardUser('agent', 'miller-ASP_AgentCaseBacklogLtngControllerTest');
  }

  @isTest static void testCreateAgentWorkRecord_AgentInInvalidState() {
    Case c = ATU_CaseTestUtil.insertCaseForOwner(AgentSmith.Id, 'Case 1');

    System.assertEquals(0, [SELECT count() FROM AgentWork]);

    System.runAs(AgentSmith) {

      Test.startTest();
      ASP_AgentCaseBacklogLtngController.createAgentWorkRecord(c.Id);

      System.assertEquals(0, [SELECT count() FROM AgentWork]);
      Test.stopTest();

    }
  }
}
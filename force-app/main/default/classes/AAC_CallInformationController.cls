/** Copyright 2017 Aria Solutions Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

global with sharing class AAC_CallInformationController {
  public static final String CACHE_PARTITION_PREFIX = 'ACTCallInfo';
  public static final String CACHE_TEST_KEY = 'TESTKEY';
  public static final String CACHE_TEST_VALUE = 'TEST-VALUE';

  public AAC_CallInformationController(AAC_NamespaceController controller) { }

  @RemoteAction
  global static Boolean isPlatformCacheAvailable() {
    try {
      Cache.Org.put(CACHE_PARTITION_PREFIX + CACHE_TEST_KEY, CACHE_TEST_VALUE, 301);
      String cacheVal = (String) Cache.Org.get(CACHE_PARTITION_PREFIX + CACHE_TEST_KEY);
      return cacheVal == CACHE_TEST_VALUE;
    }
    catch (Exception e) {
      return false;
    }
  }

  @RemoteAction
  global static void storeCallInformation(string properties) {
    Map<String, String> callInformation = (Map<String, String>) JSON.deserialize(properties, Map<String, String>.class);
    String cid = callInformation.get('contactId').replace('-', '');
    Cache.Org.put(CACHE_PARTITION_PREFIX + cid, properties, 7200);
  }

  @RemoteAction
  global static Map<String, String> returnCallInformation(string contactId) {
    Map<String, String> callInformation = new Map<String, String>();

    if (!String.isBlank(contactId)) {
      String cid = contactId.replace('-', '');
      String information = (String) Cache.Org.get(CACHE_PARTITION_PREFIX + cid);
      if (information != null) {
        callInformation = (Map<String, String>) JSON.deserialize(information, Map<String, String>.class);
      }
    }

    return callInformation;
  }
}
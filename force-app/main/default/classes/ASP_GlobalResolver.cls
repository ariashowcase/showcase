/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * List of skill requirement resolvers defined for the particular org.
 * This class should be customized for each org.
 *
 * Created by jliu on 6/14/2018.
 */

public without sharing class ASP_GlobalResolver {
  public static List<SkillRequirement> resolveSkills(List<PendingServiceRouting> routingItems) {
    return manager.createSkillRequirements(routingItems);
  }

  private static final Map<SObjectType, List<ASP_SkillRequirementResolver>> resolversByType;
  private static ASP_SkillRequirementManager manager;

  static {
    resolversByType = new Map<SObjectType, List<ASP_SkillRequirementResolver>> {
      Case.SObjectType => new List<ASP_SkillRequirementResolver> {
        /* replace with actual resolvers
        new Test_CaseConnectSkillReqResolver(),
        new Test_CaseTechSkillReqResolver()
        */
      }
    };

    manager = new ASP_SkillRequirementManager(resolversByType);
  }

  @TestVisible
  private static void resetWithResolvers(Map<SObjectType, List<ASP_SkillRequirementResolver>> resolvers) {
    manager = new ASP_SkillRequirementManager(resolvers);
  }

  @TestVisible
  private static void resetWithResolver(SObjectType type, ASP_SkillRequirementResolver resolver) {
    Map<SObjectType, List<ASP_SkillrequirementResolver>> resolvers = new Map<SObjectType, List<ASP_SkillRequirementResolver>> {
      type => new List<ASP_SkillRequirementResolver> { resolver }
    };

    manager = new ASP_SkillRequirementManager(resolvers);
  }
}
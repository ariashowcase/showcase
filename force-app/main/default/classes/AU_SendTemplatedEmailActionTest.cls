/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 30-Aug-18. 
 */

@isTest
public class AU_SendTemplatedEmailActionTest {

  private static final String ADMINUSERNAME = 'AdminUser@aria.test';

  private static Account testAccount { get; set; }
  private static Contact testContact { get; set; }
  private static Case testCase { get; set; }
  private static User adminUser {
    get {
      if (adminUser == null) {
        adminUser = [SELECT Id FROM User WHERE UserName = :ADMINUSERNAME];
      }
      return adminUser;
    }
    set;
  }

  @testSetup static void setup() {
    ATU_UserFactory.createDefaultBuilder('Aria', 'Admin')
        .withAdminProfile()
        .withField('Username', ADMINUSERNAME)
        .persist();
  }

  @isTest static void sendTest_Success() {
    System.assertEquals(0, Limits.getEmailInvocations());

    Test.startTest();
    AU_SendTemplatedEmailAction.send(createArgs(true));
    System.assertEquals(1, Limits.getEmailInvocations());
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest static void sendTest_Failure() {

    Test.startTest();
    Boolean isExceptionThrown = false;
    try {
      AU_SendTemplatedEmailAction.send(createArgs(false));
    } catch (AU_SendTemplatedEmailAction.SendEmailException ex)  {
      isExceptionThrown = true;
    }
    Test.stopTest();

    System.assertEquals(true, isExceptionThrown);
  }

  private static List<AU_SendTemplatedEmailAction.EmailArgs> createArgs(Boolean isRecipientExists) {
    createContact();
    createCase();

    String emailAddressName = [SELECT DisplayName FROM OrgWideEmailAddress LIMIT 1].DisplayName;

    AU_SendTemplatedEmailAction.EmailArgs args = new AU_SendTemplatedEmailAction.EmailArgs();
    System.runAs(adminUser) {
      args.fromAddressDisplayName = emailAddressName;
      args.recipientId = (isRecipientExists) ? testContact.Id : null;
      args.emailTemplateName = createEmailTemplate().Name;
      args.WhatId = testCase.Id;
      args.replyToAddress = 'support@test.com';
    }
    System.debug('args: ' + args);
    return new List<AU_SendTemplatedEmailAction.EmailArgs>{
        args
    };
  }

  private static void createContact() {
    testAccount = ATU_AccountFactory.createDefaultBuilder('testAccount').persist();
    testContact = ATU_ContactFactory.createDefaultBuilder('Doe', testAccount.Id)
        .withFirstName('John')
        .withField('Email', 'john.doe.test@test.com')
        .persist();
  }

  private static void createCase()  {
    testCase = ATU_CaseFactory.createDefaultBuilder('testCase')
        .withField('ContactId', testContact.Id)
        .persist();
  }

  static EmailTemplate createEmailTemplate() {
    EmailTemplate newTemplate = new EmailTemplate(
        Name = 'Test Template',
        DeveloperName = 'TestTemplate',
        TemplateType = 'text',
        FolderId = UserInfo.getUserId(),
        IsActive = true
    );
    insert newTemplate;
    return newTemplate;
  }

}
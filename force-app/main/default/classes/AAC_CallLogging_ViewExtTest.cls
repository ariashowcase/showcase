/** Copyright 2017 Aria Solutions Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

@IsTest
private class AAC_CallLogging_ViewExtTest {

	@TestSetup
	static void setup()
	{
		Account a = new Account(Name='Test Acct 1');
		insert a;
		Contact c = new Contact(LastName='Test', FirstName='User', Phone='403-681-3632', AccountId = a.Id);
		insert c;
		Task t = new Task(Subject='Call', CallObject='bc9a4383-4497-4cca-82e6-04d040562d17', WhoId=c.Id, WhatId=a.Id);
		insert t;
	}

	@isTest
	static void testGetPhoneTasks() {
		Account a = [SELECT Id FROM Account WHERE Name='Test Acct 1'];
		Contact c = [SELECT Id FROM Contact WHERE LastName='Test'];
		PageReference pageRef = Page.AAC_CallLogging_View;
		Test.setCurrentPage(pageRef);
		pageRef.getParameters().put('Id', a.Id);
		ApexPages.StandardController sc = new ApexPages.StandardController(a);
		AAC_CallLogging_ViewExt callLogging = new AAC_CallLogging_ViewExt(sc);
		Test.startTest();
		List<AAC_CallLogging_ViewExt.TaskwithPhone> tasks = callLogging.getPhoneTasks();
		Test.stopTest();

		System.assertEquals(1, tasks.size());
		System.assertEquals(c.Id, tasks[0].task.WhoId);
	}
}
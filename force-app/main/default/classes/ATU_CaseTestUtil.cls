/**
 * Created by jfischer on 7/28/2016.
 */

@IsTest
public class ATU_CaseTestUtil {

  public static Case createCase(String subject) {
    return new Case(
      Subject = subject,
      Status = 'New',
      Origin = 'Web'
    );
  }

  public static Case insertCaseForOwner(Id ownerId, String subject) {
    Case c = createCase('Test subject');
    insert c;

    c = [SELECT OwnerId FROM Case WHERE Id = :c.Id];
    c.OwnerId = ownerId;
    update c;

    return [SELECT OwnerId FROM Case WHERE Id = :c.Id];
  }

  public static String getClosedStatus() {
    CaseStatus cs = [SELECT MasterLabel FROM CaseStatus WHERE IsClosed = true];
    return cs.MasterLabel;
  }
}
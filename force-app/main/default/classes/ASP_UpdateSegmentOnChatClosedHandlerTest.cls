/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 08-Nov-18. 
 */

@isTest 
public class ASP_UpdateSegmentOnChatClosedHandlerTest {

  @isTest
  static void updateSegmentAfterChatClosedTest()    {
    

    LiveChatTranscript transcript = createLiveChatTranscript();

    AgentWork aw = new AgentWork(
        Id = '0Bz000000000001001',
        WorkItemId = transcript.Id,
        UserId = UserInfo.getUserId()
    );

    TriggerOperation operation = TriggerOperation.AFTER_UPDATE;
    AU_TriggerEntry.Args args = createUpdateArgs(new List<LiveChatTranscript> {transcript}, operation);
    Id interactionId = createInteraction(transcript.Id);
    ASP_Interaction_Segment__c segment = createSegment(interactionId, aw);

    Test.startTest();
    ASP_UpdateSegmentOnChatClosedHandler handler = new ASP_UpdateSegmentOnChatClosedHandler();
    handler.invokeMain(args, 1);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(transcript.WaitTime, [SELECT Queue_Time__c FROM ASP_Interaction_Segment__c LIMIT 1].Queue_Time__c);
  }

  @isTest
  static void notSupportedTriggers()  {
    

    LiveChatTranscript transcript = createLiveChatTranscript();

    TriggerOperation operation = TriggerOperation.BEFORE_INSERT;
    AU_TriggerEntry.Args args = createUpdateArgs(new List<LiveChatTranscript> {transcript}, operation);

    Test.startTest();
    ASP_UpdateSegmentOnChatClosedHandler handler = new ASP_UpdateSegmentOnChatClosedHandler();
    handler.invokeMain(args, 1);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  static void invokeWhileInProgressAndCompleted() {
    AU_TriggerEntry.Args args;
    ASP_UpdateSegmentOnChatClosedHandler handler = new ASP_UpdateSegmentOnChatClosedHandler();
    handler.invokeWhileInProgress(args, 1);
    ATU_DebugInfoUtil.assertNoErrors();
    handler.invokeCompleted(args, 1);
    ATU_DebugInfoUtil.assertNoErrors();
  }

  // Helper methods

  static AU_TriggerEntry.Args createUpdateArgs(List<LiveChatTranscript> items, TriggerOperation operation) {
    return new AU_TriggerEntry.Args(
        LiveChatTranscript.SObjectType,
        operation,
        false,
        items,
        items,
        new Map<Id, SObject>(items),
        new Map<Id, SObject>(items)
    );
  }

  static LiveChatTranscript createLiveChatTranscript()  {
    LiveChatTranscript transcript = new LiveChatTranscript(
        RequestTime = DateTime.newInstance(2018, 11, 18, 3, 3, 3),
        StartTime = DateTime.newInstance(2018, 11, 18, 3, 4, 3),
        EndTime = DateTime.newInstance(2018, 11, 18, 3, 5, 3),
        Body = 'Test Body',
        LiveChatVisitorId = createLiveChatVisitor());
    insert transcript;
    return transcript;
  }

  static Id createLiveChatVisitor() {
    LiveChatVisitor visitor = new LiveChatVisitor();
    insert visitor;
    return visitor.Id;
  }

  static Id createInteraction(Id workItemId)  {
    ASP_Interaction__c interaction = new ASP_Interaction__c(Interaction_Id__c = workItemId);
    insert interaction;
    return interaction.Id;
  }

  static ASP_Interaction_Segment__c createSegment(Id interactionId, AgentWork aw) {
    ASP_Interaction_Segment__c newSegment = new ASP_Interaction_Segment__c(
          Interaction_Segment_Id__c = aw.WorkItemId + '' + aw.UserId,
          RecordTypeId = AU_RecordTypes.getId('ASP_Interaction_Segment__c', 'Chat'),
          ASP_Interaction__c = interactionId);

    insert newSegment;
    return newSegment;
  }
    
}
public class Velocity_CaseTransfer {
    private final ApexPages.StandardController controller;
    
    public Velocity_CaseTransfer(ApexPages.StandardController controller) {
        this.controller = controller;
    }
    
    public PageReference GoBack() {
        return this.controller.view();
    }
    
    @RemoteAction
    public static void UpdateOwner(String caseId, String newOwnerId) {
        Case c = [Select Id, OwnerId from Case where Id = :caseId];
        c.OwnerId = newOwnerId;
        update c;
    }
    
    @RemoteAction
    public static String CreateOpportunity(String caseId) {
        Case c = [Select Id, AccountId, OwnerId from Case where Id = :caseId];
        
        Opportunity o = new Opportunity(AccountId=c.AccountId, CloseDate=Date.today(), Name='Case: ' + c.Id, OwnerId=c.OwnerId, StageName='Qualification');
        
        insert o;
        
        return o.Id;
    }
}
/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */

global class ASP_LaunchFulfillmentRequestFlowAction {

  @InvocableMethod(label='Start Fulfillment Flow' description='This action will start a flow based on the intend and origin, and pass the Payload as variables to it.')
  global static void launchFlows(List<ASP_Fulfillment_Request__e> requests) {

    Map<String, ASP_Fulfillment_Request_Mapping__mdt> intendMappings = getMappings();
    try
    {
      AU_Debugger.debug('Number of Fulfillment Requests to process: ' + requests.size());
      for (ASP_Fulfillment_Request__e request : requests) {
        AU_Debugger.debug(String.format('Processing request for intent "{0}" from "{1}"', new String[] { request.Intent__c, request.Origin__c} ));

        ASP_Fulfillment_Request_Mapping__mdt mapping = intendMappings.get(request.Intent__c);
        if (mapping != null) {

          Flow.Interview theFlow = String.isBlank(mapping.Namespace__c) ?
            Flow.Interview.createInterview(mapping.Flow_Name__c, getVariables(request)) :
            Flow.Interview.createInterview(mapping.Namespace__c, mapping.Flow_Name__c, getVariables(request));

          AU_Debugger.debug(String.format('Starting flow with name "{0}".', new String[] { mapping.Flow_Name__c } ));
          theFlow.start();
        } else {
          AU_Debugger.debug(String.format('No active mapping found. Ignore request.', new String[] { mapping.Flow_Name__c } ));
        }
      }
    } catch (Exception ex) {
      AU_Debugger.reportException('ASP_LaunchFulfillmentRequestFlowHandler', ex);
    }
  }

  private static Map<String, Object> getVariables(ASP_Fulfillment_Request__e request) {
    Map<String, Object> result = new Map<String, Object>();

    result.put('fr_intent', request.Intent__c);
    result.put('fr_contactId', request.ContactId__c);
    result.put('fr_origin', request.Origin__c);

    if (String.isBlank(request.Payload__c)) {
      AU_Debugger.debug('No payload found.');
      return result;
    }

    Map<String, Object> payload = (Map<String, Object>) JSON.deserializeUntyped(request.Payload__c);

    for (String key : payload.keyset()) {
      result.put('frx_' + key, payload.get(key));
    }

    return result;
  }

  private static Map<String, ASP_Fulfillment_Request_Mapping__mdt> getMappings() {
    Map<String, ASP_Fulfillment_Request_Mapping__mdt> result = new Map<String, ASP_Fulfillment_Request_Mapping__mdt>();

    AU_Debugger.debug('Requesting fulfillment mappings');
    for (ASP_Fulfillment_Request_Mapping__mdt mapping : [SELECT Id, Intent__c, Flow_Name__c, Namespace__c FROM ASP_Fulfillment_Request_Mapping__mdt]) {
      result.put(mapping.Intent__c, mapping);
    }

    AU_Debugger.debug('Number of mappings found: ' + result.size());
    return result;
  }
}
/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 7/17/2018.
 */

public with sharing class AAC_SupportCallbackFormController {
  private static final String CLASS_NAME = 'AAC_SupportCallbackFormController';

  @AuraEnabled
  public static Contact getUserContact() {
    Id userId = UserInfo.getUserId();
    List<User> users = [
      select  Contact.Phone, Contact.Name
      from    User
      where   Id = :userId
      and ContactId != null
    ];

    if (users.size() != 1)
      return null;

    return users[0].Contact;
  }

  @AuraEnabled
  public static void createCallCampaign(Map<String, String> formInput) {
    AU_Debugger.enterFunction(CLASS_NAME + '.createCallCampaign');

    try {
      ClientInfo info = new ClientInfo(formInput);

      AAC_Call_Campaign__c cc = new AAC_Call_Campaign__c(
        Contact__c = info.contactId,
        Phone_Number__c = info.phone,
        Comments__c = info.comments
      );

      if (info.contactId == null) {
        cc.Comments__c =
          'First Name: ' + info.firstName + '\n' +
          'Last Name: ' + info.lastName + '\n\n' +
          'Comments:\n' + info.comments;
      }

      AU_Debugger.debug('Creating Call Campaign with field values: ' + String.valueOf(cc));
      insert cc;
    }
    catch (Exception e) {
      AU_Debugger.reportException(CLASS_NAME, e);
      throw e;
    }
    finally {
      AU_Debugger.leaveFunction();
    }
  }

  @AuraEnabled
  public static void createOutboundCall(Map<String, String> formInput, Map<String, String> acInfo) {
    AU_Debugger.enterFunction(CLASS_NAME + '.createOutboundCall');

    try {
      ClientInfo clientInfo = new ClientInfo(formInput);
      AWS_ConnectContact.StartContactRequestBody request = createAWSStartContactRequestBody(acInfo, clientInfo);

      String accessKey = acInfo.get('accessKey');
      String accessSecret = acInfo.get('accessSecret');
      String region = acInfo.get('region');
      AWS_Connector connector = new AWS_Connector(accessKey, accessSecret, region);
      AWS_ConnectContact contactConnector = new AWS_ConnectContact(connector);
      contactConnector.StartOutboundVoiceContact(request);
    }
    catch (Exception e) {
      AU_Debugger.reportException(CLASS_NAME, e);
      throw e;
    }
    finally {
      AU_Debugger.leaveFunction();
    }
  }

  private static AWS_ConnectContact.StartContactRequestBody createAWSStartContactRequestBody(
    Map<String, String> acInfo,
    ClientInfo clientInfo
  ) {
    AWS_ConnectContact.StartContactRequestBody request = new AWS_ConnectContact.StartContactRequestBody();
    request.ClientToken = '';
    request.ContactFlowId = acInfo.get('contactFlowId');
    request.InstanceId = acInfo.get('instanceId');
    request.QueueId = acInfo.get('queueId');
    request.SourcePhoneNumber = acInfo.get('sourcePhoneNumber');
    request.DestinationPhoneNumber = clientInfo.phone;

    request.Attributes = new Map<String, String>();
    if (clientInfo.contactId != null)
      request.Attributes.put('sf_contact_id', clientInfo.contactId);
    request.Attributes.put('first_name', clientInfo.firstName);
    request.Attributes.put('last_name', clientInfo.lastName);
    request.Attributes.put('comments', clientInfo.comments);
    return request;
  }

  private class ClientInfo {
    Id contactId;
    String firstName;
    String lastName;
    String phone;
    String comments;

    ClientInfo(Map<String, String> inputMap) {
      contactId = (Id) inputMap.get('contactId');
      firstName = inputMap.get('firstName');
      lastName = inputMap.get('lastName');
      phone = inputMap.get('phone');
      comments = inputMap.get('comments');
    }
  }
}
/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@RestResource(urlMapping='/self-service/knowledge/email')
global class ASP_EmailKnowledgeArticleEndpoint {

  private static final Integer OK = 200;

  @HttpPost
  global static Integer emailKnowledgeArticleToContact(String contactId, String knowledgeArticleId) {

    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    mail.setSenderDisplayName('ARIA Support');
    mail.setTargetObjectId(contactId);
    mail.setReplyTo('ariainnovations@gmail.com');

    SObject article = getKnowledgeArticleDetails(getKnowledgeArticleVersion(knowledgeArticleId));

    mail.setSubject((String) article.get('Title'));
    mail.setHtmlBody((String) article.get('ArticleBody__c'));

    Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{ mail });

    return OK;
  }

  private static SObject getKnowledgeArticleDetails(KnowledgeArticleVersion kaVersion) {
    String query = 'SELECT Title, Summary, ArticleBody__c FROM ' + kaVersion.ArticleType + '__kav WHERE KnowledgeArticleId = ' + kaVersion.KnowledgeArticleId;

    List<SObject> result = Database.query(query);
    return result.get(0);
  }

  private static KnowledgeArticleVersion getKnowledgeArticleVersion(String knowledgeArticleId) {
    return [SELECT Id,Title,UrlName,Summary,ArticleType,KnowledgeArticleId
      FROM KnowledgeArticleVersion
      WHERE KnowledgeArticleId = :knowledgeArticleId AND PublishStatus ='Online'];
  }
}
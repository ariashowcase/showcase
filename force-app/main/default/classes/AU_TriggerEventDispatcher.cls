/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
public class AU_TriggerEventDispatcher {
	private static String CLASS_NAME = 'AU_TriggerEventDispatcher';
	
  private List<AU_TriggerEntry.ITriggerEntry> handlers;
  private AU_TriggerEntry.ITriggerEntry activeFunction = null;
  private Map<AU_TriggerEntry.Args, Integer>  triggerEventInvocationCounts = new Map<AU_TriggerEntry.Args, Integer>();

  public AU_TriggerEventDispatcher(List<AU_TriggerEntry.ITriggerEntry> initialHandlers) {
    AU_Check.NotNull(initialHandlers, 'AU_TriggerEventDispatcher.handlers');
    handlers = initialHandlers;
  }
  
  public void dispatch(AU_TriggerEntry.Args args) {
    Integer newInvocationCount = incrementAndGetInvocationCount(args);
    AU_Debugger.enterFunction('AU_TriggerEventDispatcher.dispatch: ' + 'invocation: ' + newInvocationCount + '; ' + args.toString());

    AU_Debugger.debug('Current Limits: DML Statements=' + Limits.getDmlStatements() + ', Queries=' + Limits.getQueries() + ', Callouts=' + Limits.getCallouts());
    
    if (activeFunction != null) {
      for (AU_TriggerEntry.ITriggerEntry handler : handlers) {
        safeInvokeWhileInProgress(handler, args); 
      }
      return;
    }

    for (AU_TriggerEntry.ITriggerEntry handler : handlers) {
      safeInvokeMain(handler, args);  
    }
    
    for(AU_TriggerEntry.ITriggerEntry handler : handlers){
    	safeInvokeCompleted(handler, args);
    }
    
    AU_Debugger.leaveFunction();
  }
  
  private void safeInvokeWhileInProgress(AU_TriggerEntry.ITriggerEntry handler, AU_TriggerEntry.Args args) {
    AU_Debugger.enterFunction('AU_TriggerEventDispatcher.safeInvokeWhileInProgress');
    try {
      handler.invokeWhileInProgress(args, triggerEventInvocationCounts.get(args));
    } catch (Exception ex) {
      AU_Debugger.reportException(CLASS_NAME, ex);
    }
    AU_Debugger.leaveFunction();
  }
  
  private void safeInvokeMain(AU_TriggerEntry.ITriggerEntry handler, AU_TriggerEntry.Args args) {
    AU_Debugger.enterFunction('AU_TriggerEventDispatcher.safeInvokeMain');
    try {
      activeFunction = handler;
      handler.invokeMain(args, triggerEventInvocationCounts.get(args));
    } catch (Exception ex) {
      AU_Debugger.reportException(CLASS_NAME, ex);
    }
    activeFunction = null;
    AU_Debugger.leaveFunction();
  }
  
  private void safeInvokeCompleted(AU_TriggerEntry.ITriggerEntry handler, AU_TriggerEntry.Args args){
  	AU_Debugger.enterFunction('AU_TriggerEventDispatcher.safeInvokeCompleted');
  	try {
      activeFunction = handler;
      handler.invokeCompleted(args, triggerEventInvocationCounts.get(args));
    } catch (Exception ex) {
      AU_Debugger.reportException(CLASS_NAME, ex);
    }
    activeFunction = null;
  	AU_Debugger.leaveFunction();
  }

  private Integer incrementAndGetInvocationCount(AU_TriggerEntry.Args args) {
    if (!triggerEventInvocationCounts.containsKey(args)) {
      triggerEventInvocationCounts.put(args, 1);
      return 1;
    }

    Integer result = triggerEventInvocationCounts.get(args) + 1;
    triggerEventInvocationCounts.put(args, result);

    return result;
  }
}
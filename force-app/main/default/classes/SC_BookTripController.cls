/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Friday August 9th 2019
 * Author: varonov
 * File type: '.cls'
 */


public with sharing class SC_BookTripController {

  public Id accountId { get; private set; }
  public Id contactId {get; set;}
  public String origin {get; set;}
  public String destination {get; set;}
  public String callToast {get; set;}
  public Date flightDate {get; set;}
  public Boolean flightIsFound {get; set;}
  public Boolean tripAdded {get; set;}
  public Boolean tripIsReviewed {get; set;} 
  public Boolean showUpgradePackage {get; set;}
  public Boolean tripBooked {get; set;}
  public SC_BookingSessionService.BookingData bookingData {get; set;}
  public Map<Id,String> selectedContactsMap {get; set;}
  public List<SC_FlightService.FlightGroup> groups {get; set;}
  public List<Flight__c> flights {get; set;}
  public List<Booked_Seat__c> bookedSeats {get; set;}
  public List<FlightPackage> flightPackage {get; set;}
  public SelectOption[] allContacts {get; set;}
  public SelectOption[] selectedContacts {get; set;}
  public SelectOption[] insurances {get; set;}
  public SelectOption[] selectedInsurances {get; set;}
  public SelectOption[] meals {get; set;}
  public List<String> confirmationNumber {get; set;}
  @TestVisible 
  private List<Id> contactIds {get; set;}
  @TestVisible 
  private List<Id> selectedFlightIds {get; set;}
  @TestVisible 
  private List<List<Id>> flightIdsPerTrip {get; set;}

  public SC_BookTripController(ApexPages.StandardController controller) {
    accountId = controller.getId();
  }

  public Pagereference searchFlights()  {   
    DateTime startTime = DateTime.newInstance(flightDate.year(), flightDate.month(), flightDate.day());
    Datetime endTime = startTime.AddDays(2);
    origin = convertToCityFormat(origin);
    destination = convertToCityFormat(destination);
    if(!Test.isRunningTest()) {
      groups = SC_FlightService.getFlightGroups(origin, destination, startTime, endTime);
    }
    if (groups.size() > 0) {
      flightIsFound = true;
      callToast = null;
    }
    else {
      flightIsFound = false;
      callToast = '<script> showToast("No available flights found", "warning"); </script>';
    }
    tripAdded = false;
    tripBooked = false;
    
    return null;
  }

  public void addTrip()  {
    flightIsFound = false;
    tripAdded = true;
    SC_FlightService.FlightGroup selectedGroup = new SC_FlightService.FlightGroup();

    if (apexpages.currentpage().getparameters().get('hiddenGroupNumber') != null)  {
      Integer selectedGroupNumber = Integer.ValueOf(apexpages.currentpage().getparameters().get('hiddenGroupNumber'));
      selectedGroup = groups[selectedGroupNumber-1];
    }

    if (flights == null || flights.isEmpty()) {
      selectedFlightIds = new List<Id>();

      for (SC_FlightService.FlightWrapper flight : selectedGroup.flights)  {
        selectedFlightIds.add(flight.flightId);
      }

      getSelectedFlights(selectedFlightIds);
    }

    tripIsReviewed = false;
    allContacts = new List<SelectOption>();
      for(Contact con :[SELECT Id, FirstName, LastName, Name FROM Contact WHERE AccountId = :accountId]){
      allContacts.add(new SelectOption(con.Id, con.Name));
    }
    selectedContacts = new List<SelectOption>();
  }

  public List<SelectOption> getSelectedSeatTypes(){
    List<SelectOption> lstSeatTypes = new List<SelectOption>();
    Schema.DescribeFieldResult seatTypesPicklist = Booked_Seat__c.Seat_Type__c.getDescribe();
    List<Schema.PicklistEntry> pleSeat = seatTypesPicklist.getPicklistValues();
    for(Schema.PicklistEntry pickListVal : pleSeat)  {
      lstSeatTypes.add(new SelectOption(pickListVal.getLabel(), pickListVal.getValue()));
    }
    return lstSeatTypes; 
  }

  public List<SelectOption> getSelectedMeals(){
    List<SelectOption> lstMeals = new List<SelectOption>();
    Schema.DescribeFieldResult mealPicklist = Booked_Seat__c.Meal__c.getDescribe();
    List<Schema.PicklistEntry> pleMeal = mealPicklist.getPicklistValues();
    for(Schema.PicklistEntry pickListVal : pleMeal)  {
      lstMeals.add(new SelectOption(pickListVal.getLabel(), pickListVal.getValue()));
    }
    return lstMeals; 
  }

  public void reviewTrip()  {
    selectedContactsMap = new Map<Id,String>();
    if (selectedContacts != null)  {
      contactIds = new List<Id>();
      for (SelectOption so : selectedContacts)  {
        Id contactId = Id.valueOf(so.getValue());
        contactIds.add(contactId);
        selectedContactsMap.put(Id.valueOf(so.getValue()), so.getLabel());
      }
    }
    else  {
      List<Contact> contacts = [SELECT Id, Name FROM Contact WHERE Id IN :contactIds];
      for (Contact con : contacts)  {
        selectedContactsMap.put(con.Id, con.Name);
      }
    }
    if (flightIdsPerTrip == null) {
      flightIdsPerTrip = new List<List<Id>>();
      flightIdsPerTrip.add(selectedFlightIds);
    }
    
    insurances = new List<SelectOption>();
    Schema.DescribeFieldResult insurancePicklist = Booked_Seat__c.Insurance__c.getDescribe();
    List<Schema.PicklistEntry> pleInsurance = insurancePicklist.getPicklistValues();
    for(Schema.PicklistEntry pickListVal : pleInsurance)  {
      insurances.add(new SelectOption(pickListVal.getLabel(), pickListVal.getValue()));
    }

    flightPackage = new List<FlightPackage>();
    for (Flight__c flight : flights)  {
      for (Id key : selectedContactsMap.keySet())  {
        flightPackage.add(new FlightPackage(
          flight.Id,
          flight.Name,
          key,
          selectedContactsMap.get(key),
          flight.Origin__c,
          flight.Destination__c,
          flight.Flight_Date_Time__c,
          '',
          ''
        ));
      }
    }

    showUpgradePackage = true;
    tripAdded = false;
    selectedInsurances = new List<SelectOption>();
  }

  public void saveTrip()  {
    List<String> insurances = new List<String>();
    for (SelectOption so : selectedInsurances)  {     
      insurances.add(so.getValue());
    }

    List<SC_TripService.SeatOptions> seatOptions = new List<SC_TripService.SeatOptions>();
    for (FlightPackage pack : flightPackage)  {
      seatOptions.add(new SC_TripService.SeatOptions(
        pack.flightId,
        pack.contactId,
        pack.seatType,
        insurances,
        pack.meals.replace('[','').replace(']','').split(',')
      ));
    }
    createTrip(seatOptions);
  }

  public void completeBooking()  {
    if (ApexPages.currentPage().getParameters().get('sessionKey') != null)  {
      String sessionKey = System.currentPagereference().getParameters().get('sessionKey');
      bookingData = getBookingData(sessionKey);
    }
    if (bookingData != null)  {
      if (bookingData.accountId != null)  {
        accountId = bookingData.accountId;
      }
      else  {
        refreshPage();
      }
      if (String.isNotBlank(bookingData.origin) && String.isNotBlank(bookingData.destination) && bookingData.travelDate != null)  {
        if (bookingData.flightIdsPerTrip != null && bookingData.flightIdsPerTrip.size() > 0)  {
          flightIdsPerTrip = bookingData.flightIdsPerTrip;
          if (bookingData.seatOptions != null && bookingData.seatOptions.size() > 0) {
            if (bookingData.contactIds != null) {
              contactIds = bookingData.contactIds;
              createTrip(bookingData.seatOptions);
            }
          }
          else  {
            List<Id> flightIds = new List<Id>();
            for (List<Id> flightId : flightIdsPerTrip)  {
              flightIds.addAll(flightId);
            }
            getSelectedFlights(flightIds);
            if (bookingData.contactIds != null) {
              contactIds = bookingData.contactIds;
              reviewTrip();
            }
            else  {
              addTrip();
            }
          }
        }
        else  {
          origin = bookingData.origin;
          destination = bookingData.destination;
          flightDate = bookingData.travelDate;
          searchFlights();
        }
      }
      else  {
        if (String.isNotBlank(bookingData.origin))  {
          origin = bookingData.origin;
        } 
        if (String.isNotBlank(bookingData.destination)) {
          destination = bookingData.destination;
        } 
        if (bookingData.travelDate != null)  {
          flightDate = bookingData.travelDate;
        }
      }
    }
  }

  public Pagereference redirectToAccount() {
    PageReference pageRef = new PageReference('/' + accountId);
    pageRef.setredirect(true);
    return pageRef;
  }

  private void getSelectedFlights(List<Id> flightIds)  {
    flights = [SELECT Id, Name, Origin__c, Destination__c, Flight_Date_Time__c, Flight_Status__c FROM Flight__c WHERE Id IN :flightIds ORDER BY Flight_Date_Time__c];
  }

  private void createTrip(List<SC_TripService.SeatOptions> seatOptions) {
    if(!Test.isRunningTest())  {
      List<SC_TripService.SeatOptions> seatPackage = new List<SC_TripService.SeatOptions>();
      System.debug('accountId: ' + accountId);
      System.debug('contactIds: ' + contactIds);
      System.debug('flightIdsPerTrip: ' + flightIdsPerTrip);
      System.debug('seatOptions: ' + seatOptions);
      confirmationNumber = SC_TripService.bookTrips(accountId, contactIds, flightIdsPerTrip, seatOptions).tripConfirmationNos;

      bookedSeats = [
          SELECT Id, Name, Contact__r.Name, Flight__r.Origin__c, Flight__r.Destination__c, Flight_Date_Time__c, Insurance__c, Meal__c, Seat_Type__c
          FROM Booked_Seat__c
          WHERE Trip__r.Confirmation_Number__c IN :confirmationNumber
      ];
    }
    tripBooked = true;
    showUpgradePackage = false;
  }

  private SC_BookingSessionService.BookingData getBookingData(String key) {
    System.debug('key: ' + key);
    SC_BookingSessionService.BookingData sessionData = new SC_BookingSessionService.BookingData();
    try {
      sessionData = SC_BookingSessionService.retrieveData(key);
    } catch (Exception e) {
      System.debug('Error: ' + e.getMessage());
      return null;
    }
    System.debug('sessionData: ' + sessionData);
    return (sessionData != null) ? sessionData : null;
  }

  private PageReference refreshPage() {
    PageReference pageRef = ApexPages.currentPage();
    pageRef.setredirect(true);
    return pageRef;
  }

  private String convertToCityFormat(String Name)  {
    String result = '';
    if (Name != null && Name != '') {    	
      for (String iter : Name.split('[ ]+')) {    		
        if (iter != null && iter != '') {	    	
          if (iter.length() > 1) { 	      			
            result += iter.substring(0,1).toUpperCase() + iter.substring(1,iter.length()) + ' ';	   	 		
          }	    		
          else {	      			
            result += iter.substring(0,1).toUpperCase() + ' ';	    		
          }    		
        }		
      }		
      result = result.substring(0, result.length() - 1);  	
    } 
    return result;
  }

  public class FlightPackage  {
    public Id flightId {get; set;}
    public String flightName {get; set;}
    public Id contactId {get; set;}
    public String contactName {get; set;}
    public String origin {get; set;}
    public String destination {get; set;}
    public Datetime flightDateTime {get; set;}
    public String seatType {get; set;}
    public String meals {get; set;}

    public FlightPackage(Id flightId, String flightName, Id contactId, String contactName, String origin, String destination, Datetime flightDateTime, String seatTypes, String meals)  {
      this.flightId = flightId;
      this.flightName = flightName;
      this.contactId = contactId;
      this.contactName = contactName;
      this.origin = origin;
      this.destination = destination;
      this.flightDateTime = flightDateTime;
      this.seatType = seatTypes;
      this.meals = meals;
    }
  }
}
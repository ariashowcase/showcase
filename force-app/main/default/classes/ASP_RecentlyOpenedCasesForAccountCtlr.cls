/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
global with sharing class ASP_RecentlyOpenedCasesForAccountCtlr {

  private static final String CLASS_NAME = 'ASP_RecentlyOpenedCasesForAccountCtlr';

  @AuraEnabled
  @RemoteAction
  global static Integer getNumberOfOpenCasesForCaseAccount(String caseId) {
    AU_Debugger.enterFunction(CLASS_NAME + '.getNumberOfOpenCasesForAssociatedAccount');
    system.debug(caseId);
    List<Case> openCaseResult = getOpenCasesForCaseAccount(caseId);

    AU_Debugger.leaveFunction();
    return openCaseResult.size();
  }

  @AuraEnabled
  @RemoteAction
  global static List<Case> getOpenCasesForCaseAccount(String caseId) {
    AU_Debugger.enterFunction(CLASS_NAME + '.getOpenCasesForCaseAccount');

    String accountId = getAssociatedAccount(caseId);
    system.debug(caseId);
    List<Case> openCases = getOpenCasesForAssociatedAccount(caseId, accountId);

    if (openCases == null) {
      return new List<Case>();
    }

    AU_Debugger.leaveFunction();
    return openCases;
  }

  private static String getAssociatedAccount(String caseId) {
    AU_Debugger.enterFunction(CLASS_NAME + '.getAssociatedAccount');
    Case c = new Case();
    system.debug(caseId);
    if(!String.isBlank(caseId)) {
      c = [SELECT AccountId FROM Case WHERE Id = :caseId];
      if (c != null) {
        AU_Debugger.debug('Case with ID "' + caseId + '" does not have an associated account');

        AU_Debugger.leaveFunction();
      }

      AU_Debugger.leaveFunction();
      return c.AccountId;
    }
    return null;

  }

  private static List<Case> getOpenCasesForAssociatedAccount(String caseId, String accountId) {
    AU_Debugger.enterFunction(CLASS_NAME + '.getOpenCasesForAssociatedAccount');

    if (accountId == null) {
      AU_Debugger.debug('Account ID is NULL'); AU_Debugger.leaveFunction();
      return null;
    }

    Date sevenDaysAgo = Date.today().addDays(-7);

    List<Case> result = [SELECT CaseNumber, Subject, CreatedDate, AccountId FROM Case WHERE Id != :caseId AND AccountId = :accountId AND IsClosed = false AND CreatedDate > :sevenDaysAgo];

    AU_Debugger.leaveFunction();
    return result;
  }

  global class OpenCasesForCaseAccountResult {
    public Integer Count { get; set; }
    public String AccountId { get; set; }

    public OpenCasesForCaseAccountResult(String accountId, Integer count) {
      this.accountId = accountId;
      this.count = count;
    }
  }
}
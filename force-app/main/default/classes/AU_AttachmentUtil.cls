/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 20-Jul-18.
 */

public class AU_AttachmentUtil {

  public static Id save(Id parentId, String fileName, String base64Data, String contentType, String fileId) {

    if (fileId == '') {
      fileId = saveFile(parentId, fileName, base64Data, contentType);
    } else {
      appendToFile(fileId, base64Data);
    }

    return Id.valueOf(fileId);
  }

  private static Id saveFile(Id parentId, String fileName, String base64Data, String contentType) {
    base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

    Attachment file = new Attachment();
    file.parentId = parentId;
    file.Body = EncodingUtil.base64Decode(base64Data);
    file.Name = fileName;
    file.ContentType = contentType;

    insert file;
    return file.Id;
  }

  private static void appendToFile(Id fileId, String base64Data) {
    base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

    Attachment file = [
        SELECT Id, Body
        FROM Attachment
        WHERE Id =: fileId
    ];

    String existingBody = EncodingUtil.base64Encode(file.Body);

    file.Body = EncodingUtil.base64Decode(existingBody + base64Data);

    update file;
  }
}
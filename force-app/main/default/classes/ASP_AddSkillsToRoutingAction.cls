/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Invocable action for creating skill requirements on omni routing items.
 * Created by jliu on 6/19/2018.
 */

global without sharing class ASP_AddSkillsToRoutingAction {

  @InvocableMethod(label='Add Skill Requirements to Routing Items' description='Add Skill Requirements to existing Routing Items.')
  global static void addSkillRequirements(List<Id> pendingServiceRoutingIds) {
    try {
      AU_Debugger.enterFunction('ASP_AddSkillToRoutingAction.addSkillRequirements');

      List<PendingServiceRouting> pendingRoutingItems = [
        SELECT  WorkItemId
        FROM    PendingServiceRouting
        WHERE   Id IN :pendingServiceRoutingIds
            AND IsReadyForRouting = false
      ];

      List<SkillRequirement> skillRequirements = ASP_GlobalResolver.resolveSkills(pendingRoutingItems);
      insert skillRequirements;
    }
    catch (Exception e) {
      AU_Debugger.reportException('ASP_AddSkillToRoutingAction.addSkillRequirements', e);
    }
    finally {
      AU_Debugger.leaveFunction();
    }
  }
}
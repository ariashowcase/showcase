/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@isTest
public with sharing class ATU_Opportunity {

  public static ATU_Opportunity.Builder build(Id accountId) {
    return new ATU_Opportunity.Builder(accountId);
  }

  // FIXME: This function belongs into a dedicated Builder class that should specialize on the three objects involved to create an OpportunityLineItem.
  // It's ok if those are managed in one class. 
  public static OpportunityLineItem addOpportunityProduct(Id oppId) {
    Product2 prod = new Product2(
      Name = 'Test Product'
    );
    insert prod;

    PricebookEntry pbe = new PricebookEntry(
      IsActive = true,
      Pricebook2Id = Test.getStandardPricebookId(),
      Product2Id = prod.Id,
      UnitPrice = 1
    );
    insert pbe;

    OpportunityLineItem oli = new OpportunityLineItem(
      OpportunityId = oppId,
      PricebookEntryId = pbe.Id,
      Quantity = 1,
      TotalPrice = 1
    );
    insert oli;

    return oli;
  }

  public class Builder {

    private final Opportunity theRecord;

    private Builder(Id accountId) {
      theRecord = new Opportunity(
        Name = 'Test Opp',
        AccountId = accountId,
        StageName = 'Qualification',
        CloseDate = system.today()
      );
    }

    public ATU_Opportunity.Builder withName(String name) {
      theRecord.Name = name;
      return this;
    }

    public ATU_Opportunity.Builder withOwnerId(Id owner) {
      theRecord.OwnerId = owner;
      return this;
    }

    public ATU_Opportunity.Builder withRecordType(String recordType) {
      // The RecordTypeId field will not exist until a record type is defined. 
      theRecord.put('RecordTypeId', AU_RecordTypes.getId('Opportunity', recordType));
      return this;
    }

    public ATU_Opportunity.Builder withStageName(String stage) {
      theRecord.StageName = stage;
      return this;
    }

    public ATU_Opportunity.Builder withField(String fieldName, Object value) {
      theRecord.put(fieldName, value);
      return this;
    }

    public Opportunity create() {
      return theRecord;
    }

    public Opportunity persist() {
      insert theRecord;
      return theRecord;
    }

    public Opportunity registerNew(AU_UnitOfWork uow) {
      uow.registerNew(theRecord);
      return theRecord;
    }

    public Opportunity registerNew(AU_UnitOfWork uow, Schema.sObjectField relatedToParentField, SObject relatedToParentRecord) {
      uow.registerNew(theRecord, relatedToParentField, relatedToParentRecord);
      return theRecord;
    }

    public Opportunity monitorNew(AU_UnitOfWork uow) {
      uow.monitorNew(theRecord);
      return theRecord;
    }

    public Opportunity monitorNew(AU_UnitOfWork uow, Schema.sObjectField relatedToParentField, SObject relatedToParentRecord) {
      uow.monitorNew(theRecord, relatedToParentField, relatedToParentRecord);
      return theRecord;
    }
  }
}
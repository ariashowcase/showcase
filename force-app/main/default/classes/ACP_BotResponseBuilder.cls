/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */

public class ACP_BotResponseBuilder implements ASP_DelayedRequestExecution {

  private static final String CLASS_NAME = 'ACP_BotResponseBuilder.';

  private String apiKey;
  private String serviceUrl;
  private BotResponseBody body = new BotResponseBody();

  public ACP_BotResponseBuilder(String serviceUrl, String apiKey) {
    this.serviceUrl = serviceUrl;
    this.apiKey = apiKey;
  }

  public ACP_BotResponseBuilder withStatus(Integer status) {
    this.body.status = status;
    return this;
  }

  public ACP_BotResponseBuilder withMessage(String message) {
    this.body.message = message;
    return this;
  }

  public ACP_BotResponseBuilder withSession(Map<String, Object> sessionEntries) {
    this.body.session.putAll(sessionEntries);
    return this;
  }

  public ACP_BotResponseBuilder withSessionEntry(String key, Object value) {
    this.body.session.put(key, value);
    return this;
  }

  public HttpResponse send() {
    AU_Debugger.enterFunction(CLASS_NAME + 'send');
    HttpRequest botRequest = new HttpRequest();
    botRequest.setEndpoint(this.serviceUrl);
    botRequest.setMethod('POST');
    botRequest.setHeader('X-Api-Key', this.apiKey);
    botRequest.setHeader('Content-Type', 'application/json');
    botRequest.setBody(JSON.serialize(this.body));

    AU_Debugger.debug(
      String.format(
        'Sending {0} request to {1} with body: {2}',
        new String[] { botRequest.getMethod(), botRequest.getEndpoint(), botRequest.getBody() }
      )
    );

    Http connection = new Http();

    HttpResponse result = connection.send(botRequest);
    AU_Debugger.leaveFunction();
    return result;
  }

  public class BotResponseBody {
    public Integer status = 0;
    public String message = '';
    public Map<String, Object> session = new Map<String, Object>();
  }
}
@RestResource(urlMapping='/Flights/*')
global with sharing class SC_FlightRestResource {
  @HttpGet 
  global static List<SC_FlightService.FlightGroup> doGet() {
    String origin = RestContext.request.params.get('origin');
    String destination = RestContext.request.params.get('destination');
    String startTime = RestContext.request.params.get('startTime');
    String endTime = RestContext.request.params.get('endTime');

    return SC_FlightService.getFlightGroups(origin, destination, Datetime.valueOf(startTime), Datetime.valueOf(endTime));
  }
}

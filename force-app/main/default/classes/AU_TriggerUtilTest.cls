/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */

@IsTest
public class AU_TriggerUtilTest {

  @IsTest
  public static void testFilterByValueChanged() {
    TestHandler handler = new TestHandler(AU_TriggerUtil.filter(null).byValueChanged('Name'));

    List<Account> accounts = new List<Account> {
      ATU_Account.build('Account A').withField('AccountNumber', 'abc').create(),
      ATU_Account.build('Account B').withField('AccountNumber', 'efg').create()
    };

    insert accounts; //Insert to have Salesforce set an ID
    handler.invokeMain(createEventArgs(TriggerOperation.AFTER_INSERT, null, accounts), 1);

    validateNewObjects(handler, 'Name', new String[] {'Account A', 'Account B'});
    validateNewObjects(handler, 'AccountNumber', new String[] {'abc', 'efg'});

    List<Account> updateAcc = cloneList(accounts);
    updateAcc.get(0).Name = 'Account C';

    handler.invokeMain(createEventArgs(TriggerOperation.AFTER_UPDATE, accounts, updateAcc), 1);

    validateNewObjects(handler, 'Name', new String[] {'Account C'});
    validateNewObjects(handler, 'AccountNumber', new String[] {'abc'});
  }

  @IsTest
  public static void testFilterByValuesChanged() {
    TestHandler handler = new TestHandler(AU_TriggerUtil.filter(null).byValuesChanged(new String[] {'Name', 'AccountNumber'}));

    List<Account> accounts = new List<Account> {
      ATU_Account.build('Account A').withField('AccountNumber', 'abc').create(),
      ATU_Account.build('Account B').withField('AccountNumber', 'efg').create()
    };
    insert accounts; //Insert to have Salesforce set an ID
    handler.invokeMain(createEventArgs(TriggerOperation.AFTER_INSERT, null, accounts), 1);

    validateNewObjects(handler, 'Name', new String[] {'Account A', 'Account B'});
    validateNewObjects(handler, 'AccountNumber', new String[] {'abc', 'efg'});

    List<Account> updateAcc = cloneList(accounts);
    updateAcc.get(1).AccountNumber = 'zzz';

    handler.invokeMain(createEventArgs(TriggerOperation.AFTER_UPDATE, accounts, updateAcc), 1);

    System.assertEquals(0, handler.filteredArgs.newObjects.size());
  }

  @IsTest
  public static void testFilterByNewValueEquals() {
    TestHandler handler = new TestHandler(AU_TriggerUtil.filter(null).byNewValueEquals('Name', 'Account A'));

    List<Account> accounts = new List<Account> {
      ATU_Account.build('Account A').withField('AccountNumber', 'abc').create(),
      ATU_Account.build('Account B').withField('AccountNumber', 'efg').create()
    };
    insert accounts; //Insert to have Salesforce set an ID
    handler.invokeMain(createEventArgs(TriggerOperation.AFTER_INSERT, null, accounts), 1);

    validateNewObjects(handler, 'Name', new String[] {'Account A'});
    validateNewObjects(handler, 'AccountNumber', new String[] {'abc'});

    List<Account> updateAcc = cloneList(accounts);
    updateAcc.get(1).Name = 'Account A';

    handler.invokeMain(createEventArgs(TriggerOperation.AFTER_UPDATE, accounts, updateAcc), 1);

    validateNewObjects(handler, 'Name', new String[] {'Account A', 'Account A'});
    validateNewObjects(handler, 'AccountNumber', new String[] {'abc', 'efg'});
  }

  @IsTest
  public static void testMultipleFilters() {
    TestHandler handler = new TestHandler(AU_TriggerUtil.filter(null).byNewValueEquals('Name', 'Account A').byValueChanged('AccountNumber'));

    List<Account> accounts = new List<Account> {
      ATU_Account.build('Account A').withField('AccountNumber', 'aaa').create(),
      ATU_Account.build('Account B').withField('AccountNumber', 'bbb').create(),
      ATU_Account.build('Account C').withField('AccountNumber', 'ccc').create()
    };

    insert accounts; //Insert to have Salesforce set an ID
    handler.invokeMain(createEventArgs(TriggerOperation.AFTER_INSERT, null, accounts), 1);

    validateNewObjects(handler, 'Name', new String[] {'Account A'});
    validateNewObjects(handler, 'AccountNumber', new String[] {'aaa'});

    List<Account> updateAcc = cloneList(accounts);

    updateAcc.get(1).Name = 'Account A';
    updateAcc.get(2).Name = 'Account A';
    updateAcc.get(2).AccountNumber = 'ddd';

    handler.invokeMain(createEventArgs(TriggerOperation.AFTER_UPDATE, accounts, updateAcc), 1);

    validateNewObjects(handler, 'Name', new String[] {'Account A'});
    validateNewObjects(handler, 'AccountNumber', new String[] {'ddd'});
  }

  private static AU_TriggerEntry.Args createEventArgs(TriggerOperation triggerOp, List<Account> oldAccounts, List<Account> newAccounts) {
      AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      Account.getSObjectType(),
      triggerOp,
      true,
      newAccounts,
      oldAccounts,
      new Map<Id, Account>(newAccounts),
      oldAccounts == null ? null : new Map<Id, Account>(oldAccounts)
    );

    return args;
  }

  private static void validateNewObjects(TestHandler handler, String fieldName, String[] values) {
    AU_TriggerEntry.Args args = handler.filteredArgs;
    for (Integer i = 0; i < args.newObjects.size(); i++) {
      System.assertEquals(values.get(i), args.newObjects.get(i).get(fieldName));
    }
  }

  private static List<Account> cloneList(List<Account> orgList) {
    List<Account> result = new List<Account>();
    for (Account a : orgList) {
      result.add(a.clone(true, false, false, false));
    }
    return result;
  }

  public class TestHandler implements AU_TriggerEntry.ITriggerEntry {

    public AU_TriggerEntry.Args filteredArgs { get; set; }

    private final AU_TriggerUtil.FilterChain chain;

    public TestHandler(AU_TriggerUtil.FilterChain chain) {
      this.chain = chain;
    }

    public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount) {
      chain.args = args;
      filteredArgs = chain.run();
    }

    public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {}

    public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {}

  }
}
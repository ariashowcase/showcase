/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */

public class ACP_BotRequestBuilder implements ASP_DelayedRequestExecution {

  private static String CLASS_NAME = 'ACP_BotRequestBuilder.';

  private String apiKey;
  private String serviceUrl;
  private BotRequestBody body = new BotRequestBody();

  public ACP_BotRequestBuilder(String serviceUrl, String apiKey) {
    this.serviceUrl = serviceUrl;
    this.apiKey = apiKey;
  }

  public ACP_BotRequestBuilder usingChannel(String channel) {
    this.body.channel = channel;
    return this;
  }

  public ACP_BotRequestBuilder withMessage(String message) {
    this.body.message = message;
    return this;
  }

  public ACP_BotRequestBuilder withSession(Map<String, Object> sessionEntries) {
    this.body.session.putAll(sessionEntries);
    return this;
  }

  public ACP_BotRequestBuilder withSessionEntry(String key, Object value) {
    this.body.session.put(key, value);
    return this;
  }

  public HttpResponse send() {
    AU_Debugger.enterFunction(CLASS_NAME + 'send');
    HttpRequest botRequest = new HttpRequest();
    botRequest.setEndpoint(this.serviceUrl);
    botRequest.setMethod('POST');
    botRequest.setHeader('X-Api-Key', this.apiKey);
    botRequest.setHeader('Content-Type', 'application/json');
    botRequest.setBody(JSON.serialize(this.body));

    AU_Debugger.debug(
      String.format(
        'Sending {0} request to {1} with body: {2}',
        new String[] { botRequest.getMethod(), botRequest.getEndpoint(), botRequest.getBody() }
      )
    );

    Http connection = new Http();

    HttpResponse result = connection.send(botRequest);
    AU_Debugger.leaveFunction();
    return result;
  }

  public class BotRequestBody {
    public String channel = '';
    public String message = '';
    public Map<String, Object> session = new Map<String, Object>();
  }
}
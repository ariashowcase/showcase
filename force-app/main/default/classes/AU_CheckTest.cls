/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@isTest
private class AU_CheckTest {

  @isTest static void testNotEmpty_Success() {
    System.assertEquals('foo', AU_Check.NotEmpty('foo', 'myName'));
    System.assertEquals('bar', AU_Check.NotEmpty('bar', 'myName'));
  }

  @isTest static void testNotEmpty_Null() {
    try {
      AU_Check.NotEmpty(null, 'myName');
    } catch (AU_Check.ArgumentException ex) {
      System.assertEquals('\'myName\' is null', ex.getMessage());
    }
  }

  @isTest static void testNotEmpty_Empty() {
    try {
      AU_Check.NotEmpty('', 'myName');
    } catch (AU_Check.ArgumentException ex) {
      System.assertEquals('\'myName\' is empty', ex.getMessage());
    }
  }

  @isTest static void testNotNull_Success() {
    System.assertEquals('', AU_Check.NotNull('', 'myName'));
    System.assertEquals('foo', AU_Check.NotNull('foo', 'myName'));
    System.assertEquals('bar', AU_Check.NotNull('bar', 'myName'));
  }

  @isTest static void testNotNull_Null() {
    try {
      AU_Check.NotNull(null, 'myName');
    } catch (AU_Check.ArgumentException ex) {
      System.assertEquals('\'myName\' is null', ex.getMessage());
    }
  }

  @isTest static void testIsNullOrEmpty() {
    System.assertEquals(true, AU_Check.isNullOrEmpty((String) null));
    System.assertEquals(true, AU_Check.isNullOrEmpty(''));

    System.assertEquals(false, AU_Check.isNullOrEmpty('foo'));
    System.assertEquals(false, AU_Check.isNullOrEmpty('bar'));
  }

  @isTest static void testIsNull() {
    System.assertEquals(true, AU_Check.isNull(null));

    System.assertEquals(false, AU_Check.isNull(''));
    System.assertEquals(false, AU_Check.isNullOrEmpty('foo'));
    System.assertEquals(false, AU_Check.isNullOrEmpty('bar'));
  }

  @isTest static void testAssert() {
    try {
      AU_Check.assert(true, 'assert true');
      AU_Check.assert(false, 'assert false');
      System.assert(false);
    }
    catch (AU_Check.AssertException ex) {
      System.assertEquals('assert false', ex.getMessage());
    }
  }
}
/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
public class AU_UnitOfWork extends fflib_SObjectUnitOfWork {

  private Map<SObjectType, List<SObject>> monitorRecords = new Map<SObjectType, List<SObject>>();

  public AU_UnitOfWork(List<Schema.SObjectType> sObjectTypes) {
    super(sObjectTypes,new SimpleDML());
  }

  public AU_UnitOfWork(List<Schema.SObjectType> sObjectTypes, IDML dml) {
    super(sObjectTypes, dml);
  }

  public void monitorNew(sObject record) {
    registerNew(record);
    getOrCreateMonitoredList(record.getSObjectType()).add(record);
  }

  public void monitorNew(SObject record, Schema.sObjectField relatedToParentField, SObject relatedToParentRecord) {
    registerNew(record, relatedToParentField, relatedToParentRecord);
    getOrCreateMonitoredList(record.getSObjectType()).add(record);
  }

  public void monitorDirty(sObject record) {
    registerDirty(record);
    getOrCreateMonitoredList(record.getSObjectType()).add(record);
  }

  public void monitorDirty(SObject record, Schema.sObjectField relatedToParentField, SObject relatedToParentRecord) {
    registerDirty(record, relatedToParentField, relatedToParentRecord);
    getOrCreateMonitoredList(record.getSObjectType()).add(record);
  }

  public List<SObject> getMonitoredRecords(SObjectType objType) {
    List<SObject> monitoredRecords = monitorRecords.get(objType);
    return monitoredRecords == null
      ? new List<SOBject>()
      : monitoredRecords.clone();
  }

  private List<SObject> getOrCreateMonitoredList(SObjectType objType) {
    if (monitorRecords.containsKey(objType)) {
      return (List<SObject>) monitorRecords.get(objType);
    }

    List<SObject> newList = new List<SObject>();
    monitorRecords.put(objType, newList);

    return newList;
  }
}
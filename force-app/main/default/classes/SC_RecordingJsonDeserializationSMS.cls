/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Wednesday November 6th 2019
 * Author: varonov
 * File type: '.cls'
 */


public with sharing class SC_RecordingJsonDeserializationSMS {
	
	public String id;
	public String conversationId;
	public String media;
	public List<Annotations> annotations;
	public List<MessagingTranscript> messagingTranscript;
	
	public class MessagingTranscript {
		public String from_N;
		public FromUser fromUser;
		public String to;
		public String timestamp;
		public String id;
		public String messageText;
	}

  public class Annotations {}

	public class FromUser	{
		public String id;
		public String name;
		public String username;
	}

  public static List<SC_RecordingJsonDeserializationSMS> parse(String json) {
		return (List<SC_RecordingJsonDeserializationSMS>) System.JSON.deserialize(json.replace('"from":', '"from_N":'), List<SC_RecordingJsonDeserializationSMS>.class);
	}
}
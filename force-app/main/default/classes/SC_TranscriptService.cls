/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Wednesday November 6th 2019
 * Author: varonov
 * File type: '.cls'
 */


public with sharing class SC_TranscriptService implements Queueable {

  private List<RelatedObjects> taskRelatedObjects {get; set;}

  public SC_TranscriptService(List<RelatedObjects> objects) {
    taskRelatedObjects = objects;
  }

  public void execute(QueueableContext context) {
    for (RelatedObjects obj : taskRelatedObjects)  {
      getTranscript(obj.conversationId, obj.taskId, obj.whatId, obj.interactionSegmentId);
    }   
  }

  @future (callout=true)
  public static void getTranscript(String conversationId, Id taskId, Id whatId, Id interactionSegmentId)  {
    TokenDetails token = getToken(conversationId);
    String transcriptBody = getRecordingDetails(token, conversationId, taskId);
    updateInteractionSegment(interactionSegmentId, transcriptBody);
  }

  private static TokenDetails getToken(String conversationId)  {
    Map<String, String> interactionSettings = buildMapOfPureCloudInteraction();
    TokenDetails token = new TokenDetails();
    String clientId;
    String clientSecret;
    for (String id : interactionSettings.keySet())  {
      clientId = id;
      clientSecret = interactionSettings.get(id);
    }
    if (clientId != null && clientSecret != null) {
      HttpRequest request = new HttpRequest();
      request.setMethod('POST');
      request.setHeader('Authorization', 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(clientId + ':' + clientSecret)));
      request.setHeader('Host','https://login.mypurecloud.com/oauth');
      request.setHeader('content-type', 'application/x-www-form-urlencoded');
      request.setEndpoint('https://login.mypurecloud.com/oauth/token');
      request.setBody('grant_type=client_credentials');

      Http http = new Http();
      try {
        HTTPResponse response = http.send(request);
        if (response.getStatusCode() != 200) {
          AU_Debugger.debug('SC_TranscriptService: The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
        } else {
          Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
          token.accessToken = (String) results.get('access_token');
          token.tokenType = (String) results.get('token_type');
        }
      } catch(System.CalloutException e){
        AU_Debugger.debug('SC_TranscriptService: Response error: ' + e.getMessage());
      }
    }
   
    return token;
  }

  private static Map<String, String> buildMapOfPureCloudInteraction() {
    Map<String, String> purecloudInteraction = new Map<String, String>();
    for(PureCloud_Interaction_Setting__mdt setting : [SELECT ClientId__c, Client_Secret__c FROM PureCloud_Interaction_Setting__mdt])	{
      purecloudInteraction.put(setting.ClientId__c, setting.Client_Secret__c);
    }
    return purecloudInteraction;
  }

  private static String getRecordingDetails(TokenDetails token, String conversationId, Id taskId)  {
    System.debug('token: ' + token);
    System.debug('conversationId: ' + conversationId);
    System.debug('taskId: ' + taskId);
    String htmlBody = '';
    String fromUser = '';
    try {
      {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://api.mypurecloud.com/api/v2/conversations/' + conversationId + '/recordings');
        request.setMethod('GET');
        request.setHeader('Authorization', token.tokenType + ' ' + token.accessToken);

        HttpResponse response = http.send(request);
        System.debug('response: ' + response.getBody());
        System.debug('status: ' + response.getStatusCode());
        Integer attempt = 0;
        if (response.getStatusCode() != 200) {
          while (response.getStatusCode() != 200 || attempt < 1000) {
            response = http.send(request);
            attempt++;
          }                  
        }
        List<Object> responseList = (List<Object>) JSON.deserializeUntyped(response.getBody());
        Map<String,Object> dataMap = (Map <String,Object>) responseList[0];
        String mediaType = (String)dataMap.get('media');

        switch on mediaType {
          when 'message' {
            List<SC_RecordingJsonDeserializationSMS> recording = SC_RecordingJsonDeserializationSMS.parse(response.getBody());              
            for (SC_RecordingJsonDeserializationSMS.MessagingTranscript t : recording[0].messagingTranscript)  {
              fromUser = t.fromUser == null ? t.from_N : t.fromUser.name;
              htmlBody += '<p><strong>' + fromUser + '</strong> ('+ Datetime.valueOf(t.timestamp.replace('T',' ')).format() +'): <br />' + t.messageText + '</p><br />';
            } 
          }
          when 'audio' {
            List<SC_RecordingJsonDeserializationCall> recording = SC_RecordingJsonDeserializationCall.parse(response.getBody());  
            htmlBody = recording[0].MediaUris.mediaData.mediaUri;
          }
          when 'chat' {
            htmlBody = getChatTranscript(taskId).replaceAll('\\\n','<br />');
            getContactAndAccount(taskId);
          }
        }
      }
    } catch (Exception ex) {
      AU_Debugger.reportException('SC_TranscriptService: Callout problem: ', ex);
    }

    return htmlBody;
  }

  private static void updateInteractionSegment(Id segmentId, String transcriptBody)  { 
    if (transcriptBody.startsWith('http'))  {
      update new ASP_Interaction_Segment__c(Id = segmentId, Call_Recording__c = transcriptBody);
    }
    else {
      {
        update new ASP_Interaction_Segment__c(Id = segmentId, Transcript__c = transcriptBody);
      }
    }    
  }

  private static String getChatTranscript(Id taskId)  {
    return String.valueOf([SELECT PureCloud_Chat_Transcript__r.purecloud__Body__c FROM Task WHERE Id = :taskId].PureCloud_Chat_Transcript__r.purecloud__Body__c);
  }

  private static void getContactAndAccount(Id taskId)  {
    String contactName = String.valueOf([SELECT Interaction_Name__c FROM Task WHERE Id = :taskId].Interaction_Name__c);
    List<Contact> contactMap = new List<Contact>([SELECT Id, AccountId FROM Contact WHERE Name = :contactName]);
    if (contactMap.size() == 1) {
      update new Task (
        Id = taskId,
        whoId = contactMap[0].Id,
        whatId = contactMap[0].AccountId
      );
    }
  }

  private class TokenDetails {
    private String tokenType {get; set;}
    private String accessToken {get; set;}

    private TokenDetails() {}
  }

  public class RelatedObjects {
    public String conversationId {get; set;}
    public Id taskId {get; set;}
    public Id whatId {get; set;}
    public Id interactionSegmentId {get; set;}

    public RelatedObjects() {}
    public RelatedObjects(String conversationId, Id taskId, Id whatId, Id interactionSegmentId) {
      this.conversationId = conversationId;
      this.taskId = taskId;
      this.whatId = whatId;
      this.interactionSegmentId = interactionSegmentId;
    }
  }
}
/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 27-Mar-18.
 *
 */

public class ASP_MarqueeController {
  @AuraEnabled
  public static List<ASP_Marquee__c> getRecords() {
    String userGroup = Site.getSiteId() != null ? 'Community' : 'Internal';

    return [
      SELECT  Title__c, User_Group__c, Start_Date__c, End_Date__c, Message_Body__c, Active__c, Show_Posted_Date__c
      FROM    ASP_Marquee__c
      WHERE   Active__c = true AND
              Start_Date__c <= TODAY AND
              (End_Date__c = null OR End_Date__c >= TODAY) AND
              User_Group__c INCLUDES (:userGroup)
      ORDER BY Start_Date__c DESC
    ];
  }
}
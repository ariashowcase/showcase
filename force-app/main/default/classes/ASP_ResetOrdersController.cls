/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 23-May-19. 
 */

public with sharing class ASP_ResetOrdersController {

  public Id accountId { get; private set; }

  public ASP_ResetOrdersController(ApexPages.StandardController controller)    {
    accountId = controller.getId();
  }

  public void ResetData() {
    ResetOrders();
    ResetCases();
  }

  public void ResetOrders() {
    List<Order> orders = [SELECT Id, Status FROM Order WHERE AccountId = :accountId AND Callback_Use_Case__c = false];
    for(Order o : orders) {
      o.Status = 'Confirmed';
    }
    update orders;
  }

  public void ResetCases()  {
    List<Case> cases = [SELECT Id, Status, Mode_of_Payment__c FROM Case WHERE AccountId = :accountId AND Order__r.Callback_Use_Case__c = true];
    for(Case c : cases) {
      c.Status = 'Closed - Credited';
      c.Mode_of_Payment__c = 'Credit';
    }
    update cases;
  }
}
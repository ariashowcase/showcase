/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 bigass.force.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
//class will be used in two areas
// 1: maps JSON props to class props
// 2: remove null values from serialized strings
//
//NOTE: For mapping it is VERY important that the new class prop
// does not exsist anywhere else in the JSON
public abstract class AU_JsonImprovedConverter {

  private static final String CLASS_NAME = 'AU_JsonImprovedConverter';

  private Map<String,String> mapKeys;

  private boolean serializeNulls = true;

  /* SETUP METHODS */
  //constructor
  protected AU_JsonImprovedConverter(Map<String,String> mapping){
    this.mapKeys = mapping;
  }

  //use to set how nulls are serialized.
  // Set false to remove nulls from string.
  public void setSerializeNulls(boolean b){
    AU_Debugger.enterFunction(CLASS_NAME + '.setSerializeNulls');
    this.serializeNulls = b;
  }

  public String serialize(Object obj){
    AU_Debugger.enterFunction(CLASS_NAME + '.serialize');

    String retString = JSON.serialize(obj);
    retString = transformStringForSerilization(retString);
    if(!serializeNulls){
      retString = removeNullsFromJSON(retString);
    }

    try{
      return retString;
    } finally {
      AU_Debugger.leaveFunction();
    }
  }

  public Object deserialize(String jsonString, System.Type type){
    AU_Debugger.enterFunction(CLASS_NAME + '.deserialize');
    jsonString = transformStringForDeserilization(jsonString);
    try {
      return JSON.deserialize(jsonString, type);
    } finally {
      AU_Debugger.leaveFunction();
    }
  }

  private String transformStringForSerilization(String s){
    return replaceAll(s, mapKeys);
  }

  private String transformStringForDeserilization(String s){
    Map<String,String> flippedMap = new Map<String,String>();
    for(String key : mapKeys.keySet()){
      flippedMap.put(mapKeys.get(key), key);
    }
    return replaceAll(s, flippedMap);
  }

  //this is questionable!
  //check back @ http://stackoverflow.com/questions/31911654/strip-all-nulls-from-json-string-with-regex
  private String removeNullsFromJSON(String s){
    return s.replaceAll('("[\\w]*":null,)|(,?"[\\w]*":null)','');
  }

  private String replaceAll(String s, Map<String,String> toFromMap){
    for(String key : toFromMap.keySet()){
      s = s.replaceAll('"'+key+'":', '"'+toFromMap.get(key)+'":');
    }
    return s;
  }
}
/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

public with sharing class ASP_UpdateSentimentQueueable implements Queueable, Database.AllowsCallouts {

  private static final String CLASS_NAME = 'ASP_UpdateSentimentQueueable';

  private final String objType;
  private final String id;
  private final String text;
  private final String sentimentFieldName;
  private final String sentimentProbabilityFieldName;
  private final TokenProvider tokenProvider;

  public ASP_UpdateSentimentQueueable(String objType, String sentimentFieldName, String sentimentProbabilityFieldName, String id, String text) {
    this(objType, sentimentFieldName, sentimentProbabilityFieldName, id, text, EinsteinTokenProvider.forDefaultProfile());
  }

  public ASP_UpdateSentimentQueueable(String objType, String sentimentFieldName, String sentimentProbabilityFieldName, String id, String text, TokenProvider tokenProvider) {
    this.id = id;
    this.text = text;
    this.objType = objType;
    this.sentimentFieldName = sentimentFieldName;
    this.sentimentProbabilityFieldName = sentimentProbabilityFieldName;
    this.tokenProvider = tokenProvider;
  }

  public void execute(QueueableContext ctx) {
    AU_Debugger.enterFunction(CLASS_NAME + '.execute');
    try {
      EinsteinLanguageService service = new EinsteinLanguageService(tokenProvider);

      EinsteinLanguageService.SentimentResult result = service.getCommunitySentiment(text);

      //AU_Debugger.debug(String.format('Fields to updated: sentimentFieldName={0}, sentimentProbabilityFieldName{1}', new String[] { sentimentFieldName, String.valueOf(sentimentProbabilityFieldName)}));
      SObject obj = Schema.getGlobalDescribe().get(objType).newSObject();
      obj.Id = id;
      obj.put(sentimentFieldName, result.probabilities.get(0).label);

      if (String.isNotEmpty(sentimentProbabilityFieldName)) { obj.put(sentimentProbabilityFieldName, result.probabilities.get(0).probability); }

      update obj;
    } catch (Exception ex) {
      AU_Debugger.reportException(CLASS_NAME, ex);
    }finally {
      AU_Debugger.leaveFunction();
    }
  }
}
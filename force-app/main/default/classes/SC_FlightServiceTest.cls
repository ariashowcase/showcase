@isTest
private class SC_FlightServiceTest {
  @isTest
  static void testCreateGraph() {
    SC_FlightService.MIN_MINUTES_BETWEEN_FLIGHTS = 0;
    List<SC_FlightService.FlightNode> nodes = new List<SC_FlightService.FlightNode> {
      createNode('1', 'Calgary', 'Vancouver', Datetime.newInstance(50), Datetime.newInstance(100)),
      createNode('2', 'Calgary', 'Vancouver', Datetime.newInstance(150), Datetime.newInstance(200)),
      createNode('3', 'Vancouver', 'San Francisco', Datetime.newInstance(150), Datetime.newInstance(200)),
      createNode('4', 'Vancouver', 'San Francisco', Datetime.newInstance(250), Datetime.newInstance(300))
    };
    SC_FlightService.FlightGraph graph = new SC_FlightService.FlightGraph(nodes);
    system.assertEquals(2, graph.nodesByOrigin.size());
    system.assert(graph.nodesByOrigin.containsKey('Calgary'));
    system.assertEquals(2, graph.nodesByOrigin.get('Calgary').size());
    system.assert(graph.nodesByOrigin.containsKey('Vancouver'));
    system.assertEquals(2, graph.nodesByOrigin.get('Vancouver').size());

    system.assertEquals(2, graph.nodesByDestination.size());
    system.assert(graph.nodesByDestination.containsKey('Vancouver'));
    system.assertEquals(2, graph.nodesByDestination.get('Vancouver').size());
    system.assert(graph.nodesByDestination.containsKey('San Francisco'));
    system.assertEquals(2, graph.nodesByDestination.get('San Francisco').size());

    system.assertEquals(2, nodes[0].connectedNodes.size());
    system.assertEquals(1, nodes[1].connectedNodes.size());
    system.assertEquals(0, nodes[2].connectedNodes.size());
    system.assertEquals(0, nodes[3].connectedNodes.size());
    
    system.assert(nodes[0].connectedNodes[0] === nodes[2]);
    system.assert(nodes[0].connectedNodes[1] === nodes[3]);
    system.assert(nodes[1].connectedNodes[0] === nodes[3]);
  }

  @isTest
  static void testSearch1() {
    SC_FlightService.MIN_MINUTES_BETWEEN_FLIGHTS = 0;
    List<SC_FlightService.FlightNode> nodes = new List<SC_FlightService.FlightNode> {
      createNode('1', 'Calgary', 'Vancouver', Datetime.newInstance(50), Datetime.newInstance(100)),
      createNode('2', 'Calgary', 'Vancouver', Datetime.newInstance(150), Datetime.newInstance(200)),
      createNode('3', 'Vancouver', 'San Francisco', Datetime.newInstance(150), Datetime.newInstance(200)),
      createNode('4', 'Vancouver', 'San Francisco', Datetime.newInstance(250), Datetime.newInstance(300))
    };
    SC_FlightService.FlightGraph graph = new SC_FlightService.FlightGraph(nodes);

    List<SC_FlightService.FlightGroup> groups = graph.searchPaths('Calgary', 'San Francisco');

    List<String> paths = new List<String>();
    for (SC_FlightService.FlightGroup g : groups) {
      paths.add(g.toShortString());
    }

    system.assert(false, paths);
  }

  @isTest
  static void testSearch2() {
    SC_FlightService.MIN_MINUTES_BETWEEN_FLIGHTS = 0;
    List<SC_FlightService.FlightNode> nodes = new List<SC_FlightService.FlightNode> {
      createNode('1', 'A', 'B', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('2', 'A', 'C', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('3', 'A', 'E', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('4', 'B', 'D', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('5', 'B', 'F', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('6', 'E', 'F', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('7', 'B', 'D', Datetime.newInstance(0), Datetime.newInstance(0))
    };
    SC_FlightService.FlightGraph graph = new SC_FlightService.FlightGraph(nodes);

    List<SC_FlightService.FlightGroup> groups = graph.searchPaths('A', 'F');

    List<String> paths = new List<String>();
    for (SC_FlightService.FlightGroup g : groups) {
      paths.add(g.toShortString());
    }

    system.assert(false, paths);
  }

  @isTest
  static void testSearch3() {
    SC_FlightService.MIN_MINUTES_BETWEEN_FLIGHTS = 0;
    List<SC_FlightService.FlightNode> nodes = new List<SC_FlightService.FlightNode> {
      createNode('1', 'A', 'B', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('2', 'B', 'A', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('3', 'B', 'C', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('4', 'C', 'B', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('5', 'C', 'D', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('6', 'D', 'C', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('7', 'D', 'A', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('8', 'A', 'D', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('9', 'B', 'E', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('10', 'E', 'B', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('11', 'D', 'E', Datetime.newInstance(0), Datetime.newInstance(0)),
      createNode('12', 'E', 'D', Datetime.newInstance(0), Datetime.newInstance(0))
    };
    SC_FlightService.FlightGraph graph = new SC_FlightService.FlightGraph(nodes);

    List<SC_FlightService.FlightGroup> groups = graph.searchPaths('A', 'E');

    List<String> paths = new List<String>();
    for (SC_FlightService.FlightGroup g : groups) {
      paths.add(g.toShortString());
    }

    system.assert(false, paths);
  }

  static SC_FlightService.FlightNode createNode(
    String flightNo,
    String origin, 
    String destination, 
    Datetime departureTime, 
    Datetime arrivalTime
  ) {
    SC_FlightService.FlightWrapper wrapper = new SC_FlightService.FlightWrapper();
    wrapper.flightNumber = flightNo;
    wrapper.origin = origin;
    wrapper.destination = destination;
    wrapper.departureTime = departureTime;
    wrapper.arrivalTime = arrivalTime;
    wrapper.price = 0;
    SC_FlightService.FlightNode node = new SC_FLightService.FlightNode();
    node.flight = wrapper;
    return node;
  }
}

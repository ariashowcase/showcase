public class CaseInsurancesController {
    public final String insurances{public get; private set;}
    
    public CaseInsurancesController(ApexPages.StandardController stdController) {
        stdController.addFields(new List<String> {'Order__c'});
        Case c = (Case) stdController.getRecord();
        Set<String> insuranceNames = new Set<String>();
        
        for (OrderItem oi : [
            SELECT 	Product2.Name
            FROM 	OrderItem
            WHERE 	OrderId = :c.Order__c AND
            		Product2.Family = 'Travel Insurance'
        ]) {
            insuranceNames.add(oi.Product2.Name);
        }
        
        if (insuranceNames.isEmpty())
            insurances = 'none';
        else
            insurances = String.join(new List<String>(insuranceNames), ', ');
    }    
}
/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Friday August 23rd 2019
 * Author: varonov
 * File type: '.cls'
 */

@isTest
public class SC_BookTripControllerTest {
    
  private static Account testAccount {
  get {
    if(testAccount == null) {
    testAccount = [SELECT Id FROM Account LIMIT 1];
    }
    return testAccount;
  }
  set;
  }

  private static Contact testContact {
  get {
    if(testContact == null) {
    testContact = [SELECT Id, Name FROM Contact WHERE AccountId = :testAccount.Id LIMIT 1];
    }
    return testContact;
  }
  set;
  }

  @testSetup
  private static void setup() {
    testAccount = ATU_AccountFactory.createDefaultBuilder('testAccount').persist();
    testContact = ATU_ContactFactory.createDefaultBuilder('Jonson', testAccount.Id)
      .withFirstName('John')
      .withField('Email', 'john.doe.test@test.com')
      .persist();
  }

  static SC_BookTripController createBookTripController(Account acct)  {
    ApexPages.StandardController stdController = new ApexPages.StandardController(acct);
    return new SC_BookTripController(stdController);
  }

  @isTest
  private static void searchFlightsTest() {
    Id productId = createProduct();
    Id flightId = createFlight(productId);
    SC_BookTripController controller = createBookTripController(testAccount);
    controller.origin = 'Calgary';
    controller.destination = 'Vancouver';
    controller.flightDate = Date.newInstance(2019, 2, 17);
    List<Flight__c> flights = new List<Flight__c>();
    flights.add([
        SELECT Flight_Product__r.Origin__c, Flight_Product__r.Destination__c, Flight_Product__r.ProductCode,
            Flight_Date_Time__c, Arrival_Date_Time__c, Available_Seats__c, Flight_Status__c
        FROM Flight__c
        WHERE Id = :flightId
    ]);
    controller.groups = new List<SC_FlightService.FlightGroup> {
        new SC_FlightService.FlightGroup(
            flights,
            1480,
            1250.00)
    };

    Test.startTest();
    controller.searchFlights();
    Test.stopTest();
    System.assertEquals(1, controller.groups.size());
  }

  @isTest
  private static void addTripTest() {
    Id productId = createProduct();
    Id flightId = createFlight(productId);
    SC_BookTripController controller = createBookTripController(testAccount);
    List<Flight__c> flights = new List<Flight__c>();
    flights.add([
        SELECT Flight_Product__r.Origin__c, Flight_Product__r.Destination__c, Flight_Product__r.ProductCode,
        Flight_Date_Time__c, Arrival_Date_Time__c, Available_Seats__c, Flight_Status__c
        FROM Flight__c
        WHERE Id = :flightId
    ]);
    PageReference pageRef = Page.SC_BookTrip;
    pageRef.getParameters().put('hiddenGroupNumber', '1');
    Test.setCurrentPage(pageRef);

    controller.groups = new List<SC_FlightService.FlightGroup> {
        new SC_FlightService.FlightGroup(
            flights,
            1480,
            1250.00)
    };

    Test.startTest();
    controller.addTrip();
    Test.stopTest();

    System.assertEquals(1, controller.selectedFlightIds.size());
  }

  @isTest
  private static void createTripTest()  {
    Id productId = createProduct();
    Id flightId = createFlight(productId);
    SC_BookTripController controller = createBookTripController(testAccount);
    controller.contactId = testContact.Id;
    controller.selectedInsurances = new List<SelectOption>();
    controller.flightPackage = new List<SC_BookTripController.FlightPackage>{
        new SC_BookTripController.FlightPackage(
            flightId,
            null,
            controller.contactId,
            null,
            'Calgary',
            'Vancouver',
            date.newInstance(2019, 12, 17),
            'Economy',
            'Full'
        )
    };

    Test.startTest();
    controller.insurances = new List<SelectOption>();
    Schema.DescribeFieldResult insurancePicklist = Booked_Seat__c.Insurance__c.getDescribe();
    List<Schema.PicklistEntry> pleInsurance = insurancePicklist.getPicklistValues();
    for(Schema.PicklistEntry pickListVal : pleInsurance)  {
      controller.insurances.add(new SelectOption(pickListVal.getLabel(), pickListVal.getValue()));
    }
    controller.saveTrip();
    Test.stopTest();

    System.assertEquals(true, controller.tripBooked);
    System.assertEquals(false, controller.showUpgradePackage);
  }

  @isTest
  private static void getSelectedSeatTypesTest() {
    SC_BookTripController controller = createBookTripController(testAccount);
    List<SelectOption> lstSeatTypes = new List<SelectOption>();
    List<SelectOption> lstSeatTypesResults = new List<SelectOption>();
    lstSeatTypes.add(new SelectOption('Economy', 'Economy'));
    lstSeatTypes.add(new SelectOption('Business', 'Business'));
    lstSeatTypes.add(new SelectOption('First Class', 'First Class'));
    Test.startTest();
    lstSeatTypesResults = controller.getSelectedSeatTypes();
    Test.stopTest();
    System.assertEquals(lstSeatTypes, lstSeatTypesResults);
  }

  @isTest
  private static void getSelectedMealsTest() {
    SC_BookTripController controller = createBookTripController(testAccount);
    List<SelectOption> lstMeals = new List<SelectOption>();
    List<SelectOption> lstMealsResults = new List<SelectOption>();
    lstMeals.add(new SelectOption('Full', 'Full'));
    lstMeals.add(new SelectOption('Snack', 'Snack'));
    lstMeals.add(new SelectOption('Drink', 'Drink'));

    Test.startTest();
    lstMealsResults = controller.getSelectedMeals();
    Test.stopTest();

    System.assertEquals(lstMeals, lstMealsResults);
  }

  @isTest
  private static void reviewTripTest()  {
    Id productId = createProduct();
    Id flightId = createFlight(productId);
    SC_BookTripController controller = createBookTripController(testAccount);
    List<SelectOption> lstContacts = new List<SelectOption>();
    lstContacts.add(new SelectOption(testContact.Id, testContact.Name));
    controller.selectedContacts = lstContacts;

    Test.startTest();
    controller.flights = new List<Flight__c>();
    controller.flights.add([
        SELECT Id, Name, Origin__c, Destination__c, Flight_Date_Time__c
        FROM Flight__c
        WHERE Id = :flightId
    ]);
    controller.reviewTrip();
    Test.stopTest();

    System.assertEquals(1, controller.flightPackage.size());
    System.assertEquals(true, controller.showUpgradePackage);
    System.assertEquals(false, controller.tripAdded);
  }

  @isTest
  private static void completeBookingTest() {
    SC_BookTripController controller = createBookTripController(testAccount);
    controller.contactId = testContact.Id;
    Id productId = createProduct();
    Id flightId = createFlight(productId);
    List<Id> contactIds = new List<Id>{controller.contactId};

    controller.flightIdsPerTrip = new List<List<Id>>();
    controller.flightIdsPerTrip.add(new List<Id>{flightId});
    controller.bookingData = new SC_BookingSessionService.BookingData(
      testAccount.Id, contactIds, 'Calgary', 'Vancouver', date.newInstance(2019, 12, 17), controller.flightIdsPerTrip, null
    );
    String key = SC_BookingSessionService.storeData(controller.bookingData);
    Test.setCurrentPageReference(new PageReference('Page.SC_BookTrip'));
    System.currentPageReference().getParameters().put('sessionKey', key);

    Test.startTest();
    controller.completeBooking();
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(testAccount.Id, controller.accountId);
    System.assertEquals(contactIds, controller.contactIds);
  }

  @isTest
  private static void redirectToAccountTest()  {
    SC_BookTripController controller = createBookTripController(testAccount);
    PageReference pageRef = Page.SC_BookTrip;
    pageRef.getParameters().put('Id', testAccount.Id);
    Test.setCurrentPage(pageRef);

    Test.startTest();
    controller.redirectToAccount();
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  private static void propertiesCoverage()  {
    SC_BookTripController controller = createBookTripController(testAccount);
    controller.contactId = testContact.Id;
    controller.meals = new List<SelectOption>();
    controller.bookedSeats = new List<Booked_Seat__c>();
    controller.confirmationNumber = new List<String>();
  }

  // Helper methods

  private static Id createFlight(Id productId) {
    Flight__c testFlight =  new Flight__c(
        Flight_Date_Time__c = Datetime.newInstance(2019, 2, 17),
        Flight_Product__c = productId
    );
    insert testFlight;
    return testFlight.Id;
  }

  private static Id createProduct() {
    Product2 testProduct = new Product2(
        Name = 'AC111',
        Origin__c = 'Calgary',
        Destination__c = 'Vancouver',
        Capacity__c = 10
    );
    insert testProduct;
    return testProduct.Id;
  }

  private static Id createTrip() {
    Trip__c testTrip = new Trip__c(Account__c = testAccount.Id, Order__c = createOrder());
    insert testTrip;
    return testTrip.Id;
  }

  private static void createSeats(Id productId)  {
    insert new Booked_Seat__c(Trip__c = createTrip(), Flight__c = createFlight(productId));
  }

  private static Id createOrder() {
    Order testOrder = new Order(
        AccountId = testAccount.Id,
        Status = 'Confirmed',
        EffectiveDate = date.newInstance(2018, 12, 17)
    );
    insert testOrder;
    return testOrder.Id;
  }
}
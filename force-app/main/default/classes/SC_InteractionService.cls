/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Monday October 21st 2019
 * Author: varonov
 * File type: '.cls'
 */


global with sharing class SC_InteractionService {

  public static Id createTask(String conversationId, Datetime startTime, Id contactId, String direction, String conversationType, String phoneNumber) {
    String userId = UserInfo.getUserId();
    Id accountId;
    if (contactId != null)  {
      accountId = getRelatedAccountId(contactId);
    }

    Task t = new Task();
    t.OwnerId = userId;
    t.Subject = 'Bot ' + conversationType.toUpperCase() + ' ' + startTime.format();
    t.Status = 'In Progress';
    t.Priority = 'Normal';
    t.WhoId = contactId != null ? contactId : null;
    t.WhatId = accountId != null ? accountId : null;
    t.CallObject = conversationId; 
    t.CallType = direction;
    t.Interaction_Name__c = phoneNumber;
    t.Caller_Phone__c = phoneNumber;
    t.ActivityDate = date.newInstance(startTime.year(), startTime.month(), startTime.day());
    insert t;
    return t.Id;
  }

  public static Id updateTask(Id taskId, Datetime startTime, Datetime endTime, String queueName, String callResult, Integer afterWorkDuration) {

    Task t = new Task();
    t.Id = taskId;
    t.Status = 'Completed';
    t.CallDurationInSeconds = (endTime.getTime().intValue() - startTime.getTime().intValue())/1000;
    t.Queue__c = queueName;
    t.CallDisposition = callResult;
    t.After_Work_Duration__c = afterWorkDuration;
    update t;
    return t.Id;
  }

  private static Id getRelatedAccountId(Id contactId)  {
    return [SELECT AccountId FROM Contact WHERE Id = :contactId].AccountId;
  }

}
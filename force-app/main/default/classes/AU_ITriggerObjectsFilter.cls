/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

/*
 * Base interface for any AU_TriggerEntry.Args object filter.
 */
public interface AU_ITriggerObjectsFilter {

  AU_TriggerEntry.Args run(AU_TriggerEntry.Args args);
}
public with sharing class ActivityCallDataController {
    @AuraEnabled
    public static Id getInteractionSegmentIdFromTask(Id taskId) {
        List<Task> tasks = [
            SELECT 	CallObject
            from 	Task
            where 	Id = :taskId
        ];
        
        if (tasks.isEmpty() || tasks[0].CallObject == null)
            return null;
        
        List<String> tokens = tasks[0].CallObject.split(':');
        String acId = tokens[tokens.size() - 1];
        system.debug(tokens);
        system.debug(acId);
        
        List<ASP_Interaction_Segment__c> segments = [
            SELECT 	Id
            FROM 	ASP_Interaction_Segment__c
            WHERE 	Interaction_Segment_Id__c =: acId
        ];
        
        if (segments.size() != 1)
            return null;
        
        return segments[0].Id;
    }
}
/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@isTest
private class EinsteinLanguageServiceTest {

  @isTest static void testGetCommunitySentiment() {
    Test.setMock(HttpCalloutMock.class, new ATU_SimpleHttpCalloutMock(createExpectedRequest(), createMockResponse(), createRequestOptions()));

    EinsteinLanguageService service = new EinsteinLanguageService(new TokenProviderMock());

    EinsteinLanguageService.SentimentResult result = service.getCommunitySentiment('the presentation was great and I learned a lot');

    assertSentiment(result.probabilities.get(0), 'positive', 0.8673582);
    assertSentiment(result.probabilities.get(1), 'negative', 0.1316828);
    assertSentiment(result.probabilities.get(2), 'neutral', Decimal.valueOf('9.590242E-4'));
  }

  private static void assertSentiment(EinsteinLanguageService.Sentiment sentiment, String label, Decimal probability) {
    System.assertEquals(label, sentiment.label);
    System.assertEquals(probability, sentiment.probability);
  }

  private static HttpRequest createExpectedRequest() {
    HttpRequest req = new HttpRequest();
    req.setMethod('POST');
    req.setEndpoint('https://api.einstein.ai/v2/language/sentiment');
    req.setHeader('Content-type', HttpFormDataBodyPart.GetContentType());

    return req;
  }

  private static HttpResponse createMockResponse() {
    HttpResponse resp = new HttpResponse();
    resp.setStatusCode(200);
    resp.setBody('{"probabilities":[{"label":"positive","probability":0.8673582},{"label":"negative","probability":0.1316828},{"label":"neutral","probability":9.590242E-4}],"object":"predictresponse"}');

    return resp;
  }

  private static ATU_SimpleHttpCalloutMock.Options createRequestOptions() {
    ATU_SimpleHttpCalloutMock.Options options = new ATU_SimpleHttpCalloutMock.Options();
    options.checkBody = false;
    return options;
  }

  public class TokenProviderMock implements TokenProvider {

    public String getToken() {
      return 'fake-token';
    }
  }
}
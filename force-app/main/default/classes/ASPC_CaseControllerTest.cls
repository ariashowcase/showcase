/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 25-Jul-18. 
 */

@isTest
public class ASPC_CaseControllerTest {

  private static final String PORTALUSERNAME = 'PortalUser@aria.test';
  private static final String ADMINUSERNAME = 'AdminUser@aria.test';

  private static Account testAccount { get; set; }
  private static Contact testContact { get; set; }
  private static Case testCase { get; set; }

  private static User portalUser {
    get {
      if (portalUser == null) {
        portalUser = [SELECT Id FROM User WHERE UserName = :PORTALUSERNAME];
      }
      return portalUser;
    }
    set;
  }

  private static User adminUser {
    get {
      if (adminUser == null) {
        adminUser = [SELECT Id FROM User WHERE UserName = :ADMINUSERNAME];
      }
      return adminUser;
    }
    set;
  }

  @testSetup static void setup() {

    Id userRoleId = [SELECT Id FROM UserRole LIMIT 1].Id;

    adminUser = ATU_UserFactory.createDefaultBuilder('Aria', 'Admin')
        .withProfile('System Administrator')
        .withField('Username', ADMINUSERNAME)
        .withField('UserRoleId', userRoleId)
        .persist();
  }

  @isTest static void getCasesTest() {
    System.runAs(adminUser) {
      createAccountContactCaseAndOpportunity();
      createPortalUser();
    }

    System.runAs(portalUser) {
      List<Case> cases = [SELECT Id, CaseNumber, Subject, Status, CreatedDate FROM Case];

      Test.startTest();
      System.assertEquals(cases, ASPC_CaseController.getCases(null));
      System.assertEquals(cases, ASPC_CaseController.getCases(testContact.Id));
      Test.stopTest();
    }
  }

  @isTest static void getCasesTest_NoCases() {

      Test.startTest();
      System.assertEquals(0, ASPC_CaseController.getCases(null).size());
      Test.stopTest();
  }

  @isTest static void getCaseStatusesTest() {
    String objectName = 'Case';
    String field = 'Status';
    List<String> caseStatuses = new List<String>();
    for (CaseStatus status : [SELECT MasterLabel FROM CaseStatus ORDER BY SortOrder]) {
      caseStatuses.add(status.MasterLabel);
    }

    Test.startTest();
    System.assertEquals(caseStatuses, ASPC_CaseController.getCaseStatuses(objectName, field));
    Test.stopTest();
  }

  @isTest static void saveChunkTest() {
    String fileName = 'TestFileName';
    String contentType = 'text/plain';
    String base64data = 'MTIzNA==';

    createAccountContactCaseAndOpportunity();

    Test.startTest();
    Id fileId = ASPC_CaseController.saveChunk(testCase.Id, fileName, base64data, contentType, '');
    System.assert(fileId != null);

    Blob fileBody = [SELECT Id, Body FROM Attachment WHERE Id = :fileId].Body;
    ASPC_CaseController.saveChunk(testCase.Id, fileName, base64data, contentType, fileId);
    System.assertNotEquals(fileBody, [SELECT Id, Body FROM Attachment WHERE Id = :fileId].Body);
    Test.stopTest();
  }

  // Helper methods

  private static void createAccountContactCaseAndOpportunity() {
    testAccount = ATU_AccountFactory.createDefaultBuilder('testAccount').persist();
    testContact = ATU_ContactFactory.createDefaultBuilder('Doe', testAccount.Id)
        .withFirstName('John')
        .withField('Email', 'john.doe.test@test.com')
        .persist();

    ATU_OpportunityFactory.createDefaultBuilder(testAccount.Id).persist();

    testCase = ATU_CaseFactory.createDefaultBuilder('testCase')
        .withField('ContactId', testContact.Id)
        .persist();
  }

  private static void createPortalUser()  {
    String profileName = [SELECT Name FROM Profile WHERE UserType like '%portal%' LIMIT 1].Name;

    ATU_UserFactory.createDefaultBuilder('Portal', 'User')
        .withProfile(profileName)
        .withField('ContactId', testContact.Id)
        .withField('Username', PORTALUSERNAME)
        .persist();
  }
}
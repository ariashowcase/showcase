/** Copyright 2017, Aria Solutions Inc.
*
* All Rights Reserved
* Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

/*
	Handles the grabbing of records for the custom lookup page
	Loads all records the user can see at load time.
	If query is used will show all records based on SOSL query.
 */
public with sharing class AAC_CustomLookupController {
	private static String objectType;

	public String query {get; set;}
	public List<Case> cases {get; set;}
	public List<Opportunity> opportunities {get;set;}


	public AAC_CustomLookupController(){
		objectType = ApexPages.currentPage().getParameters().get('objectType');
		switch on objectType {
			when 'Case' {
				cases = [SELECT CaseNumber, Subject FROM Case];
			}
			when 'Opportunity'{
				opportunities = [SELECT Name FROM Opportunity];
			}
		}
	}
	public void runQuery()
	{
		//TODO: add fieldset for queries
		if(query.length()>1) {
			String escapedQuery = String.escapeSingleQuotes(query);
			switch on objectType {
				when 'Case' {
					List<List<SObject>> searchResults = [FIND :escapedQuery IN ALL FIELDS RETURNING Case (Id, CaseNumber, Subject)];
					cases = (List<Case>) searchResults[0];
				}
				when 'Opportunity' {
					List<List<SObject>> searchResults = [FIND :escapedQuery IN ALL FIELDS RETURNING Opportunity (Id, Name)];
					opportunities = (List<Opportunity>) searchResults[0];
				}
			}
		}
		else {
			apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR,'Search cannot be blank. Please enter a value');
			apexpages.addmessage(msg);
		}

	}
}
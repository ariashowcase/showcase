/** Copyright 2017 Aria Solutions Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

@isTest
private class AAC_CallInformationControllerTest {
  @isTest
  static void testStoreAndCallInfo() {
    if (AAC_CallInformationController.isPlatformCacheAvailable()) {
      String info = '{"contactId" : "testContact", "data" : "123"}';
      AAC_CallInformationController.storeCallInformation(info);
      Map<String, String> result = AAC_CallInformationController.returnCallInformation('testContact');

      if (result == null || result.get('data') == null) {
        // Platform cache is configured but no space is allocated.
        System.assert(true);
      } else {
        System.assertEquals('123', result.get('data'));
      }
    }
  }

  @isTest
  static void testStoreCallInformation_CodeCoverage() {
    try {
      String info = '{"contactId" : "testContact", "data" : "123"}';
      AAC_CallInformationController.storeCallInformation(info);
    } catch (Exception ex) {
      System.debug('Cache not available');
    }
  }

  @isTest
  static void testReturnCallInformation_CodeCoverage() {
    try {
      String info = '{"contactId" : "testContact", "data" : "123"}';
      AAC_CallInformationController.returnCallInformation('testContact');
    } catch (Exception ex) {
      System.debug('Cache not available');
    }
  }
}
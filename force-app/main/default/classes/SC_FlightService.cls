global with sharing class SC_FlightService {
  @TestVisible
  private static Integer MIN_MINUTES_BETWEEN_FLIGHTS = 30;
  private static final Integer MAX_FLIGHTS_IN_TRIP = 10;
  private static final Map<Id, Decimal> STANDARD_FLIGHT_PRICES = new Map<Id, Decimal>();

  public static List<FlightGroup> getFlightGroups(String origin, String destination, Datetime startTime, Datetime endTime) {
    List<Flight__c> flights = [
      SELECT  Flight_Product__r.Origin__c, Flight_Product__r.Destination__c, Flight_Product__r.ProductCode, 
              Flight_Date_Time__c, Arrival_Date_Time__c, Available_Seats__c, Flight_Status__c
      FROM    Flight__c
      WHERE   Flight_Date_Time__c >= :startTime 
          AND Arrival_Date_Time__c <= :endTime
          AND Available_Seats__c > 0
    ];

    Set<Id> flightProductIds = new Set<Id>();
    for (Flight__c flight : flights) {
      flightProductIds.add(flight.Flight_Product__c);
    }

    for (PricebookEntry pbe : [
      SELECT  Product2Id, UnitPrice
      FROM    PricebookEntry
      WHERE   Pricebook2.IsStandard = true
          AND Product2Id in :flightProductIds
    ]) {
      STANDARD_FLIGHT_PRICES.put(pbe.Product2Id, pbe.UnitPrice);
    }

    FlightGraph graph = new FlightGraph(flights);
    List<FlightGroup> result = graph.searchPaths(origin, destination);
    result.sort();

    System.debug('results: ' + result);
    return result;
  }

  public static String printFlightGroups(List<FlightGroup> groups) {
    List<String> paths = new List<String>();

    for (SC_FlightService.FlightGroup g : groups) {
      paths.add(g.toShortString());
    }

    return '[' + String.join(paths, ', ') + ']';
  }

  global class FlightGroup implements Comparable {
    public List<FlightWrapper> flights {get; set;}
    public Integer duration {get; set;}
    public Decimal cost {get; set;}

    public FlightGroup()  {}

    public FlightGroup(List<Flight__c> flightSObjs, Integer duration, Decimal cost) {
      flights = new List<FlightWrapper>();
      for (Flight__c flightSObj : flightSObjs) {
        flights.add(new FlightWrapper(flightSObj));
      }
      this.duration = duration;
      this.cost = cost;
    }

    public FlightGroup(List<FlightWrapper> flights) {
      this.flights = flights;
      cost = 0;
      Datetime startTime, endTime;
      for (FlightWrapper flight : flights) {
        if (flight.price != null) {
          cost += flight.price;
        }  
        if (startTime == null || flight.departureTime < startTime) {
          startTime = flight.departureTime;
        }
        if (endTime == null || flight.arrivalTime >= endTime) {
          endTime = flight.arrivalTime;
        }
      }
      duration = (Integer) ((endTime.getTime() - startTime.getTime()) / 60000); // convert milliseconds to minutes
    }

    public String toShortString() {
      List<String> flightStrings = new List<String>();
      for (FlightWrapper flight : flights) {
        flightStrings.add('(' + flight.flightNumber + ') ' + flight.origin + ' -> ' + flight.destination);
      }
      return '[' + String.join(flightStrings, ', ') + ']';
    }

    public Integer compareTo(Object other) {
      FlightGroup that = (FlightGroup) other;
      return this.duration - that.duration;
    }
  }

  global class FlightWrapper {
    private transient Flight__c flightSobj;
    public Id flightId {get; set;}
    public Datetime departureTime {get; set;}
    public Datetime arrivalTime {get; set;}
    public String flightNumber {get; set;}
    public String origin {get; set;}
    public String destination {get; set;}
    public Decimal price {get; set;}
    public Decimal availableSeats {get; set;}
    public String flightStatus {get; set;}

    @TestVisible
    private FlightWrapper() {
    }

    public FlightWrapper(Flight__c flight) {
      flightSobj = flight;
      flightId = flight.Id;
      flightNumber = flight.Flight_Product__r.ProductCode;
      departureTime = flight.Flight_Date_Time__c;
      arrivalTIme = flight.Arrival_Date_Time__c;
      origin = flight.Flight_Product__r.Origin__c;
      destination = flight.Flight_Product__r.Destination__c;
      price = STANDARD_FLIGHT_PRICES.get(flightSobj.Flight_Product__c);
      availableSeats = flight.Available_Seats__c;
      flightStatus = flight.Flight_Status__c;
    }
  }

  public class FlightGraph {
    public List<FlightNode> allNodes;
    public Map<String, List<FlightNode>> nodesByOrigin;
    public Map<String, List<FlightNode>> nodesByDestination;

    private FlightGraph() {
      allNodes = new List<FlightNode>();
      nodesByOrigin = new Map<String, List<FlightNode>>();
      nodesByDestination = new Map<String, List<FlightNode>>();
    }

    public FlightGraph(List<Flight__c> flightSobjs) {
      this();
      for (Flight__c flightSobj : flightSobjs) {
        FlightNode node = new FlightNode(flightSobj);
        addNode(node);
      }
    }

    @TestVisible
    private FlightGraph(List<FlightNode> nodes) {
      this();
      for (FlightNode node : nodes) {
        addNode(node);
      }
    }

    public void addNode(FlightNode node) {
      allNodes.add(node);

      // put node in origin and destination maps
      List<FlightNode> nodeList = nodesByOrigin.get(node.flight.origin);
      if (nodeList == null) {
        nodeList = new List<FlightNode>();
        nodesByOrigin.put(node.flight.origin, nodeList);
      }
      nodeList.add(node);

      nodeList = nodesByDestination.get(node.flight.destination);
      if (nodeList == null) {
        nodeList = new List<FlightNode>();
        nodesByDestination.put(node.flight.destination, nodeList);
      }
      nodeList.add(node);

      // link connected flights
      nodeList = nodesByOrigin.get(node.flight.destination);
      if (nodeList != null) {
        for (FlightNode otherNode : nodeList) {
          node.addConnection(otherNode);
        }
      }

      nodeList = nodesByDestination.get(node.flight.origin);
      if (nodeList != null) {
        for (FlightNode otherNode : nodeList) {
          otherNode.addConnection(node);
        }
      }
    }

    public List<FlightGroup> searchPaths(String origin, String destination) {
      List<FlightGroup> result = new List<FlightGroup>();

      if (!(nodesByOrigin.containsKey(origin) && nodesByDestination.containsKey(destination))) {
        return result;
      }

      List<FlightNode> nodePath = new List<FlightNode>();
      List<List<FlightNode>> validPaths = new List<List<FlightNode>>();
      List<FlightNode> startNodes = nodesByOrigin.get(origin);
      Set<String> visited = new Set<String>();

      for (FlightNode startNode : startNodes) {
        visited.add(startNode.flight.origin);
        findNode(startNode, destination, nodePath, validPaths, visited);
        visited.remove(startNode.flight.origin);
      }

      for (List<FlightNode> path : validPaths) {
        List<FlightWrapper> flights = new List<FlightWrapper>();
        for (FlightNode node : path) {
          flights.add(node.flight);
        }
        result.add(new FlightGroup(flights));
      }

      return result;
    }

    private void findNode(
      FlightNode currNode, 
      String destination, 
      List<FlightNode> nodePath, 
      List<List<FlightNode>> validPaths, 
      Set<String> visited
    ) {
      nodePath.add(currNode);
      visited.add(currNode.flight.destination);
      
      if (currNode.flight.destination == destination) {
        validPaths.add(nodePath.clone());
      }
      else if (nodePath.size() <= MAX_FLIGHTS_IN_TRIP) {
        for (FlightNode connectedNode : currNode.connectedNodes) {
          if (!visited.contains(connectedNode.flight.destination)) {
            findNode(connectedNode, destination, nodePath, validPaths, visited);
          }
        }
      }

      nodePath.remove(nodePath.size() - 1);
      visited.remove(currNode.flight.destination);
    }
  }

  public class FlightNode {
    public FlightWrapper flight;
    public List<FlightNode> connectedNodes;

    @TestVisible
    private FlightNode() {
      connectedNodes = new List<FlightNode>();
    }

    public FlightNode(Flight__c flightSobj) {
      this();
      flight = new SC_FlightService.FlightWrapper(flightSObj);
    }

    public void addConnection(FlightNode otherNode) {
      if (
        flight.destination == otherNode.flight.origin && 
        flight.arrivalTime.addMinutes(MIN_MINUTES_BETWEEN_FLIGHTS) <= otherNode.flight.departureTime
      )
      {
        connectedNodes.add(otherNode);
      }
    }
  }
}

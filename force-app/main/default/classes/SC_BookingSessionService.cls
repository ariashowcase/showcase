global with sharing class SC_BookingSessionService {
  private static String KEY_PREFIX = 'SCBookingSession';
  private static Integer TTL_SECONDS = 300; // 5 minutes

  public static String storeData(BookingData data) {
    return storeData(data, generateKey());
  }

  public static String storeData(BookingData data, String sessionKey) {
    String key = KEY_PREFIX + sessionKey;
    Cache.Org.put(key, data, TTL_SECONDS);

    return sessionKey;
  }

  public static BookingData retrieveData(String sessionKey) {
    String key = KEY_PREFIX + sessionKey;
    if (!Cache.Org.contains(key)) {
      return null;
    }

    return (BookingData) Cache.Org.get(key);
  }

  private static String generateKey() {
    String key;

    // generate a unique key
    key = String.valueOf(UserInfo.getUserId()) + String.valueOf(System.now().getTime());
    System.debug('sessionKey: ' + key);
    return key;
  }

  // adjust this as needed
  global class BookingData {
    public Id accountId;
    public List<Id> contactIds;
    public String origin;
    public String destination;
    public Date travelDate;
    public List<List<Id>> flightIdsPerTrip;
    public List<SC_TripService.SeatOptions> seatOptions;

    public BookingData() {}

    public BookingData(Id accountId, List<Id> contactIds, String origin, String destination, Date travelDate, List<List<Id>> flightIdsPerTrip, List<SC_TripService.SeatOptions> seatOptions)  {
      this.accountId = accountId;
      this.contactIds = contactIds;
      this.origin = origin;
      this.destination = destination;
      this.travelDate = travelDate;
      this.flightIdsPerTrip = flightIdsPerTrip;
      this.seatOptions = seatOptions;
    }
  }
}
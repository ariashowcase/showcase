/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@isTest
private class ASP_UpdateSentimentQueueableTest {

  /*
  @isTest static void testExecute_Success() {
    ASP_TriggerBus.resetHandlers();

    ATU_MultiHttpCalloutMock.Options options = new ATU_MultiHttpCalloutMock.Options();
    options.checkBody = false;

    Test.setMock(HttpCalloutMock.class, new ATU_MultiHttpCalloutMock(createRequestMapping(), options));

    Case c = createCase('This is great!');

    Test.startTest();
    new ASP_UpdateSentimentQueueable('Case', c.Id, c.Description).execute(null);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();

    Case result = getCase(c.id);

    System.assertEquals('positive', result.Sentiment__c);
    System.assertEquals(0.8673582, result.Sentiment_Probability__c);
  }
  */

  @isTest static void testExecute() {
    Test.setMock(HttpCalloutMock.class, new ATU_MultiHttpCalloutMock(createRequestMapping()));

    Case c = createCase('This is great!');

    Test.startTest();
    new ASP_UpdateSentimentQueueable('Case', 'Subject', null, c.Id, c.Description).execute(null);
    Test.stopTest();

    ATU_DebugInfoUtil.assertErrorsRecorded();
  }

  @isTest static void testExecute_Failure() {
    Test.setMock(HttpCalloutMock.class, new ATU_MultiHttpCalloutMock(createRequestMapping()));

    Case c = createCase('This is not great!');

    Test.startTest();
    new ASP_UpdateSentimentQueueable('Case', 'Subject', null, c.Id, c.Description, new TokenProviderMock()).execute(null);
    Test.stopTest();

    ATU_DebugInfoUtil.assertErrorsRecorded();
  }

  private static Case createCase(String description) {
    return ATU_Case.build('Foo bar').withField('Description', description).persist();
  }

  private static Map<HttpRequest, HttpResponse> createRequestMapping() {
    return new Map<HttpRequest, HttpResponse> {
      createTokenProviderRequest() => createTokenProviderResponse(),
      createSentimentRequest() => createSentimentResponse()
    };
  }

  private static HttpRequest createTokenProviderRequest() {
    HttpRequest req = new HttpRequest();
    req.setMethod('POST');
    req.setEndpoint('https://api.einstein.ai/v2/oauth2/token');
    req.setHeader('Content-type', 'application/x-www-form-urlencoded');

    return req;
  }

  private static HttpResponse createTokenProviderResponse() {
    HttpResponse resp = new HttpResponse();
    resp.setStatusCode(200);
    resp.setBody('{ "access_token": "foo-bar" }');

    return resp;
  }

  private static HttpRequest createSentimentRequest() {
    HttpRequest req = new HttpRequest();
    req.setMethod('POST');
    req.setEndpoint('https://api.einstein.ai/v2/language/sentiment');
    req.setHeader('Content-type', HttpFormDataBodyPart.GetContentType());

    return req;
  }

  private static HttpResponse createSentimentResponse() {
    HttpResponse resp = new HttpResponse();
    resp.setStatusCode(200);
    resp.setBody('{"probabilities":[{"label":"positive","probability":0.8673582},{"label":"negative","probability":0.1316828},{"label":"neutral","probability":9.590242E-4}],"object":"predictresponse"}');

    return resp;
  }

  public class TokenProviderMock implements TokenProvider {

    public String getToken() {
      throw new ASP_UpdateSentimentQueueableTest.TokenProviderMockException();
    }
  }

  public class TokenProviderMockException extends Exception { }
}
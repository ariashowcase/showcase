/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 17-Jul-18. 
 */

public with sharing class ASPC_UserInfoController {
  @AuraEnabled
  public static User getCurrentUserContactIdAndAccountId() {
    return [SELECT ContactId, AccountId FROM User WHERE Id = :UserInfo.getUserId()];
  }
}
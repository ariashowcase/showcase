/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 7/25/2018.
 */

@isTest
private class AAC_SupportCallbackFormControllerTest {
  @testSetup
  static void setup() {
    Account acc = ATU_Account.build('Test Account').persist();
    Contact con = ATU_Contact.build('Test Contact', acc.Id).persist();
  }

  @isTest
  static void testGetUserContact() {
    // not testing the positive case as the org may not have communities set up
    Contact c = AAC_SupportCallbackFormController.getUserContact();
    system.assertEquals(null, c); // community users can't run unit tests
  }

  static Map<String, String> createFormInput() {
    Map<String, String> inputMap = new Map<String, String>();
    inputMap.put('contactId', null);
    inputMap.put('firstName', 'John');
    inputMap.put('lastName', 'Smith');
    inputMap.put('phone', '+17805550163');
    inputMap.put('comments', 'Test comment');
    return inputMap;
  }

  static Map<String, String> createACMap() {
    Map<String, String> acMap = new Map<String, String>();
    acMap.put('accessKey', 'aaa');
    acMap.put('accessSecret', 'bbb');
    acMap.put('region', 'us-east-1');
    acMap.put('contactFlowId', '1234');
    acMap.put('instanceId', '111');
    acMap.put('queueId', '222');
    acMap.put('sourcePhoneNumber', '');
    return acMap;
  }

  @isTest
  static void testCreateCallCampaignWithContact() {
    Contact c = [select Id from Contact];
    Map<String, String> inputMap = createFormInput();
    inputMap.put('contactId', c.Id);
    AAC_SupportCallbackFormController.createCallCampaign(inputMap);
    AAC_Call_Campaign__c cc = [select Contact__c, Phone_Number__c, Comments__c from AAC_Call_Campaign__c];

    system.assertEquals(c.Id, cc.Contact__c);
    system.assertEquals('+17805550163', cc.Phone_Number__c);
    system.assertEquals('Test comment', cc.Comments__c);
  }

  @isTest
  static void testCreateCallCampaign() {
    Map<String, String> inputMap = createFormInput();
    AAC_SupportCallbackFormController.createCallCampaign(inputMap);
    AAC_Call_Campaign__c cc = [select Phone_Number__c, Comments__c from AAC_Call_Campaign__c];

    system.assertEquals('+17805550163', cc.Phone_Number__c);
    system.assert(cc.Comments__c.contains('John'));
    system.assert(cc.Comments__c.contains('Smith'));
    system.assert(cc.Comments__c.contains('Test comment'));
  }

  @isTest
  static void testCreateOutboundCallSuccess() {
    Test.setMock(HttpCalloutMock.class, new AcCalloutMock(true));
    Map<String, String> inputMap = createFormInput();
    Map<String, String> acMap = createACMap();
    Contact c = [select Id from Contact];
    inputMap.put('contactId', c.Id);

    Test.startTest();
    AAC_SupportCallbackFormController.createOutboundCall(inputMap, acMap);
    Test.stopTest();

    // no database changes, so nothing to assert besides there not being any errors
    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  static void testCreateOutboundCallFail() {
    Test.setMock(HttpCalloutMock.class, new AcCalloutMock(false));
    Map<String, String> inputMap = createFormInput();
    Map<String, String> acMap = createACMap();

    Test.startTest();
    try {
      AAC_SupportCallbackFormController.createOutboundCall(inputMap, acMap);
      System.assert(false, 'Expecting exception to be thrown.');
    }
    catch (Exception e) {
      ATU_DebugInfoUtil.assertErrorsRecorded();
    }
    Test.stopTest();
  }

  @isTest
  static void testErrors() {
    Map<String, String> inputMap = createFormInput();
    // use an Account Id instead of a Contact Id
    inputMap.put('contactId', '0016C00000AUb4h');
    try {
      AAC_SupportCallbackFormController.createCallCampaign(inputMap);
      System.assert(false, 'Expecting exception to be thrown.');
    }
    catch (Exception e) {
      ATU_DebugInfoUtil.assertErrorsRecorded();
    }

    delete [select Id from AU_DebugInfo__c];

    // how about a random paragraph for a first name?
    inputMap.put('comments', 'The wish recovers after the collective! The pedantry mobs a phrase under the eligible mankind. A growing controversy stumbles inside an infinite consent. Does the touched thread stray beneath the alcoholic? A scrap romance oils the gulf on top of a hazy appearance.');
    try {
      AAC_SupportCallbackFormController.createCallCampaign(inputMap);
      System.assert(false, 'Expecting exception to be thrown.');
    }
    catch (Exception e) {
      ATU_DebugInfoUtil.assertErrorsRecorded();
    }
  }

  class AcCalloutMock implements HttpCalloutMock {
    Boolean isSuccess;

    AcCalloutMock(Boolean isSuccess) {
      this.isSuccess = isSuccess;
    }

    public HttpResponse respond(HttpRequest req) {
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/json');
      if (isSuccess) {
        res.setStatusCode(200);
        res.setBody('{"ContactId": "6789"}');
      }
      else {
        res.setStatusCode(400);
      }
      return res;
    }
  }
}
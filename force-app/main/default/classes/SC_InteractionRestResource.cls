/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Monday October 21st 2019
 * Author: varonov
 * File type: '.cls'
 */

@RestResource(urlMapping='/Interaction/*')
global with sharing class SC_InteractionRestResource {
	@HttpPost
	global static CreateInteractionResponse doPost(CreateInteractionRequest request)	{
		String taskId = '';
		if (request.conversationId != null)	{
			taskId = SC_InteractionService.createTask(request.conversationId, request.startTime, request.contactId, request.direction, request.type, request.fromAddress);
		}
		return new CreateInteractionResponse(taskId);
	}

	@HttpPut
  global static CreateInteractionResponse doPut(CreateInteractionRequest request) {
    String taskId = '';
		if (request.taskId != null)	{
			taskId = SC_InteractionService.updateTask(request.taskId, request.startTime, request.endTime, request.queueName, request.wrapup, request.durationSeconds);
		}
		return new CreateInteractionResponse(taskId);
  }

	global class CreateInteractionRequest	{
		public String conversationId {get; set;}
		public Id taskId {get; set;}
		public Id contactId {get; set;}
		public Datetime startTime {get; set;}
		public Datetime endTime {get; set;}
		public String direction {get; set;}
    public String type {get; set;}
    public String fromAddress {get; set;}
		public String queueName {get; set;}
    public String wrapup {get; set;}
		public Integer durationSeconds {get; set;}
	}

	global class CreateInteractionResponse {
		public String taskId {get; set;}

		public CreateInteractionResponse(String taskId) {
			this.taskId = taskId;
		}
	}
}
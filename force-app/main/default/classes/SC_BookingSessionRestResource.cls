@RestResource(urlMapping='/BookingSession/*')
global with sharing class SC_BookingSessionRestResource {
  @HttpPost
  global static String storeData(SC_BookingSessionService.BookingData data, String key) {
    if (String.isBlank(key))
      return SC_BookingSessionService.storeData(data);
    else
      return SC_BookingSessionService.storeData(data, key);
  }

  @HttpGet
  global static SC_BookingSessionService.BookingData retrieveData() {
    String key = RestContext.request.params.get('key');
    SC_BookingSessionService.BookingData data = SC_BookingSessionService.retrieveData(key);
    if (data == null) {
      RestContext.response.statusCode = 400;
      RestContext.response.responseBody = Blob.valueOf('Invalid key');
      return null;
    }

    return data;
  }
}

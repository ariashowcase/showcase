/** Copyright 2017 Aria Solutions Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

@isTest
private class AAC_UpdateReportAgentFieldTest {

  private static final Profile STANDARD_USER;

  static {
    STANDARD_USER = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
  }

  private static User ConnectUser {
    get {
      if (ConnectUser == null) {
        ConnectUser = [SELECT Id, FirstName, LastName, Username FROM User WHERE LastName = 'ConnectUser'];
      }
      return ConnectUser;
    }
    set;
  }

  private static User PureSalesforceUser {
    get {
      if (PureSalesforceUser == null) {
        PureSalesforceUser = [SELECT Id, FirstName, LastName, Username FROM User WHERE LastName = 'PureSalesforceUser'];
      }
      return PureSalesforceUser;
    }
    set;
  }

  @TestSetup static void setup() {
    createUser('ConnectUser', 'john.smith');
    createUser('PureSalesforceUser', '');
  }

  @isTest static void testUserAssignment() {
    List<ACT_HistoricalReportData__c> newRecords = new List<ACT_HistoricalReportData__c> {
      new ACT_HistoricalReportData__c( Type__c = 'Queue', AC_Object_Name__c = 'Queue ABC'),
      new ACT_HistoricalReportData__c( Type__c = 'Agent', AC_Object_Name__c = 'john.smith')
    };

    System.assertEquals(0, [SELECT Type__c, AC_Object_Name__c, Agent__c FROM ACT_HistoricalReportData__c].size());

    Test.startTest();
    insert newRecords;
    Test.stopTest();

    System.assertEquals(2, [SELECT Type__c, AC_Object_Name__c, Agent__c FROM ACT_HistoricalReportData__c].size());

    ACT_HistoricalReportData__c agentRecord = [SELECT Type__c, AC_Object_Name__c, Agent__c FROM ACT_HistoricalReportData__c WHERE Type__c = 'Agent'];
    System.assertEquals(ConnectUser.Id, agentRecord.Agent__c);

    ACT_HistoricalReportData__c queueRecord = [SELECT Type__c, AC_Object_Name__c, Agent__c FROM ACT_HistoricalReportData__c WHERE Type__c = 'Queue'];
    System.assert(String.isBlank(queueRecord.Agent__c));
  }

  private static void createUser(String lastName, String connectUserName) {
    User u = new User(
      FirstName = 'John',
      LastName = lastName,
      Alias = 'foo',
      Username = lastName.toLowerCase() + '@test.com',
      Email = lastName.toLowerCase() + '@testcompany.com',
      ProfileId = STANDARD_USER.Id,
      EmailEncodingKey='UTF-8',
      LanguageLocaleKey='en_US',
      LocaleSidKey='en_US',
      TimeZoneSidKey='America/Los_Angeles',
      Amazon_Connect_Username__c = connectUserName
    );

    insert u;
  }
}
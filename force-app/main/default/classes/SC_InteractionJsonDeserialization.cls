/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Tuesday October 22nd 2019
 * Author: varonov
 * File type: '.cls'
 */


public with sharing class SC_InteractionJsonDeserialization {
  public class Metrics {
		public String name;
		public Integer value;
		public String emitDate;
	}

	public String conversationId;
	public String conversationStart;
	public String conversationEnd;
	public String originatingDirection;
	public List<Participants> participants;
	public List<String> divisionIds;

	public class Sessions {
		public String mediaType;
		public String sessionId;
		public String addressFrom;
		public String addressTo;
		public String messageType;
		public String direction;
		public String peerId;
		public List<Segments> segments;
		public List<Metrics> metrics;
		public String provider;
	}

	public class Segments {
		public String segmentStart;
		public String segmentEnd;
		public String queueId;
		public String segmentType;
		public Boolean conference;
		public String disconnectType;
		public String wrapUpCode;
	}

	public class Participants {
		public String participantId;
		public String purpose;
		public List<Sessions> sessions;
		public String userId;
	}

	
	public static SC_InteractionJsonDeserialization parse(String json) {
		return (SC_InteractionJsonDeserialization) System.JSON.deserialize(json, SC_InteractionJsonDeserialization.class);
	}
}

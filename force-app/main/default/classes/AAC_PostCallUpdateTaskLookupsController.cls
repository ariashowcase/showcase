/** Copyright 2017 Aria Solutions Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

public with sharing class AAC_PostCallUpdateTaskLookupsController {

  public AAC_PostCallUpdateTaskLookupsController(ApexPages.StandardController stdController) { }

  public List<SelectOption> getDispositionCodes() {
    List<SelectOption> dispositionCodes = new List<SelectOption>();
    for(AAC_Call_Disposition_Code__mdt dc: [SELECT DeveloperName, MasterLabel, Disposition_Code__c FROM AAC_Call_Disposition_Code__mdt WHERE Active__c = true ORDER BY DeveloperName ASC]) {
      dispositionCodes.add(new SelectOption(dc.Disposition_Code__c, dc.MasterLabel));
    }

    return dispositionCodes;
  }
}
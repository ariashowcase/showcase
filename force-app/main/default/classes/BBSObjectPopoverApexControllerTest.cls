/** Copyright 2017, Aria Solutions Inc.
*
* All Rights Reserved
* Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@IsTest
private class BBSObjectPopoverApexControllerTest {
    @IsTest
    static void retrieveContact(){
      
      Contact con = new Contact(FirstName='FirstTestName', LastName='LastTestName');
      insert con;

      Case c = new Case(ContactId=con.Id, Subject='new test subject');
      insert c;

      test.startTest();

      Contact con2 = BBSObjectPopoverApexController.getContact(c.Id);

      test.stopTest();

      system.assertEquals('FirstTestName LastTestName', con2.Name);
      
    }
}
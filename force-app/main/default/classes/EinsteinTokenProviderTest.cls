/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@isTest
private class EinsteinTokenProviderTest {

  @isTest static void testTokenProvider() {
    Test.setMock(HttpCalloutMock.class, new ATU_SimpleHttpCalloutMock(createExpectedRequest(), createMockResponse(), createRequestOptions()));
    TokenProvider tokenProvider = EinsteinTokenProvider.forProfile('ApexUnitTest');

    String token = tokenProvider.getToken();

    System.assertEquals('foo-bar', token);
  }

  private static HttpRequest createExpectedRequest() {
    HttpRequest req = new HttpRequest();
    req.setMethod('POST');
    req.setEndpoint('https://api.einstein.ai/v2/oauth2/token');
    req.setHeader('Content-type', 'application/x-www-form-urlencoded');

    return req;
  }

  private static HttpResponse createMockResponse() {
    HttpResponse resp = new HttpResponse();
    resp.setStatusCode(200);
    resp.setBody('{ "access_token": "foo-bar" }');

    return resp;
  }

  private static ATU_SimpleHttpCalloutMock.Options createRequestOptions() {
    ATU_SimpleHttpCalloutMock.Options options = new ATU_SimpleHttpCalloutMock.Options();
    options.checkBody = false;
    return options;
  }
}
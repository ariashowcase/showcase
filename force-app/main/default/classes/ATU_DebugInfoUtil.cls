/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
public with sharing class ATU_DebugInfoUtil {

  public static void assertNoErrors () {

    List<AU_DebugInfo__c> errors = [SELECT Id, Component__c, Data__c, ExtraInfo__c  FROM AU_DebugInfo__c WHERE Level__c = 'Error'];
    for (AU_DebugInfo__c de : errors) {
      System.Debug('ATU_DebugInfoUtil.assertNoErrors Component = ' + de.Component__c + ' Data = ' + de.Data__c + ' ExtraInfo = ' + de.ExtraInfo__c );
    }
    System.assertEquals(0, errors.size(), 'Errors reported: ' + String.valueOf(errors));
  }

  public static List<AU_DebugInfo__c> assertErrorsRecorded() {
    List<AU_DebugInfo__c> errors = [SELECT Id, Component__c, Data__c, ExtraInfo__c  FROM AU_DebugInfo__c WHERE Level__c = 'Error'];
    System.assert(errors.size() > 0, 'There are no errors reported, although it was expected.');
    return errors;
  }
}
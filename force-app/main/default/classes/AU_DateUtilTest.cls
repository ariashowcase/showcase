/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@isTest
private class AU_DateUtilTest {

    @isTest static void testAddBusinessDays() {

        Date monday = Date.newInstance(2018, 2, 5);
        Date friday = Date.newInstance(2018, 2, 9);
        Integer twoDays = 2;
        Integer sevenDays = 7;

        System.assertEquals(Date.newInstance(2018, 2, 7), AU_DateUtil.addBusinessDays(monday, twoDays));
        System.assertEquals(Date.newInstance(2018, 2, 14), AU_DateUtil.addBusinessDays(monday, sevenDays));
        System.assertEquals(Date.newInstance(2018, 2, 13), AU_DateUtil.addBusinessDays(friday, twoDays));
        System.assertEquals(Date.newInstance(2018, 2, 20), AU_DateUtil.addBusinessDays(friday, sevenDays));
    }

}
global with sharing class SC_TripService {
  public static Trip__c getTrip(String confirmationNo) {
    List<Trip__c> trips = [
      SELECT  Account__r.Id, Account__r.Name, Confirmation_Number__c, Date__c, Destination__c, Order__c, Origin__c, Status__c, (
        SELECT  Contact__r.Id, Contact__r.Name, Flight__r.Origin__c, Flight__r.Destination__c, Flight__r.Flight_Status__c, Flight__r.Name,
                Flight_Date_Time__c, Insurance__c, Meal__c, Seat_Number__c, Seat_Type__c, Status__c
        FROM    Booked_Seats__r
      )
      FROM    Trip__c
      WHERE   Confirmation_Number__c = :confirmationNo
    ];

    if (trips.isEmpty()) {
      return null;
    }

    return trips[0];
  }

  public static OrderConfirmation bookTrips(Id accountId, List<Id> contactIds, List<List<Id>> flightIdsPerTrip) {
    return bookTrips(accountId, contactIds, flightIdsPerTrip, new List<SeatOptions>());
  }

  public static OrderConfirmation bookTrips(Id accountId, List<Id> contactIds, List<List<Id>> flightIdsPerTrip, List<SeatOptions> seatOptions) {
    fflib_ISObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<Schema.SObjectType> {
      Order.SObjectType,
      OrderItem.SObjectType,
      Trip__c.SObjectType,
      Booked_Seat__c.SObjectType
    });

    Set<Id> allFlightIds = new Set<Id>();
    for (List<Id> flightIds : flightIdsPerTrip) {
      allFlightIds.addAll(flightIds);
    }

    if (accountId == null || contactIds.isEmpty() || allFlightIds.isEmpty()) {
      return null;
    }

    Map<Id, Flight__c> flightsById = new Map<Id, Flight__c> ([
      SELECT  Flight_Product__c, Flight_Date_Time__c, Arrival_Date_Time__c, Origin__c, Destination__c
      FROM    Flight__c
      WHERE   Id in :allFlightIds
    ]);

    Order order = createOrder(uow, accountId, flightsById, contactIds.size());

    // create Trips and Booked Seats
    List<Booked_Seat__c> seats = new List<Booked_Seat__c>();
    for (List<Id> flightIds : flightIdsPerTrip) {
      Trip__c trip = new Trip__c(
        Account__c = accountId
      );
      uow.registerNew(trip, Trip__c.Order__c, order);

      Flight__c firstFlight, lastFlight;

      for (Id flightId : flightIds) {
        Flight__c flight = flightsById.get(flightId);
        for (Id contactId : contactIds) {
          Booked_Seat__c seat = new Booked_Seat__c(
            Flight__c = flightId,
            Contact__c = contactId
          );
          seats.add(seat);
          uow.registerNew(seat, Booked_Seat__c.Trip__c, trip);
        }

        if (firstFlight == null || flight.Flight_Date_Time__c < firstFlight.Flight_Date_Time__c) {
          firstFlight = flight;
        }
        if (lastFlight == null || flight.Arrival_Date_Time__c > lastFlight.Arrival_Date_Time__c) {
          lastFlight = flight;
        }
      }

      trip.Origin__c = firstFlight.Origin__c;
      trip.Destination__c = lastFlight.Destination__c;
      trip.Date__c = firstFlight.Flight_Date_Time__c.date();
    }

    if (seatOptions != null) {
      applySeatOptions(seats, seatOptions);
    }

    uow.commitWork();

    return new OrderConfirmation(order.Id);
  }

  public static void cancelTrip(Id tripId, Boolean cancelOrder) {
    Trip__c trip = [
      SELECT  Order__c, (
        SELECT  Id 
        FROM    Booked_Seats__r
      )
      FROM    Trip__c
      WHERE   Id = :tripId
    ];

    if (cancelOrder) {
      trip.Status__c = 'Cancelled';
      for (Booked_Seat__c seat : trip.Booked_Seats__r) {
        seat.Status__c = 'Cancelled';
      }
      Order order = new Order(
        Id = trip.Order__c,
        Status = 'Cancelled'
      );
      update order;
    }
    else {
      trip.Status__c = 'Rescheduled';
      for (Booked_Seat__c seat : trip.Booked_Seats__r) {
        seat.Status__c = 'Rescheduled';
      }
      Order order = new Order(
        Id = trip.Order__c,
        Status = 'Rescheduled'
      );
      update order;
    }
    update trip;
    update trip.Booked_Seats__r;
  }

  public static OrderConfirmation rescheduleTrips(String confirmationNumber, List<List<Id>> flightIdsPerTrip) {
    Trip__c trip = getTrip(confirmationNumber);
    List<Id> contactIds = new List<Id>();
    List<SeatOptions> seatOptions = new List<SeatOptions>();
    for(Booked_Seat__c seat : trip.Booked_Seats__r) {
      contactIds.add(seat.Contact__c);
      seatOptions.add(new seatOptions (
        seat.Flight__c,
        seat.Contact__c,
        seat.Seat_Type__c,
        (seat.Insurance__c != null) ? seat.Insurance__c.split(';') : null,
        (seat.Meal__c != null) ? seat.Meal__c.split(';') : null
      ));
    }
    
    cancelTrip(trip.Id, false);
    OrderConfirmation confirmation = bookTrips(trip.Account__c, contactIds, flightIdsPerTrip, seatOptions);   
    return confirmation;
  }

  // creates the Order and OrderItems records
  private static Order createOrder(fflib_ISObjectUnitOfWork uow, Id accountId, Map<Id, Flight__c> flightsById, Integer seatsPerFlight) {
    Set<Id> productIds = new Set<Id>();
    for (Flight__c flight : flightsById.values()) {
      productIds.add(flight.Flight_Product__c);
    }
    Map<Id, PricebookEntry> pricebookEntriesByProductId = new Map<Id, PricebookEntry>();
    for (PricebookEntry pbe : [
      SELECT  UnitPrice, Product2Id, Pricebook2Id
      FROM    PricebookEntry
      WHERE   Pricebook2.IsStandard = true
          AND Product2Id in :productIds
    ]) {
      pricebookEntriesByProductId.put(pbe.Product2Id, pbe);
    }

    Order order = new Order(
      AccountId = accountId,
      Pricebook2Id = pricebookEntriesByProductId.values()[0].Pricebook2Id,
      Status = 'Confirmed'
    );
    uow.registerNew(order);

    Datetime orderStartTime, orderEndTime;
    for (Id flightId : flightsById.keySet()) {
      Flight__c flight = flightsById.get(flightId);
      PricebookEntry pbe = pricebookEntriesByProductId.get(flight.Flight_Product__c);

      if (orderStartTime == null || flight.Flight_Date_Time__c < orderStartTime) {
        orderStartTime = flight.Flight_Date_Time__c;
      }
      if (orderEndTime == null || flight.Arrival_Date_Time__c < orderEndTime) {
        orderEndTime = flight.Arrival_Date_Time__c;
      }
      
      OrderItem item = new OrderItem(
        Product2Id = flight.Flight_Product__c,
        PricebookEntryId = pbe.Id,
        UnitPrice = pbe.UnitPrice,
        Quantity = seatsPerFlight
      );
      uow.registerNew(item, OrderItem.OrderId, order);
    }
    order.EffectiveDate = orderStartTime.date();
    order.EndDate = orderEndTime.date();

    return order;
  }

  private static void applySeatOptions(List<Booked_Seat__c> seats, List<SeatOptions> optionsList) {
    SeatMap seatmap = new SeatMap(seats);
    for (SeatOptions options : optionsList) {
      if (options.flightId != null && options.contactId != null) {
        Booked_Seat__c seat = seatMap.getSeatByBothIds(options.flightId, options.contactId);
        applySeatOptions(seat, options);
      }
      else if (options.flightId != null && options.contactId == null) {
        List<Booked_Seat__c> filteredSeats = seatMap.getSeatsByFlightId(options.flightId);
        if (filteredSeats != null) {
          for (Booked_Seat__c seat : filteredSeats) {
            applySeatOptions(seat, options);
          }
        }
      }
      else if (options.flightId == null && options.contactId != null) {
        List<Booked_Seat__c> filteredSeats = seatMap.getSeatsByContactId(options.contactId);
        if (filteredSeats != null) {
          for (Booked_Seat__c seat : filteredSeats) {
            applySeatOptions(seat, options);
          }
        }
      }
    }
  }

  private static void applySeatOptions(Booked_Seat__c seat, SeatOptions options) {
    if (String.isNotBlank(options.SeatType)) {
      seat.Seat_Type__c = options.seatType;
    }
    if (options.insurances != null && !options.insurances.isEmpty()) {
      seat.Insurance__c = String.join(options.insurances, ';');
    }
    if (options.meals != null && !options.meals.isEmpty()) {
      seat.Meal__c = String.join(options.meals, ';');
    }
  }

  private class SeatMap {
    Map<Id, Map<Id, Booked_Seat__c>> seatsByBothIds; // first by flight Id, then by contact Id
    Map<Id, List<Booked_Seat__c>> seatsByContactId;
    
    SeatMap(List<Booked_Seat__c> seats) {
      seatsByBothIds = new Map<Id, Map<Id, Booked_Seat__c>>();
      seatsByContactId = new Map<Id, List<Booked_Seat__c>>();

      for (Booked_Seat__c seat : seats) {
        addSeat(seat);
      }
    }

    void addSeat(Booked_Seat__c seat) {
      Id flightId = seat.Flight__c;
      Id contactId = seat.Contact__c;

      Map<Id, Booked_Seat__c> subMap = seatsByBothIds.get(flightId);
      if (subMap == null) {
        subMap = new Map<Id, Booked_Seat__c>();
        seatsByBothIds.put(flightId, subMap);
      }
      subMap.put(contactId, seat);

      List<Booked_Seat__c> subList = seatsByContactId.get(contactId);
      if (subList == null) {
        subList = new List<Booked_Seat__c>();
        seatsByContactId.put(contactId, subList);
      }
      subList.add(seat);
    }

    List<Booked_Seat__c> getSeatsByFlightId(Id flightId) {
      if (!seatsByBothIds.containsKey(flightId)) {
        return null;
      }

      return seatsByBothIds.get(flightId).values();
    }

    List<Booked_Seat__c> getSeatsByContactId(Id contactId) {
      return seatsByContactId.get(contactId);
    }

    Booked_Seat__c getSeatByBothIds(Id flightId, Id contactId) {
      if (!seatsByBothIds.containsKey(flightId)) {
        return null;
      }

      return seatsByBothIds.get(flightId).get(contactId);
    }
  }

  global class SeatOptions {
    public Id flightId {get; set;}
    public Id contactId {get; set;}
    public String seatType {get; set;}
    public List<String> insurances {get; set;}
    public List<String> meals {get; set;}

    public SeatOptions()  {}

    public SeatOptions(Id flightId, Id contactId, String seatType, List<String> insurances, List<String> meals)  {
      this.flightId = flightId;
      this.contactId = contactId;
      this.seatType = seatType;
      this.insurances = insurances;
      this.meals = meals;
    }
  }

  global class OrderConfirmation {
    public String orderNo {get; set;}
    public List<String> tripConfirmationNos {get; set;}

    public OrderConfirmation(Id orderId) {
      Order order = [
        SELECT  OrderNumber, (
          SELECT  Confirmation_Number__c
          FROM    Trips__r
          ORDER BY Date__c
        )
        FROM Order 
        WHERE Id = :orderId
      ];

      orderNo = order.OrderNumber;
      tripConfirmationNos = new List<String>();
      for (Trip__c trip : order.Trips__r) {
        tripConfirmationNos.add(trip.Confirmation_Number__c);
      }
    }
  }
}
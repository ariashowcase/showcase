<!--
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Friday August 9th 2019
 * Author: varonov
 */
-->

<apex:page id="SC_BookTrip" docType="html-5.0" standardController="Account" extensions="SC_BookTripController" lightningStylesheets="true" title="Book Trip" action="{!completeBooking}">
  <apex:slds />
  <head>
    <meta charset="utf-8" />
    <title>Book Trip</title>
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <apex:includeScript value="/lightning/lightning.out.js" />
    <apex:includeScript value="/support/console/44.0/integration.js" />
    <apex:includeScript value="/support/api/44.0/interaction.js"/>
    <apex:includeScript value="{!URLFOR($Resource.AU_Resources, '/lib/jquery.js')}"  />
    <apex:includeScript value="{!URLFOR($Resource.AU_Resources, 'bootstrap.js')}" />
  </head>

  <style>
    .hidden: {
      display: none;
    }
    .textInput {
    	margin-left: 0 !important;
    }
    .form_block {
      width: 80%;
      display: block;
      margin: auto;
    }
    .col{
      width:20%;
      display:inline-block;
      /* float: left; */
      position: relative;
      margin: 0 10px;
    }
    .search_box {
      height: 200px;
    }
    .flight_list  {
      max-height: 500px;
      overflow: auto;
      width: 80%;
      margin: auto;
    }
    .flight_list table th {
      font-weight: bold;
    }
    .flight_list table th, .flight_list table td {
      text-align: center;
      margin: auto;
      padding: 10px;
    }
    .main_column  {
      width: 80%;
    }
    .save_button  {
      width: 200px;
      position: relative;
      margin: 30px auto;
    }
    .error  {
      text-align: center;
      color: red;
      position: relative;
      right: 50px;
    }
    .selected_flights_panel {
      /* padding: 80px; */
    }
    .inner_flight {
      padding: 10px 0;
    }
    .booked_flight_panel  {
      width: 80%;
      margin: auto;
    }
    .package_inner_box  {
      margin: 50px auto 20px;
      width: 300px;
    }
    .upgrade_flight_panel {
      margin: auto;
    }
    table.buttons_block {
      margin: 30px auto;
    }
    table.buttons_block td  {
      padding: 0 20px;
    }
    .contacts_panel {
      display: block;
      margin: 50px 0;
      margin: 20px;
    }
    .contact_box  {
      width: 200px;
    }
    .multiselect  {
      max-width: 120px !important;
      max-height: 125px !important;
    }
    h3.confirmation {
      margin-bottom: 20px;
    }
    h3.confirmation {
      margin-bottom: 20px;
    }
  </style>
  <script>
    function getSelected()  {
      var selectedRow = document.querySelector('input[name="selectedGroup"]:checked').value;
      var hiddenElement = document.getElementById('hiddenGroupNumber');
      hiddenElement.value = selectedRow;
    }

    function validateInput()  {
      var radios = document.querySelectorAll('input[type="radio"]:checked');
      if (radios.length > 0) {
        afterValidation(); 
      }
      else{
        document.getElementById('errorMessage').innerHTML = 'Please select flights!';
      }
    }

    function showToast(message, type)  {
      sforce.one.showToast({
        "message": message,
        "type": type
      });
    }

    function copyToClipboard(copyTextId)  {
      var copyText = document.getElementById(copyTextId).innerText;
      var hiddenInput = document.createElement("input");
      hiddenInput.setAttribute("value", copyText);
      document.body.appendChild(hiddenInput);
      hiddenInput.select();
      document.execCommand("copy");
      document.body.removeChild(hiddenInput);
    }

    window.onload = function(){
      var elems = document.querySelectorAll('[id$="setRequired"]');
      for (var i = 0; i < elems.length; i++){
          elems[i].required = true;
      }
    }
  </script>

  <apex:include pageName="AU_Include" />
  <body>
    <div class="search_box" style="{!IF(showUpgradePackage, 'display:none', 'display:block')}">
      <apex:form styleClass="slds-scope form_block" id="form">        
        <apex:pageBlock title="Search Flights" id="pageBlock">
          <div class="col">
            <div class="slds-form-element">
              <label class="slds-form-element__label">Origin: </label>
              <div class="slds-form-element__control">
                <apex:input type="text" style="width:200px;" html-placeholder="Origin" styleClass="textInput" id="OriginParam" value="{!origin}" required="true" />
              </div>
            </div>
          </div>
          <div class="col">
            <div class="slds-form-element">
              <label class="slds-form-element__label">Destination:</label>
              <div class="slds-form-element__control">
                <apex:input type="text" style="width:200px;" html-placeholder="Destination" styleClass="textInput" id="DestinationParam" value="{!destination}" required="true" />
              </div>
            </div>
          </div>
          <div class="col">
            <div class="slds-form-element">
              <label class="slds-form-element__label">Trip Date:</label>
              <div class="slds-form-element__control">
                <apex:input type="date" style="width:200px;" html-placeholder="Travel Date" styleClass="textInput" id="DateTimeSelector" value="{!flightDate}" required="true" />
              </div>
            </div>        
          </div>
          <div class="col">     
            <div class="slds-form-element">
              <label class="slds-form-element__label"></label>
              <div class="slds-form-element__control">
                <apex:commandButton action="{!searchFlights}" value="Search Flights" styleClass="slds-button slds-button_brand" />  
              </div>
            </div>                 
          </div>
        </apex:pageBlock>
      </apex:form>
    </div>
    <apex:outputText value="{!callToast}" escape="false"></apex:outputText>
    <div class="flight_list bPageBlock" style="{!IF(flightIsFound, 'display:block', 'display:none')}">
      <apex:form id="SelectTripForm">
        <table>
          <tr>
            <th>Option</th>
            <th class="main_column">Flights</th>
            <th>Cost</th>
          </tr>
          <apex:variable var="i" value="{!0}"/>
          <apex:repeat var="group" value="{!groups}">
          <apex:variable var="i" value="{!i+1}"/>
          <tr>
            <td>           
              <input type="radio" name="selectedGroup" value="{!i}" id="selected" onclick="getSelected()"/>
            </td>
            <td class="main_column">
              <apex:pageBlock >
                <apex:pageBlockTable value="{!group.flights}" var="gr" id="selectedFlights">
                  <apex:column headerValue="Origin" value="{!gr.origin}"/>
                  <apex:column headerValue="Destination" value="{!gr.destination}"/>
                  <apex:column headerValue="Departure" value="{!gr.departureTime}"/>
                  <apex:column headerValue="Arrival" value="{!gr.arrivalTime}"/>               
                  <apex:column headerValue="Flight Number" value="{!gr.flightNumber}"/>
                  <apex:column headerValue="Available Seats" value="{!gr.availableSeats}"/>
                  <apex:column headerValue="Flight Status" value="{!gr.flightStatus}"/>
                </apex:pageBlockTable>
              </apex:pageBlock>
              <input type="hidden" id="hiddenGroupNumber" name="hiddenGroupNumber" value=""/>
            </td>
            <td>
              <apex:outputText value="${!group.cost}"/>
            </td>
            <td>
              <apex:outputPanel id="copyToClipboardPanel" style="display:none">
                <apex:variable value="{!1}" var="rowNum"/>
                <apex:repeat value="{!group.flights}" var="gr">
                  <apex:outputText value="Flight {!rowNum}: {!gr.origin} - {!gr.destination}, Departure: {!gr.departureTime}, Arrival: {!gr.arrivalTime}, Flight#: {!gr.flightNumber}; "/>
                  <apex:variable var="rowNum" value="{!rowNum + 1}"/>
          </apex:repeat>
              </apex:outputPanel>
              <input type="button" value="Copy Flights" class="slds-button slds-button_neutral" onclick="copyToClipboard('{!$Component.copyToClipboardPanel}');" />
            </td>
          </tr> 
          </apex:repeat>
        </table>
        <div class="error">
          <span id="errorMessage"></span>
        </div>
        <div class="save_button">
          <input type="button" value="Select Trip" class="slds-button slds-button_brand" onclick="validateInput();" />
          <apex:actionFunction name="afterValidation" action="{!addTrip}"/>
        </div>
      </apex:form>      
    </div>
    <div class="booked_flight_panel" style="{!IF(tripAdded, 'display:block', 'display:none')}">
      <apex:form styleClass="slds-scope" id="booked_flights">
        <apex:pageBlock title="Booked Flights" id="pageBlock">
          <apex:pageBlockTable value="{!flights}" var="flight">
            <apex:column headerValue="Origin" value="{!flight.Origin__c}" />
            <apex:column headerValue="Destination" value="{!flight.Destination__c}" />
            <apex:column headerValue="Flight Date/Time" value="{!flight.Flight_Date_Time__c}" />
            <apex:column headerValue="Flight Status" value="{!flight.Flight_Status__c}" />
          </apex:pageBlockTable>         
          <div class="slds-form-element contacts_panel">
            <apex:pageBlockSection >           
              <c:SC_MultiselectPicklist leftLabel="Available Travellers"
                leftOption="{!allContacts}"
                rightLabel="Selected Travellers"
                rightOption="{!selectedContacts}"
                size="5"
                width="150px"/>
            </apex:pageBlockSection>        
          </div>
          <div class="slds-form-element package_inner_box">       
            <apex:commandButton action="{!reviewTrip}" value="Review Trip" styleClass="slds-button slds-button_brand" />
          </div>              
        </apex:pageBlock> 
      </apex:form>               
    </div>
    <div class="booked_flight_panel" style="{!IF(showUpgradePackage, 'display:block', 'display:none')}">
      <apex:form styleClass="slds-scope" id="upgrade_package">
        <apex:pageBlock title="Upgrade Package" id="packageBlock" mode="Edit">
          <div class="panel bPageBlock">
            <apex:pageBlockTable value="{!flightPackage}" var="package">
              <apex:column headerValue="Flight Number" value="{!package.flightName}"/>
              <apex:column headerValue="Traveller" value="{!package.contactName}"/>
              <apex:column headerValue="Origin" value="{!package.origin}"/>
              <apex:column headerValue="Destination" value="{!package.destination}"/>
              <apex:column headerValue="Flight Date/Time" value="{!package.flightDateTime}"/>
              <apex:column headerValue="Seat Type">
                <apex:selectList size="1" value="{!package.seatType}" required="true" multiselect="false"> 
                  <apex:selectOptions value="{!selectedSeatTypes}"/>  
                </apex:selectList>
              </apex:column>
              <apex:column headerValue="Meal (Multiselect)">
                <apex:selectList size="3" value="{!package.meals}" required="false" multiselect="true"> 
                  <apex:selectOptions value="{!selectedMeals}"/>  
                </apex:selectList>
              </apex:column>
            </apex:pageBlockTable>
          </div>
          <div class="panel bPageBlock">
            <apex:pageBlockSection >
              <c:SC_MultiselectPicklist leftLabel="Available Insurance"
                leftOption="{!insurances}"
                rightLabel="Selected Insurance"
                rightOption="{!selectedInsurances}"
                size="3"
                width="150px"/>
            </apex:pageBlockSection>
          </div>                  
          <div class="slds-form-element package_inner_box">
            <apex:commandButton action="{!saveTrip}" value="Save" styleClass="slds-button slds-button_brand" onclick="showToast('Trip is booked', 'success');" />
          </div>
        </apex:pageBlock>
      </apex:form>
    </div>
    <div class="booked_flight_panel" style="{!IF(tripBooked, 'display:block', 'display:none')}">
      <apex:form styleClass="slds-scope">
        <apex:pageBlock title="Confirmation" id="finalConfirmation">
          <apex:repeat var="cn" value="{!confirmationNumber}">
            <h3 class="confirmation">Confirmation Number: {!cn}</h3>
          </apex:repeat>  
          <apex:pageBlockTable value="{!bookedSeats}" var="bs">
            <apex:column headerValue="Traveller" value="{!bs.Contact__r.Name}"/>
            <apex:column headerValue="Origin" value="{!bs.Flight__r.Origin__c}"/>
            <apex:column headerValue="Destination" value="{!bs.Flight__r.Destination__c}"/>
            <apex:column headerValue="Flight Date/Time" value="{!bs.Flight_Date_Time__c}"/>
            <apex:column headerValue="Seat Type" value="{!bs.Seat_Type__c}"/>
            <apex:column headerValue="Meal" value="{!bs.Meal__c}"/>
            <apex:column headerValue="Insurance" value="{!bs.Insurance__c}"/>
          </apex:pageBlockTable>
        </apex:pageBlock>
        <div class="slds-form-element package_inner_box">
          <apex:commandButton action="{!redirectToAccount}" value="Close" styleClass="slds-button slds-button_neutral" />
        </div>
      </apex:form>
    </div>
  </body>
</apex:page>
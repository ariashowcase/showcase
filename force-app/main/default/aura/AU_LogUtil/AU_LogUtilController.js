/**
 * Created by jliu on 5/7/2018.
 */
({
  scriptsLoaded : function(component) {
    var baseUrl = $A.get('$Resource.AU_Resources');
    window.aria.Rjs.require.config({
      baseUrl: baseUrl
    });

    window.aria.Rjs.require(['utils/js/logUtil'], function(LogUtil) {
      var loggerId = component.get('v.loggerId');
      var logger = LogUtil.getLogger(loggerId);

      logger.setDebugInfoWriter(function(debugInfoDetails) {
        var saveCmp = component.find('debugInfoCmp');
        saveCmp.getNewRecord(
          "AU_DebugInfo__c",
          null,
          false,
          $A.getCallback(function() {
            var debugInfoRecord = component.get('v.newDebugInfo');
            debugInfoRecord.Component__c = debugInfoDetails.Component;
            debugInfoRecord.Data__c = debugInfoDetails.Data;
            debugInfoRecord.ExtraInfo__c = debugInfoDetails.ExtraInfo;
            debugInfoRecord.Level__c = debugInfoDetails.Level;
            component.set('v.newDebugInfo', debugInfoRecord);

            saveCmp.saveRecord(function(saveResult) {
              if (saveResult.state !== "SUCCESS") {
                var fatalErrorMsg = StringUtil.format(
                  "FATAL: Failed to report error! Message: {0}, State: {1}, Error: {2}",
                  debugInfoDetails.Data,
                  saveResult.state,
                  saveResult.error
                );

                window.console && window.console[level](fatalErrorMsg);
              }
            });
          })
        );
      });
    });
  },

  debug : function(component, event) {
    window.aria.Rjs.require(['utils/js/logUtil'], function(LogUtil) {
      var loggerId = component.get('v.loggerId');
      var params = event.getParam('arguments');
      var logger = LogUtil.getLogger(loggerId);

      logger.debug.apply(null, params);
    });
  },

  info : function(component, event) {
    window.aria.Rjs.require(['utils/js/logUtil'], function(LogUtil) {
      var loggerId = component.get('v.loggerId');
      var params = event.getParam('arguments');
      var logger = LogUtil.getLogger(loggerId);

      logger.info.apply(null, params);
    });
  },

  warn : function(component, event) {
    window.aria.Rjs.require(['utils/js/logUtil'], function(LogUtil) {
      var loggerId = component.get('v.loggerId');
      var params = event.getParam('arguments');
      var logger = LogUtil.getLogger(loggerId);

      logger.warn.apply(null, params);
    });
  },

  error : function(component, event) {
    window.aria.Rjs.require(['utils/js/logUtil'], function(LogUtil) {
      var loggerId = component.get('v.loggerId');
      var params = event.getParam('arguments');
      var logger = LogUtil.getLogger(loggerId);

      logger.error.apply(null, params);
    });
  }
})
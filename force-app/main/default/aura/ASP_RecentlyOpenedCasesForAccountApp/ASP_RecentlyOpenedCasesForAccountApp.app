<!--
 - Created by jfischer on 7/20/2016.
 -->

<aura:application access="GLOBAL" extends="ltng:outApp" description="ASP_RecentlyOpenedCasesForAccountApp">
  <!-- Case without Account -->
  <c:ASP_RecentlyOpenedCasesForAccount referenceCaseId="5001500000VJXBm" />

  <!-- Case with Account -->
  <c:ASP_RecentlyOpenedCasesForAccount referenceCaseId="5001500000VJXgL" />

</aura:application>
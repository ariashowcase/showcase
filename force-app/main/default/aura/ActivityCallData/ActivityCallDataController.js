({
	init : function(component, event, helper) {
        var action = component.get("c.getInteractionSegmentIdFromTask");
        action.setParams({"taskId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                component.set("v.interactionSegmentId", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    
    openInteractionSegment : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        var interactionSegmentId = component.get("v.interactionSegmentId");
        
        if (interactionSegmentId) {
            workspaceAPI.openTab({
                recordId: interactionSegmentId,
                focus: true
            });
        }
    }
})
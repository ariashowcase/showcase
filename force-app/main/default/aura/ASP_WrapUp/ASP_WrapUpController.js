/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 30-Oct-18.
 */

({
  initCrossWindowLib: function (component, event, helper) {
      aria.log.debug('WrapUp:crossWindow library loaded');
      window.__registeredWindows = [];

      /************************************************************
       *********************** REGISTRATION ***********************
       ************************************************************/
      var registerWindow = function (args, postbackFunction, senderWindow) {
        aria.log.info("WrapUp:registerWindow invoked");
        var i, numRegisteredWindows = window.__registeredWindows.length;
        for (i = 0; i < numRegisteredWindows; i++) {
          // Since the API does not unregister a window, we need to ensure that the current window does not get
          // registered multiple times. This could happen if a screen pop overrides the current API client windows
          // in CIMplicity's iFrames.
          if (window.__registeredWindows[i] === senderWindow) {
            postbackFunction();
            aria.log.info("WrapUp:registerWindow windows already registered");
            return;
          }
        }

        window.__registeredWindows.push(senderWindow);
        postbackFunction();
        aria.log.info("WrapUp:registerWindow done");
      };

      aria.crossWindowMessage.registerHandler("Aria.WrapUp.InitializeApi", registerWindow, "*");

      // TODO: All handlers have to be checked later. Some of them may be modified or removed.
      
      aria.crossWindowMessage.registerHandler("Aria.WrapUp.Command.OpenPrimaryTab", function (args, postbackFunction) { helper.openPrimaryTab(component, args, postbackFunction); }, "*");
      aria.crossWindowMessage.registerHandler("Aria.WrapUp.Command.OpenSubtab", function (args, postbackFunction) { helper.openSubtab(component, args, postbackFunction); }, "*");
      aria.crossWindowMessage.registerHandler("Aria.WrapUp.Command.GetFocusedPrimaryTabId", function (args, postbackFunction) { helper.getFocusedPrimaryTabId(component, args, postbackFunction); }, "*");
      aria.crossWindowMessage.registerHandler("Aria.WrapUp.Command.SetPresenceStatus", function (args, postbackFunction) { helper.setOnmiPresenceStatus(component, args, postbackFunction); }, "*");
      aria.crossWindowMessage.registerHandler("Aria.WrapUp.Command.Logout", function (args, postbackFunction) { helper.logoutOmni(component, args, postbackFunction); }, "*");

      aria.crossWindowMessage.registerHandler("Aria.WrapUp.Command.ShowToast", function (args) { helper.showToast(component, args); }, "*");
    },

    onTabClosed : function(component, event, helper) {
      aria.log.debug("WrapUp:onTabClosed invoked.");
      var tabId = event.getParam('tabId');
      var workItemId = event.getParam('workItemId');

      helper.triggerEvent(window.__registeredWindows, 'TabClosed', {
        id: tabId
      });
    },

    onTabCreated : function(component, event, helper) {
      if (typeof aria === "undefined")
          return;
        
      aria.log.debug("WrapUp:onTabOpened invoked.");
      var tabId = event.getParam('tabId');

      helper.triggerEvent(window.__registeredWindows, 'TabOpened', {
        id: tabId
      });

      var workItemId = component.get("v.workItemId");
      var workId = component.get("v.workId");
      if (workItemId && workId) {
        component.set("v.workItemId", null);
        component.set("v.workId", null);
        helper.openTab(component, workItemId, workId, tabId);
      }
    },

    onOmniStatusChanged : function(component, event, helper) {
      aria.log.debug("WrapUp:onOmniStatusChanged invoked.");
      var statusId = event.getParam('statusId');
      var channels = event.getParam('channels');
      var statusName = event.getParam('statusName');
      var statusApiName = event.getParam('statusApiName');

      helper.triggerEvent(window.__registeredWindows, 'PresenceStatusChanged', {
        statusId: statusId,
        channels: channels,
        statusName: statusName,
        statusApiName: statusApiName
      });
    },

    onOmniLogout : function(component, event, helper) {
      aria.log.debug("WrapUp:onOmniLogout invoked.");
      helper.triggerEvent(window.__registeredWindows, 'Logout', {});
    },

//    onWorkAssigned : function(component, event, helper) {
//      aria.log.debug("WrapUp:onWorkAssigned invoked.");
//      var workItemId = event.getParam('workItemId');
//      var workId = event.getParam('workId');
//      component.set("v.workItemId", workItemId);
//      component.set("v.workId", workId);
//      helper.triggerEvent(window.__registeredWindows, 'WorkAssigned', {
//        workId: workId,
//        workItemId: workItemId
//      });
//    },

    onWorkAccepted : function(component, event, helper) {
      aria.log.debug("WrapUp:onWorkAccepted invoked.");
      var workItemId = event.getParam('workItemId');
      var workId = event.getParam('workId');
      component.set("v.workItemId", workItemId);
      component.set("v.workId", workId);
      helper.triggerEvent(window.__registeredWindows, 'WorkAccepted', {
        workId: workId,
        workItemId: workItemId
      });
    },

    onWorkClosed : function(component, event, helper) {
      aria.log.debug("WrapUp:onWorkAccepted invoked.");
      var workItemId = event.getParam('workItemId');
      var workId = event.getParam('workId');
      helper.triggerEvent(window.__registeredWindows, 'WorkAccepted', {
        workId: workId,
        workItemId: workItemId
      });
    }
})
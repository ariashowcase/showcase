/**
 * Created by jliu on 7/17/2018.
 */
({
  init : function(component, event, helper) {
    helper.getUserContact(component,
      function(response) {
        var contact = response.getReturnValue();
        component.set("v.contact", contact);
        component.set("v.isGuest", contact == null);
      },
      function() {
        component.set("v.contact", null);
        component.set("v.isGuest", true);
      }
    );

    component.set("v.formInput", {
      contactId : null,
      firstName : null,
      lastName : null,
      phone: null,
      comments : null
    });
    var loginURL = "/s/login/?startURL=" + encodeURIComponent(window.location.pathname);
    component.set("v.loginURL", loginURL);
  },

  submit : function(component, event, helper) {
    var isValid = component.find("callCampaignForm").reduce(function(validSoFar, inputCmp) {
      inputCmp.reportValidity();
      return validSoFar && inputCmp.checkValidity();
    }, true);

    if (!isValid)
      return;

    var formInput = component.get("v.formInput");
    var contact = component.get("v.contact");

    if (contact != null) {
      formInput.contactId = contact.Id;
      if (formInput.phone === null || formInput.phone === "") {
        formInput.phone = contact.Phone;
      }
    }

    if (formInput.phone.charAt(0) !== "+")
      formInput.phone = "+" + formInput.phone;

    var successCallback = function() {
      component.set("v.isSubmitted", true);
    };
    var errorCallback = function(response) {
      component.set("v.isError", true);
      component.set("v.isSubmitted", true);
    };

    if (component.get("v.createOutboundCalls")) {
      var acInfo = {
        accessKey: component.get("v.accessKey"),
        accessSecret: component.get("v.accessSecret"),
        region: component.get("v.region"),
        clientToken: component.get("v.clientToken"),
        contactFlowId: component.get("v.contactFlowId"),
        instanceId: component.get("v.instanceId"),
        queueId: component.get("v.queueId"),
        sourcePhoneNumber: component.get("v.sourcePhoneNumber")
      };
      helper.createOutboundCall(component, formInput, acInfo, successCallback, errorCallback);
    }
    else {
      helper.createCallCampaign(component, formInput, successCallback, errorCallback);
    }
  }
})
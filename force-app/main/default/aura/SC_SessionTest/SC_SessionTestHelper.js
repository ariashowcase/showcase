/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 10-Sep-19.
 */

({
  getSessionKey : function (component, helper)  {
    var action = component.get("c.getKey");
    console.log('isSearchData: ' + component.get('v.isSearchData'));
    console.log('isContacts: ' + component.get('v.isContacts'));
    console.log('isFlights: ' + component.get('v.isFlights'));
    console.log('isSeatOptions: ' + component.get('v.isSeatOptions'));
    action.setParams({
      isSearchData : component.get('v.isSearchData'),
      isContacts : component.get('v.isContacts'),
      isFlights : component.get('v.isFlights'),
      isSeatOptions : component.get('v.isSeatOptions')
    });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var sessionKey = response.getReturnValue();
        console.log("sessionKey: " + sessionKey);
        component.set('v.sessionKey', sessionKey);
        component.set('v.message', 'Session is set');
      }
      else {
        var errors = response.getError();
        if (errors && errors[0] && errors[0].message) {
          window.console.log("Error message: " + errors[0].message);
          component.set("v.message", "Error message: " + errors[0].message);
        }
        else {
          window.console.log("Unknown error");
          component.set("v.errorMessage", "Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  },

  openTab : function(component, tabId)  {
    var workspaceAPI = component.find("workspace");
    workspaceAPI.openTab({
      // pageReference: {
      //   "type": "standard__component",
      //   "attributes": {
      //       "componentName": "c__SC_BookTrip"
      //   },
      //   "state":  {
      //     "c__accountId": "0015600000FTlWPAA1",
      //     "c__sessionKey": component.get('v.sessionKey')
      //   }
      // },
      url: '/apex/SC_BookingLauncher?accountId=0015600000FTlWPAA1&sessionKey=' + component.get('v.sessionKey'),
      focus: false
      }).then(function(openSubTabResponse) {
        workspaceAPI.setTabLabel({
          tabId: openSubTabResponse,
          label: "Loading"
        });
        workspaceAPI.focusTab({tabId : openSubTabResponse});
      }).catch(function(error){
        console.log(error);
      });
  }
})
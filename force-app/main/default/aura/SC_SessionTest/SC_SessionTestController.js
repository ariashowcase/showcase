/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 10-Sep-19.
 */

({
  setSession : function(component, event, helper) {
    helper.getSessionKey(component, helper);
  },

  onCheck: function(component, event) {
    var checkSearchData = component.find("checkboxSelectedSearchData");
    var checkFlights = component.find("checkboxSelectedFlights");
    var checkContacts = component.find("checkboxSelectedContacts");
    var checkSeatOptions = component.find("checkboxSeatOptions");
    component.set('v.isSearchData', checkSearchData.get("v.value"));
    component.set('v.isFlights', checkFlights.get("v.value"));
    component.set('v.isContacts', checkContacts.get("v.value"));
    component.set('v.isSeatOptions', checkSeatOptions.get("v.value"));

    console.log('isSearchData: ' + component.get('v.isSearchData'));
    console.log('isContacts: ' + component.get('v.isContacts'));
    console.log('isFlights: ' + component.get('v.isFlights'));
    console.log('isSeatOptions: ' + component.get('v.isSeatOptions'));
    if (checkSeatOptions.get("v.value"))  {
      component.set('v.isContacts', true);
      component.set('v.isFlights', true);
      checkContacts.set('v.value', true);
      checkFlights.set('v.value', true);
      checkSearchData.set('v.value', true);
    }
    if (checkContacts.get("v.value"))  {
      component.set('v.isFlights', true);
      component.set('v.isSearchData', true);
      checkFlights.set('v.value', true);
      checkSearchData.set('v.value', true);
    }
    if (checkFlights.get("v.value"))  {
      component.set('v.isSearchData', true);
      checkSearchData.set('v.value', true);
    }
    component.set('v.message', '');
  },

  openPage : function (component, event, helper)  {
    var workspaceAPI = component.find("workspace");
    workspaceAPI.getFocusedTabInfo().then(function(response) {
      var focusedTabId = response.tabId;
      console.log(focusedTabId);
      helper.openTab(component, focusedTabId);
    })
    .catch(function(error) {
       console.log(error);
    });
  }
})
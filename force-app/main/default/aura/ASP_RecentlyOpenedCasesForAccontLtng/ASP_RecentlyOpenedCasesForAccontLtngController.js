/** Copyright 2017, Aria Solutions Inc.
*
* All Rights Reserved
* Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */

({
  doInit : function(component, event, helper){
    var recordId = component.get("v.recordId");
    window.console.log(recordId);
    var action = component.get("c.getOpenCasesForCaseAccount");
    action.setParams({
      caseId: component.get("v.recordId")
    });
    action.setCallback(this, function(response){
      var state = response.getState();
      if(state === 'SUCCESS'){
        var cases = response.getReturnValue();
        window.console.log("Cases: "+cases);
        if(cases !== null){
          component.set("v.numOfOpenCases", cases.length);
          component.set("v.caseList", cases);
          $A.util.removeClass(component.find("caseListTable"), "slds-hide");
        }
      }
    });
    $A.enqueueAction(action);
  },
  selectRecord: function(component, event, helper){
    var myEvent = $A.get("e.c:ASP_RecentlyOpenedCasesSelectRecordEvt");
    myEvent.setParam("recordId", event.target.id);
    myEvent.setParam("recordName", event.target.name);
    myEvent.fire();
  }
})
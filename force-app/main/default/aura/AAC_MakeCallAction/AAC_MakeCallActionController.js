/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */
 ({
   invoke: function(component, event, helper) {
       var phoneNumber = component.get("v.phoneNumber");

       return aria.ac.makeCall(phoneNumber);
   }
})
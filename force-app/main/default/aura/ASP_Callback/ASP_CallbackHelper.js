/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 12-Apr-19.
 */

({
  triggerEvent: function(registeredWindows, command, parameters) {
    aria.log.info("WrapUpHelper:triggerEvent '" + command + "' invoked, number of registered windows=" + registeredWindows.length);

    var apiCommand = "Aria.WrapUp.Event." + command;

    for (var i = 0, length = registeredWindows.length; i < length; i++) {
      aria.crossWindowMessage.post({
        targetWindow: registeredWindows[i],
        command: apiCommand,
        parameters: parameters,
        targetDomain: "*",
        timeout: 3000
      });
    }

    aria.log.info("WrapUpHelper:triggerEvent done");
  },

  setOnmiPresenceStatus: function (component, args, postbackFunction) {
    aria.log.debug("WrapUpHelper:setOnmiPresenceStatus(" + args.statusId + ")");
    var omniAPI = component.find("omniToolkit");

    omniAPI.getServicePresenceStatusId()
      .then(function(getStatusResult) {
        var currentStatusId = getStatusResult.statusId;
        if (currentStatusId !== args.statusId) {
          omniAPI.setServicePresenceStatus({
            statusId: args.statusId,
            callback: function(setStatusResult) {
              aria.log.debug("WrapUpHelper:onAgentHandler:AgentOnStateChangeHandler " +
                  (setStatusResult.success ? "SUCCEEDED" : "FAILED") +
                  ". Omni status set to " + setStatusResult.statusApiName);

              postbackFunction(setStatusResult);
            }
          });
        } else {
          aria.log.debug('WrapUpHelper:Omni state already set');
          postbackFunction({
            success: true,
            statusId: currentStatusId
          });
        }
      })
      .catch(function(error) {
        postbackFunction({ exception: { message: 'Failed to retrieve current presence status ID' }, error: error });
      });
  },

  logoutOmni: function (component, args, postbackFunction) {
    aria.log.debug("WrapUpHelper:logoutOmni");
    var omniAPI = component.find("omniToolkit");

    omniAPI.getServicePresenceStatusId()
      .then(function(getStatusResult) {
        omniAPI.logout({
          callback: function (logoutResult) {
            postbackFunction(logoutResult);
          }
        });
      })
      .catch(function(error) {
        postbackFunction({ exception: { message: 'Failed to retrieve current presence status ID' }, error: error });
      });
  },

  getCallCampaign : function(component, workItemId)  {
    aria.log.debug("WrapUpHelper:getCallCampaign");
    var action = component.get("c.getCallCampaign");
    console.log("CampaignId: " + component.get("v.workItemId"));
    action.setParams({campaignId : component.get("v.workItemId")});
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var records = response.getReturnValue();
        console.log("Phone Number",records[0].Phone_Number__c);
        if(records.length > 0)  {
          // TODO: get CTI type and make auto dial accordingly
          // CIMplicity auto dial:
          aria.api.voice.dial({destination: records[0].Phone_Number__c});
        }
        else{
          console.log("No Campaign records found");
        }
      }
      else {
        var errors = response.getError();
        if (errors && errors[0] && errors[0].message) {
          console.log("Error message: " + errors[0].message);
        }
        else {
          console.log("Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  },

  showToast : function (component, args) {
    aria.log.debug("WrapUpHelper:showToast");
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams(args);
    toastEvent.fire();
  },
})
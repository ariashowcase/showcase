/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 * Created on 16-Apr-18.
 */

({
  getMessages : function(component, helper) {
    var action = component.get("c.getRecords");
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var records = response.getReturnValue();
        window.console.log("records",records);
        helper.createSlides(component, records);
        helper.startAutoSliding(component, helper);
      }
      else {
        var errors = response.getError();
        if (errors && errors[0] && errors[0].message) {
          window.console.log("Error message: " + errors[0].message);
          component.set("v.errorMessage", "Error message: " + errors[0].message);
        }
        else {
          window.console.log("Unknown error");
          component.set("v.errorMessage", "Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  },

  startAutoSliding : function(component, helper) {
    var autoSlidingEnabled = component.get("v.autoSlidingEnabled");
    var autoSlidingInterval = component.get("v.autoSlidingInterval");
    var slideInterval = component.get("v.slideInterval")

    if (autoSlidingEnabled && !autoSlidingInterval) {
      autoSlidingInterval = window.setInterval(function() {
        helper.nextSlide(component);
      }, slideInterval * 1000);
      component.set("v.autoSlidingInterval", autoSlidingInterval);
    }
  },

  stopAutoSliding : function(component) {
    var autoSlidingInterval = component.get("v.autoSlidingInterval");
    window.clearInterval(autoSlidingInterval);
    component.set("v.autoSlidingInterval", null);
  },

  createSlides : function(component, messages) {
    var slides = [];
    var slidesToShow = Math.min(10, messages.length);  

    for (var i = 0; i < slidesToShow; i++) {
      slides.push({
        title : messages[i].Title__c ,
        content : messages[i].Message_Body__c,
        date : messages[i].Start_Date__c,
        showPostedData : messages[i].Show_Posted_Date__c,
        isFocused : i === 0
      });
    }
    component.set("v.slidersList", slides);
  },

  nextSlide : function(component) {
    var slidersList = component.get("v.slidersList");

    if (slidersList) {
      for (var i = 0; i < slidersList.length; i++) {
        if (slidersList[i].isFocused) {
          slidersList[i].isFocused = false;
          slidersList[(i + 1) % slidersList.length].isFocused = true;
          break;
        }
      }
    }
    component.set("v.slidersList",slidersList);
  }
})
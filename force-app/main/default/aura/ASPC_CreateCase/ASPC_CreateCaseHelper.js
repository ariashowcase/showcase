/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 * Created on 17-Jul-18.
 */

({
  MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB
  CHUNK_SIZE: 750000,      //Chunk Max size 750Kb

  saveRecord : function(component, event, helper)  {
    var logger = component.find('logUtilCmp');
    component.find("caseRecordCreator").saveRecord(aria_ltngUtil.createSaveResult(logger,
      function(saveResult) {
        aria_ltngUtil.showToast("Saved", "success", "The record was saved");

        component.set("v.parentId", saveResult.recordId);
        component.set("v.openModal", false);
        $A.get('e.force:refreshView').fire();

        if (component.find("fileAttachment").get("v.files").length > 0) {
          helper.uploadFile(component, event);
        } else {
          aria_ltngUtil.showToast("Error", "error", "Please select a valid file");
        }
      },
      function(saveResult) {
        logger.error("Problem saving contact, error: " + JSON.stringify(saveResult.error));
      },
      function(saveResult) {
        logger.error("User is offline, device doesn't support drafts.");
      }
    ));
  },

  uploadFile: function(component, event) {
    component.set("v.showLoadingSpinner", true);
    var fileInput = component.find("fileAttachment").get("v.files");

    var file = fileInput[0];
    var self = this;
    if (file.size > self.MAX_FILE_SIZE) {
        component.set("v.showLoadingSpinner", false);
        component.set("v.fileName", 'Alert : File size cannot exceed ' + self.MAX_FILE_SIZE + ' bytes.\n' + ' Selected file size: ' + file.size);
        return;
    }

    var objectFileReader = new FileReader();
    objectFileReader.onload = $A.getCallback(function() {
        var fileContents = objectFileReader.result;
        var base64 = 'base64,';
        var dataStart = fileContents.indexOf(base64) + base64.length;

        fileContents = fileContents.substring(dataStart);
        self.uploadProcess(component, file, fileContents);
    });

    objectFileReader.readAsDataURL(file);
  },

  uploadProcess: function(component, file, fileContents) {
    var startPosition = 0;
    var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);

    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '');
  },


  uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
    var getChunk = fileContents.substring(startPosition, endPosition);
    var action = component.get("c.saveChunk");
    action.setParams({
       parentId: component.get("v.parentId"),
       fileName: file.name,
       base64Data: encodeURIComponent(getChunk),
       contentType: file.type,
       fileId: attachId
    });

    var promise = this.executeAction(component, action);

    promise.then(
      $A.getCallback(function(chunkAttachmentId) {
        var startPoint = endPosition;
        var endPoint = Math.min(fileContents.length, startPoint + this.CHUNK_SIZE);

        if (startPoint < endPoint) {
          this.uploadInChunk(component, file, fileContents, startPoint, endPoint, chunkAttachmentId);
        } else {
          window.console.log('File is uploaded successfully');
          component.set("v.showLoadingSpinner", false);
        }
      })
    ).catch(
      $A.getCallback(function(error)  {
        component.set("v.errorMessage", error.message);
      })
    );
  },

  executeAction: function(component, action)  {
    var logger = component.find('logUtilCmp');
    return new Promise(function(resolve, reject)  {
      action.setCallback(this, aria_ltngUtil.createActionCallback(logger,
        function(response) {
          attachId = response.getReturnValue();
          resolve(attachId);
        },
        function(response)  {
          var errors = response.getError();
          var errorMessage = '';
          if (errors) {
            for(var i = 0; i < error.length; i++)  {
              if (errors[i] && errors[i].message) {
                errorMessage += errors[i].message + ";";
              }
            }
            reject(Error("Error message: Cases cannot be displayed. " + errorMessage + ". Please contact your Salesforce administrator"));
          } else {
           reject(Error("Cases cannot be displayed. Please contact your Salesforce administrator"));
          }
        },
        function(response)  {
          aria_ltngUtil.showToast("Error", "error", "From server: " + response.getReturnValue());
          reject(Error("From server: " + response.getReturnValue()));
        })
      );
      $A.enqueueAction(action);
    });
  },

  fetchPickListVal: function(component, fieldName, elementId) {
    var action = component.get("c.getCaseStatuses");
    action.setParams({
        selectedObject: "Case",
        field: fieldName
    });

    var promise = this.executeFetch(component, action);

    promise.then(
      $A.getCallback(function(allValues) {
        var picklist = [];
        if (allValues !== undefined && allValues.length > 0) {
        allValues.forEach(function(element)  {
          picklist.push({
           "class": "optionClass",
           label: element,
           value: element,
           selected: (element === 'New') ? 'true' : null});
        });
      }
      component.find(elementId).set("v.options", picklist);
      })
    ).catch(
      $A.getCallback(function(error)  {
        component.set("v.errorMessage", error.message);
      })
    );
  },

  executeFetch: function(component, action) {
    var logger = component.find('logUtilCmp');
    return new Promise(function(resolve, reject)  {
      action.setCallback(this, aria_ltngUtil.createActionCallback(logger,
        function(response) {
          var allValues = response.getReturnValue();
          resolve(allValues);
        },
        function(response)  {
          var errors = response.getError();
          var errorMessage = '';
          if (errors) {
            for(var i = 0; i < error.length; i++)  {
              if (errors[i] && errors[i].message) {
                errorMessage += errors[i].message + ";";
              }
            }
            reject(Error("Error message: Cases cannot be displayed. " + errorMessage + ". Please contact your Salesforce administrator"));
          } else {
           reject(Error("Cases cannot be displayed. Please contact your Salesforce administrator"));
          }
        }
      ));
      $A.enqueueAction(action);
    });
  },

  validateForm : function(component) {
    component.find("Subject").reportValidity();
    return (component.get("v.caseFields.Subject") === null) ? false : true;
  }
})
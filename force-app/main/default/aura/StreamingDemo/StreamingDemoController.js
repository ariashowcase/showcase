/** Copyright 2017, Aria Solutions Inc.
*
* All Rights Reserved
* Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

/**
 * Created by rmiller on 9/19/2017.
 */
({
  handleMessage : function(component, event, helper){
    console.log(event.getParam("payload"));
    component.set("v.lastMessagePayload", JSON.stringify(event.getParam("payload")));
  }
})
/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 14-Dec-18.
 */

({
  init : function(component, event, helper) {
    helper.getTranscript(component, event, helper);
  },

  openTranscript : function(component, event, handler)  {
    var navEvt = $A.get("e.force:navigateToSObject");
    navEvt.setParams({
        "recordId": event.target.id
    });
    navEvt.fire();
  }
})
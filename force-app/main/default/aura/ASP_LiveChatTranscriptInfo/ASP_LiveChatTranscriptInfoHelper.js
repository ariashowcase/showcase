/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 14-Dec-18.
 */

({
    getTranscript : function(component, event, helper) {
      var action = component.get("c.getRecords");
      action.setParams({segmentId : component.get("v.recordId")});
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var records = response.getReturnValue();
          window.console.log("records",records);
          (records.length > 0 && records[0].EndTime) ? component.set("v.hasTranscript", true) : component.set("v.hasTranscript", false);
          component.set('v.liveChatTranscriptRecord', records);
        }
        else {
          var errors = response.getError();
          if (errors && errors[0] && errors[0].message) {
            window.console.log("Error message: " + errors[0].message);
            component.set("v.errorMessage", "Error message: " + errors[0].message);
          }
          else {
            window.console.log("Unknown error");
            component.set("v.errorMessage", "Unknown error");
          }
        }
      });
      $A.enqueueAction(action);
    }
})
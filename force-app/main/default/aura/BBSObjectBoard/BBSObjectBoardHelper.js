({
  // initialisation function - retrieves the board data from the
  // Apex controller
	doInit : function(cmp, ev) {
    var sobjectType = cmp.get('v.SObjectType');
    var stageValueField = cmp.get('v.StageValueField');
    var stageConfigField = cmp.get('v.StageConfigField');
    var fieldNames = cmp.get('v.FieldNames');
    var warningIconField = cmp.get('v.WarningIcon');
    var secondIconField = cmp.get('v.SecondIcon');
    var includeValues = cmp.get('v.IncludeValues');
    var excludeValues = cmp.get('v.ExcludeValues');
    var filter = cmp.get('v.Filter');
    var orderBy = cmp.get('v.OrderBy');

		var action = cmp.get("c.GetStages");
    var params = {
      "sobjectType":sobjectType,
      "stageValueField": stageValueField,
      "stageConfigField": stageConfigField,
      "warningIconField": warningIconField,
      "secondIconField": secondIconField,
      "includeValues": includeValues,
      "excludeValues": excludeValues,
      "fieldNames": fieldNames,
      "filter": filter,
      "orderBy": orderBy
    };

		window.console.log('Params = ' + JSON.stringify(params));

    action.setParams(params);

    var self = this;
    action.setCallback(this, function(response) {
      try {
        self.actionResponseHandler(response, cmp, self, self.gotStages);
      }
      catch (e) {
        // TODO: We need to get rid of this window.console and use a better notification message
        window.console.log('Exception ' + e);
      }
    });
    $A.enqueueAction(action);
  },

  // generic action method response handler - carries out error checking
  // and assuming successful invokes the supplied callback, including the
  // callback data
	actionResponseHandler : function (response, component, helper, cb, cbData) {
    try {
      var state = response.getState();
			if (state === "SUCCESS") {
				var retVal = response.getReturnValue();
        cb(component, helper, retVal, cbData);
			}
			else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						window.console.log("Error message: " + errors[0].message);
					}
				}
				else {
					window.console.log("Unknown error");
				}
			}
    }
    catch (e) {
      window.console.log('Exception in actionResponseHandler: ' + e);
    }
  },

  // callback invoked when the board data is retrieved
  gotStages : function(cmp, helper, stages) {
    window.console.log('Got stages ' + JSON.stringify(stages));

    cmp.set('v.Stages', stages);
    cmp.set('v.ColumnWidths', stages.length);
  },

  // helper method to extract a parameter from the URL
  getURLParameter : function(param, defaultValue) {
    var result = decodeURIComponent((new RegExp('[?|&]' + param + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[""])[1].replace(/\+/g, '%20'))||null;
    if (null == result && defaultValue) {
      result = defaultValue;
    }
    return result;
  },

  checkLightningExperience: function() {
    var isEnabled = true;
    if (window.location.href.indexOf("visual.force.com") > -1) {
      isEnabled = false;
    }
    return isEnabled;
  },

  onStatusChange : function(component, event) {
    var omniAPI = component.find("omniToolKit");
    omniAPI.getServicePresenceStatusId().then(function(result) {
      window.console.log('Status Id is: ' + result.statusApiName);

      var statusApiName = result.statusApiName;
      var links = component.find("clickableTitle");
      var msg = component.find("stateMsg");
      var refreshBtn = component.find("refreshButton");

      if (statusApiName === 'ASP_BacklogWork') {
        $A.util.addClass(msg, "slds-hide");
        $A.util.removeClass(refreshBtn, "slds-hide");
        for(var cmp in links) {
          $A.util.removeClass(links[cmp], "avoid-clicks");
        }
      }
      else {
        $A.util.addClass(refreshBtn, "slds-hide");
        $A.util.removeClass(msg, "slds-hide");
        for (var comp in links) {
          window.console.log(comp);
          $A.util.addClass(links[comp], "avoid-clicks");
        }
      }
    }).catch(function(error) {
      window.console.log(error);
    });
  }
})
({
  doInit : function(component, event, helper) {
    var action = component.get("c.getUserId");
    action.setCallback(this, function(response) {
      var state = response.getState();
      component.set("v.OrderBy", "LastModifiedDate ASC");
      if (state === "SUCCESS") {
        component.set("v.Filter", "OwnerId = '" + response.getReturnValue() + "'");
      }
      else {
        window.console.log("couldn't get user information to filter on");
      }
      helper.doInit(component, event);
    });
    $A.enqueueAction(action);

    var win = window.setInterval(
      $A.getCallback(function() {
        helper.doInit(component, event);
      }), 300000
    );
    component.set("v.win", win);
    helper.onStatusChange(component, event, helper);
	},

  handleRecordStageChanged: function (component, event, helper) {
    helper.doInit(component, event);
  },

  openTab : function(component, event, helper) {
    if (helper.checkLightningExperience()) {
      var workspaceAPI = component.find("workspace");
      var utilityBar = component.find("utilitybar");
      var id = event.target.id;

      workspaceAPI.openTab({
        url: '#/sObject/'+id+'/view',
        focus: true
      }).catch(function(error) {
        window.console.log(error);
      });
      utilityBar.minimizeUtility();
    } 
    else {
      var myEvent = $A.get("e.c:BBSObjectBoardRecordSelectedEvent");
      myEvent.setParam("recordId", event.target.id);
      myEvent.fire();
    }
  },

  refreshData: function(component, event, helper) {
    helper.doInit(component, event);
  },

  handleConsoleStatusChanged: function(component, event, helper) {
    var isEnabled = event.getParam("status");
    var links = component.find("clickableTitle");
    window.console.log('entered handleConsoleStatusChanged');
    if (isEnabled) {
      for(var cmp in links) {
        $A.util.removeClass(links[cmp], "avoid-clicks");
      }
    }
    else {
      for(var comp in links) {
         $A.util.addClass(links[comp], "avoid-clicks");
      }
    }
  },

  onStatusChange: function(component, event, helper) {
    helper.onStatusChange(component,event, helper);
  },

  handleShowPopover: function(component, event, helper) {
    var selectItemId = event.target.id;
    if (selectItemId.startsWith("500"))	{
      var selector = "a[id='" +  selectItemId + "']";
        //TODO try to go off name if possible
        $A.createComponent(
          "c:BBSObjectPopover",
          { "CaseId": selectItemId },
          function(content, status) {
            if (status === 'SUCCESS') {
              var overlay = component.get('v.overlayPanel');
              if (overlay) {
                overlay.close();
              }
    
              component.find('overlayLib').showCustomPopover({
                body: content,
                referenceSelector: selector,
                cssClass: "popover"
              }).then($A.getCallback(function(overlay) {
                component.set('v.overlayPanel', overlay);
              }));
            }
          }
        );    
    }  
  },

  handleClosePopover: function(component, event, helper) {
    var overlay = component.get('v.overlayPanel');
    if (overlay) {
      setTimeout(function() {
        overlay.close();
      }, 5000);
    }
  }
})
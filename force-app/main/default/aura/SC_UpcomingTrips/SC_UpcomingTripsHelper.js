({
  getTrips : function(component, event, helper) {
    var action = component.get("c.getTrips");
    action.setParams({accountId : component.get("v.recordId")});
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var records = response.getReturnValue();
        window.console.log("records",records);
        (records.length > 0) ? component.set("v.hasTrip", true) : component.set("v.hasTrip", false);
        component.set('v.tripRecord', records);
      }
      else {
        var errors = response.getError();
        if (errors && errors[0] && errors[0].message) {
          window.console.log("Error message: " + errors[0].message);
          component.set("v.errorMessage", "Error message: " + errors[0].message);
        }
        else {
          window.console.log("Unknown error");
          component.set("v.errorMessage", "Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  }
})
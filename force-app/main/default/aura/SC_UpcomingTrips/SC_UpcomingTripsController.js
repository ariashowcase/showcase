({
    init : function(component, event, helper) {
      helper.getTrips(component, event, helper);
    },

    openTrip : function(component, event, handler)  {
      var navEvt = $A.get("e.force:navigateToSObject");
      navEvt.setParams({
        "recordId": event.target.id
      });
      navEvt.fire();
    }
})
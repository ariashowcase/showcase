/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 13-Jul-18.
 */

({
  getContactIdAndAccountId : function(component, helper)  {
    var action = component.get("c.getCurrentUserContactIdAndAccountId");
    var promise = helper.executeAction(component, action);

    promise.then(
      $A.getCallback(function(userRecord) {
        component.set("v.contactId", userRecord.ContactId);
        component.set("v.accountId", userRecord.AccountId);
      })
    ).catch(
      $A.getCallback(function(error)  {
        component.set("v.errorMessage", error.message);
      })
    );
  },

  executeAction: function(component, action)  {
    var logger = component.find('logUtilCmp');
    return new Promise(function(resolve, reject)  {
      action.setCallback(this, aria_ltngUtil.createActionCallback(logger,
        function(response) {
          var state = response.getState();
          var userRecord = response.getReturnValue();
          resolve(userRecord);
        },
        function(response) {
          var errors = response.getError();
          var errorMessage = '';
          if (errors) {
            for(var i = 0; i < error.length; i++)  {
              if (errors[i] && errors[i].message) {
                errorMessage += errors[i].message + ";";
              }
            }
            reject(Error("Error message: Cases cannot be displayed. " + errorMessage + ". Please contact your Salesforce administrator"));
          } else {
           reject(Error("Cases cannot be displayed. Please contact your Salesforce administrator"));
          }
        })
      );
      $A.enqueueAction(action);
    });
  }
})
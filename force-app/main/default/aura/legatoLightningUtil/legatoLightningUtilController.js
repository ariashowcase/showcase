({
	init : function(component, event, helper) {
    var m = aria.crossWindowMessage;
    var registerWindow = function () {};
    var registerSfUtil = function () {};

    var omniApi = component.find("omniApi");

    window.legato = window.legato || {};
    legato.atOmniCapacity = false;
    legato.lastState = null;
    var openctiWindow = window.legato.openctiWindow || null;
  
    var registered = new Promise(function (resolve) {
      registerSfUtil = function(data, pb, sender) {
        if (!openctiWindow)
          openctiWindow = window.legato.openctiWindow = sender;
        pb();
        resolve();
        m.removeHandler("Aria.Api.InitializeLightningComponent", registerSfUtil);
      }

      m.registerHandler("Aria.Api.InitializeLightningComponent", registerSfUtil, "*")

      registerWindow = function(data, pb, sender) {
        if (!openctiWindow)
          openctiWindow = window.legato.openctiWindow = sender;
        pb();
        resolve();
        m.removeHandler("Aria.Api.InitializeApi", registerWindow);
      }

      m.registerHandler("Aria.Api.InitializeApi", registerWindow, "*")
    });
     
    
    function sendMessage(command, data) {
      return registered.then(function () {
        return new Promise(function (resolve, reject) {
          m.post({
            targetWindow: openctiWindow,
            targetDomain: "*", 
            command: command,
            parameters: data || {},
            successCallback: function () {
              resolve.apply(null, arguments);
            },
            errorCallback: function () {
              reject.apply(null, arguments);
            }
          });
        });
      });
    }

    window.legato.lightningUtil = { sendMessage: sendMessage };

    m.registerHandler("e.force:showToast", function (data, pb) {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
          "title": data.title,
          "message": data.message,
          "type": data.type
      });
      toastEvent.fire();
      pb();
    }, "*");

    m.registerHandler("omniApi.setServicePresenceStatus", function (data, pb) {
      omniApi.setServicePresenceStatus(data)        
        .then(pb)
        .catch(function (e) { pb({ exception: e }) });
    }, "*");

    m.registerHandler("omniApi.getServicePresenceStatusId", function (data, pb) {
      omniApi.getServicePresenceStatusId()        
        .then(pb)
        .catch(function (e) { pb({ exception: e }) });
    }, "*");

    m.registerHandler("omniApi.logout", function (data, pb) {
      omniApi.logout()        
        .then(pb)
        .catch(function (e) { pb({ exception: e }) });
    }, "*");
  },
  onOmniChannelWorkloadChanged: function (component, event) {
    var configuredCapacity = event.getParam('configuredCapacity');
    var previousWorkload = event.getParam('previousWorkload');
    var newWorkload = event.getParam('newWorkload');

    if (configuredCapacity === newWorkload) {
      legato.atOmniCapacity = true;
      legato.lightningUtil.sendMessage("lightning.omni.onChannelStatusChanged", {
        statusApiName: 'Busy'
      });
    } else {
      legato.atOmniCapacity = false;      
      legato.lightningUtil.sendMessage("lightning.omni.onChannelStatusChanged", {
        statusApiName: legato.lastState
      });
    }
  },
  onOmniChannelStatusChanged: function(component, event, helper) {
    if (legato.atOmniCapacity) {
      legato.lightningUtil.sendMessage("lightning.omni.onChannelStatusChanged", {
        statusApiName: 'Busy'
      });
      return;
    }

    legato.lastState = event.getParam('statusApiName')
    legato.lightningUtil.sendMessage("lightning.omni.onChannelStatusChanged", {
      channels: event.getParam('channels'),
      statusId: event.getParam('statusId'),
      statusName: event.getParam('statusName'),
      statusApiName: event.getParam('statusApiName')
    });
  },
  onOmniChannelLogout: function (component, event, helper) {    
    legato.atOmniCapacity = false;
    legato.lastState = event.getParam('statusApiName')
    legato.lightningUtil.sendMessage("lightning.omni.onChannelLogout");
  }
})
/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belowing to 
 * the customer require a written permission from Aria Solutions. 
 */
({
  triggerEvent: function(registeredWindows, command, parameters) {
    aria.log.info("LightningOmniCtiApiBrokerHelper:triggerEvent '" + command + "' invoked, number of registered windows=" + registeredWindows.length);

    var apiCommand = "Ltng.Api.Event." + command;

    for (var i = 0, length = registeredWindows.length; i < length; i++) {
      aria.crossWindowMessage.post({
        targetWindow: registeredWindows[i],
        command: apiCommand,
        parameters: parameters,
        targetDomain: "*",
        timeout: 3000
      });
    }

    aria.log.info("LightningOmniCtiApiBrokerHelper:triggerEvent done");
  },

  setOnmiPresenceStatus: function (component, args, postbackFunction) {
    aria.log.debug("LightningOmniCtiApiBrokerHelper:setOnmiPresenceStatus(" + args.statusId + ")");
    var omniAPI = component.find("omniToolkit");

    omniAPI.getServicePresenceStatusId({
      callback: function(getStatusResult) {
        if (getStatusResult.success) {
          var currentStatusId = getStatusResult.statusId;
          if (currentStatusId !== args.statusId) {
            omniAPI.setServicePresenceStatus({
              statusId: args.statusId,
              callback: function(setStatusResult) {
                aria.log.debug("LightningOmniCtiApiBrokerHelper:onAgentHandler:AgentOnStateChangeHandler " +
                    (setStatusResult.success ? "SUCCEEDED" : "FAILED") +
                    ". mni status set to " + setStatusResult.statusApiName);

                postbackFunction(setStatusResult);
              }
            });
          } else {
            aria.log.debug('LightningOmniCtiApiBrokerHelper:Omni state already set');
            postbackFunction({
              success: true,
              statusId: currentStatusId
            });
          }
        } else {
          postbackFunction({ exception: { message: 'Failed to retrieve current presence status ID', result: getStatusResult}, result: getStatusResult });
        }
      }
    });
  },

  logoutOmni: function (component, args, postbackFunction) {
    aria.log.debug("LightningOmniCtiApiBrokerHelper:logoutOmni");
    var omniAPI = component.find("omniToolkit");

    omniAPI.getServicePresenceStatusId({
      callback: function(getStatusResult) {
        if (getStatusResult.success) {
          omniAPI.logout({
            callback: function (logoutResult) {
              postbackFunction(logoutResult);
            }
          });
        } else {
          postbackFunction({ exception: {message: 'Failed to retrieve currently focused tab'}, result: getStatusResult });
        }
      }
    });
  },

  getFocusedPrimaryTabId: function (component, args, postbackFunction) {
    aria.log.debug("LightningOmniCtiApiBrokerHelper:getFocusedPrimaryTabId");
    var workspaceAPI = component.find("workspace");

    workspaceAPI.getFocusedTabInfo().then(function(response) {
      var focusedTabId = response.parentTabId || response.tabId;
      postbackFunction({
        id: focusedTabId
      });
    })
    .catch(function(error) {
      postbackFunction({ exception: { message: 'Failed to retrieve currently focused tab', error: error }, result: {success: false} });
    });
  },

  openSubtab: function (component, args, postbackFunction) {
    aria.log.debug("LightningOmniCtiApiBrokerHelper:openSubtab");
    var workspaceAPI = component.find("workspace");
    workspaceAPI.openSubtab(args).then(function(response) {
      postbackFunction({
        success: true,
        id: response
      });
    })
    .catch(function(error) {
      postbackFunction({ exception: { message: 'Failed to open subtab', error: error}, result: {success: false} });
    });
  },

  openPrimaryTab: function (component, args, postbackFunction) {
    aria.log.debug("LightningOmniCtiApiBrokerHelper:openPrimaryTab");
    var workspaceAPI = component.find("workspace");
    workspaceAPI.openTab(args).then(function(response) {
      postbackFunction({
        success: true,
        id: response
      });
    })
    .catch(function(error) {
      postbackFunction({ exception: {message: 'Failed to open primary tab', error: error}, result: {success: false} });
    });
  },
})
/**
 * Created by jliu on 7/20/2018.
 */
({
  init : function(component) {
    var vfOrigin = window.location.origin; // works since VF page is hosted in same domain as community page
    window.addEventListener("message", function(event) {

      if (event.origin === vfOrigin) {
        if (event.data === "captcha-unlock") {
          component.set("v.isVerified", true);
        }
        else if (event.data === "captcha-lock") {
          component.set("v.isVerified", false);
        }
        else if (event.data.type === "captcha-resize") {
          var iframeId = "captcha_iframe_" + component.getGlobalId();
          document.getElementById(iframeId).height = event.data.height + 20;
          document.getElementById(iframeId).width = event.data.width + 20;
        }
      }
    });
  }
})
/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */
 ({
  invoke: function(component) {
    var workspaceAPI = component.find('workspace');

    workspaceAPI.getEnclosingTabId().
      then(function(tabId) {
        workspaceAPI.closeTab({tabId: tabId});
      }).
      catch (function(error) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Failed to close tab",
            message: "Please close the tab of the wizard manually.",
            type: "warning"
        });
        toastEvent.fire();
      });
  }
})
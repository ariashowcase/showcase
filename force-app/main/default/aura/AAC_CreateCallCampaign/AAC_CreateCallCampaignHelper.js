/**
 * Created by jliu on 7/18/2018.
 */
({
  getUserContact : function(component, successCallback, errorCallback) {
    var action = component.get("c.getUserContact");
    action.setCallback(this, aria_ltngUtil.createActionCallback(null, successCallback, errorCallback, errorCallback));
    $A.enqueueAction(action);
  },

  createCallCampaignWithContact : function(component, contactId, phone, comments, successCallback, errorCallback) {
    var action = component.get("c.createCallCampaignWithContact");
    action.setParams({
      contactId: contactId,
      phone: phone,
      comments: comments
    });
    action.setCallback(this, aria_ltngUtil.createActionCallback(null, successCallback, errorCallback, errorCallback));
    $A.enqueueAction(action);
  },

  createCallCampaign : function(component, firstName, lastName, phone, comments, successCallback, errorCallback) {
    var action = component.get("c.createCallCampaign");
    action.setParams({
      firstName: firstName,
      lastName: lastName,
      phone: phone,
      comments: comments
    });
    action.setCallback(this, aria_ltngUtil.createActionCallback(null, successCallback, errorCallback, errorCallback));
    $A.enqueueAction(action);
  }
})
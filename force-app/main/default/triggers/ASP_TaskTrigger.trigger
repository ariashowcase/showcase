/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 15-May-19. 
 */

trigger ASP_TaskTrigger on Task (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

  AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      Task.SObjectType,
      trigger.operationType,
      trigger.isExecuting,
      trigger.new,
      trigger.old,
      trigger.newMap,
      trigger.oldMap
  );

  ASP_TriggerBus.dispatch(args);
}
/** Copyright 2018, Aria Solutions Inc.
*
* All Rights Reserved
* Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

trigger ASP_AgentWorkTrigger on AgentWork (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

  AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
    AgentWork.getSObjectType(),
    trigger.operationType,
    trigger.isExecuting,
    trigger.new,
    trigger.old,
    trigger.newMap,
    trigger.oldMap
  );

  ASP_TriggerBus.dispatch(args);
}